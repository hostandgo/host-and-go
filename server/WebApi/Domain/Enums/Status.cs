namespace WebApi.Domain.Enums
{
    public enum Status
    {
        Host = 1,
        Travel = 2
    }
}