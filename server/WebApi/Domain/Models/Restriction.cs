using System;
using WebApi.Domain.Enums;
using WebApi.Domain.Exceptions;

namespace WebApi.Domain.Models
{
    public class Restriction
    {
        public Restriction(RestrictionsTypes type, string complement = null)
        {
            if (NeedComplement(type) && String.IsNullOrEmpty(complement))
                throw new DomainException($"{type.GetDescription()} need complement");

            this.Type = type;
            this.Complement = complement;
        }
        public RestrictionsTypes Type { get; private set; }
        public string Complement { get; private set; }

        public bool NeedComplement(RestrictionsTypes restriction)
        => restriction == Enums.RestrictionsTypes.Religion;
    }
}