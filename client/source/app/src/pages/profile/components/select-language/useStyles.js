import { makeStyles } from "@material-ui/core/styles";

export default makeStyles(theme => ({
  container: {
    display: "flex",
    alignItems: "center",
    flexWrap: "wrap",
    width: "100%",
    paddingRight: "8px",
    paddingLeft: "8px"
  },
  fabLanguage: {
    margin: theme.spacing(1)
  }
}));
