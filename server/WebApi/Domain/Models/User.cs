using System.Collections.Generic;
using System.Linq;
using WebApi.Domain.Enums;
using WebApi.Domain.Exceptions;
using WebApi.Domain.ValueObjects;

namespace WebApi.Domain.Models
{
    public class User : BaseEntity
    {
        public User(string email, string password)
        {
            this.Status = Status.Host;
            this.HasSecurity = false;
            this.Weeks = 0;
            this.Stars = 0;
            this.CanTravel = false;
            this.Authentication = new Authentication(email, password);
        }
        public Authentication Authentication { get; private set; }
        public Status Status { get; private set; }
        public bool HasSecurity { get; private set; }
        public int Weeks { get; private set; }
        public bool CanTravel { get; private set; }
        public int Stars { get; private set; }


        public User BuySecurity()
        {
            this.HasSecurity = true;
            return this;
        }

        public User FinishedHosting(int weeks)
        {
            if(weeks <= 0)
                throw new DomainException("Weeks less than zero");

            this.Status = Status.Travel;    
            this.Weeks += weeks;
            this.CanTravel = true;
            return this;
        }
    }
}