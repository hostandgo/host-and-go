using System;
using System.Linq;
using WebApi.Application.Payloads;
using WebApi.Domain.ValueObjects;

namespace WebApi.Application.ViewModels
{
    public class ProfileViewModel
    {
        public ProfileViewModel() { }
        public ProfileViewModel(
            int stars,
            string firstName,
            string secondName,
            string email,
            string status,
            int? genre,
            string nativeLanguage,
            string documentURL,
            string nationality,
            DateTime? dateOfBirth,
            Location location,
            string instagram,
            string facebook,
            string twitter,
            string linkedin,
            string religion,
            string deficiency,
            Language[] languages,
            string[] familyPhotos,
            string picture)
        {
            this.Stars = stars;
            this.FirstName = firstName;
            this.SecondName = secondName;
            this.Email = email;
            this.Status = status;
            this.Genre = genre;
            this.NativeLanguage = nativeLanguage;
            this.Document = documentURL;
            this.Nationality = nationality;
            this.DateOfBirth = dateOfBirth;
            this.Location = location;
            this.Instagram = instagram;
            this.Facebook = facebook;
            this.Twitter = twitter;
            this.Linkedin = linkedin;
            this.Languages = languages.Select(l => new LanguagePayload(){ Name = l.Name, Level = l.Level }).ToArray();
            this.FamilyPhotos = familyPhotos;
            this.Picture = picture;
            this.Deficiency = deficiency;
            this.Religion = religion;
        }

        public string Religion { get; set; }

        public string Deficiency { get; set; }

        public int Stars { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Email { get; set; }
        public string Status { get; set; }
        public int? Genre { get; set; }
        public string Profission { get; set; }
        public string NativeLanguage { get; set; }
        public LanguagePayload[] Languages { get; set; }
        public string Document { get; set; }
        public string Nationality { get; set; }
        public Location Location { get; set; }
        public string Instagram { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Linkedin { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string[] FamilyPhotos { get; set; }
        public string Picture { get; set; }
    }
}