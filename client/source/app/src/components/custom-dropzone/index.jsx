import React from 'react';
import { useDropzone } from 'react-dropzone';
import { makeStyles } from '@material-ui/core/styles';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import Fab from '@material-ui/core/Fab';
import GridImageList from '../grid-image-list';
import Loader from '../loader';

const useStyles = makeStyles(theme => ({
  root: {
    height: 200,
    width: '100%',
    display: 'flex',
    alignItems: 'center'
  },
  fab: {
    margin: theme.spacing(1),
  }
}));

export default function CustomDropzone({ files, setFiles, multiple, isLoading, limitAmount = 0 }) {
  const classes = useStyles();

  const { getRootProps, getInputProps } = useDropzone({
    accept: 'image/*',
    onDrop: acceptedFiles => {
      const allFiles = multiple ? files.concat(acceptedFiles.map(file => Object.assign(file, {
        preview: URL.createObjectURL(file)
      })))
        : acceptedFiles.map(file => Object.assign(file, {
          preview: URL.createObjectURL(file)
        }))
      if (limitAmount > 0)
        setFiles(allFiles.slice(0, limitAmount));
      else
        setFiles(allFiles);
    }
  });

  function remove({ file }) {
    setFiles(files.filter(f => f.name !== file.name))
  }

  const editInputProps = { ...getInputProps(), multiple }

  return (
    <section className={classes.root}>

      <div {...getRootProps({ className: 'dropzone' })}>
        <input {...editInputProps} />
        <Fab color="primary" aria-label="add" className={classes.fab}>
          <CloudUploadIcon />
        </Fab>
      </div>
      {
        isLoading ?
          <Loader size={50} />
          :
          <GridImageList
            files={files}
            remove={remove}
          />
      }
    </section>
  );
}