import React, { useEffect, useState } from 'react';
import moment from 'moment';
import qs from 'qs';
import MenuItem from '@material-ui/core/MenuItem';
import { Avatar, Chip, Typography, TextField, Paper, Link, FormControlLabel, Checkbox } from '@material-ui/core';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import PermIdentityIcon from '@material-ui/icons/PermIdentityOutlined';
import { useDropzone } from 'react-dropzone';
import { KeyboardDatePicker } from "@material-ui/pickers";
import langmap from 'langmap';
import CustomDropzone from '../../components/custom-dropzone';
import useWindowWidth from '../../hooks/useWindowWidth';
import useStyles from './useStyles';
import api from '../../api';
import countryAndCities from '../../services/countries-and-cities'
import { formatImageToView } from '../../services/image';
import Onboarding from '../../components/onboarding';
import { steps } from '../../services/onboardings';
import DefaultWrapper from '../../components/default-wrapper';
import ButtonSaveContext from '../../components/button-save-context';
import history from '../../services/history';
import SelectLanguage from './components/select-language';
import SelectRegion from './components/select-region';

const genres = [
    {
        value: 1,
        label: 'Male',
    },
    {
        value: 2,
        label: 'Female',
    },
    {
        value: 3,
        label: 'Non-Binary',
    }
];

const allLanguages = Object.values(langmap);

const levelLanguages = [
    {
        value: 'Beginner',
        label: 'Beginner',
    },
    {
        value: 'Intermediate',
        label: 'Intermediate',
    },
    {
        value: 'Fluent',
        label: 'Fluent',
    }
];

const countryItem = (country) => {
    return (
        <MenuItem key={country} value={country}>
            {country}
        </MenuItem>
    )
}


export default function Profile() {
    const [firstName, setFirstName] = useState('')
    const [secondName, setSecondName] = useState('')
    const [nationality, setNationality] = useState('')
    const [dateOfBirth, setDateOfBirth] = useState(null)
    const [email, setEmail] = useState('')
    const [genre, setGenre] = useState('')
    const [level, setLevel] = useState('')
    const [nativeLanguage, setNativeLanguage] = useState('')
    const [languages, setLanguages] = useState([])
    const [status, setStatus] = useState('');
    const [document, setDocument] = useState([])
    const [instagram, setInstagram] = useState('')
    const [twitter, setTwitter] = useState('')
    const [facebook, setFacebook] = useState('')
    const [linkedin, setLinkedin] = useState('')
    const [region, setRegion] = useState({ id: '', name: 'Select' });
    const [continent, setContinent] = useState({ id: '', name: 'Select' });
    const [state, setState] = useState({ id: '', name: 'Select' });
    const [country, setCountry] = useState({ id: '', name: 'Select' });
    const [currentLanguage, setCurrentLanguage] = useState('')
    const [success, setSuccess] = useState(false)
    const [loading, setLoading] = useState(false)
    const [showSaveButton, setShowSaveButton] = useState(false)
    const [screenLoading, setScreenLoading] = useState(false)
    const [familyPhotos, setFamilyPhotos] = useState([])
    const [disabled, setDisabled] = useState(false);
    const [showOnboarding, setShowOnBoarding] = useState(false);
    const [picture, setPicture] = useState([])
    const [hasDeficiency, setHasDeficiency] = useState(false);
    const [deficiency, setDeficiency] = useState('');
    const [hasReligion, setHasReligion] = useState(false);
    const [religion, setReligion] = useState('');
    const [currentOnboarding, setCurrentOnboarding] = useState({
        id: 0,
        steps: []
    })
    const [errors, setErrors] = useState(false);
    const [isLoadingDocument, setIsLoadingDocument] = useState(false);
    const [isLoadingFamilyPhotos, setIsLoadingFamilyPhotos] = useState(false);

    const { getRootProps, getInputProps } = useDropzone({
        accept: 'image/*',
        onDrop: acceptedFiles => {
            const newFiles = acceptedFiles.map(file => Object.assign(file, {
                preview: URL.createObjectURL(file)
            }));
            setPicture(newFiles);
        }
    });

    function validateForm() {
        let hasError = false;

        if (!firstName) hasError = true;
        if (!secondName) hasError = true;
        if (!nationality) hasError = true;
        if (!dateOfBirth) hasError = true;
        if (!email) hasError = true;
        if (!genre) hasError = true;
        if (!nativeLanguage) hasError = true;
        if (!continent) hasError = true;
        if (!country) hasError = true;
        if (!state) hasError = true;
        if (!region) hasError = true;

        return hasError;
    }

    const classes = useStyles();
    const windowWidth = useWindowWidth();
    const isMobile = windowWidth <= 900;

    async function saveProfile() {
        setErrors(false);
        let hasError = validateForm();

        if (hasError) {
            setErrors(hasError);
            return;
        }

        if (!loading) {
            setSuccess(false);
            setLoading(true);

            await api.profile.update(
                {
                    firstName,
                    secondName,
                    nationality,
                    dateOfBirth,
                    email,
                    genre: Number(genre),
                    level,
                    nativeLanguage,
                    languages,
                    continent,
                    country,
                    state,
                    region,
                    instagram,
                    twitter,
                    facebook,
                    linkedin,
                    religion,
                    deficiency
                },
                () => { }
            )

            let formData = new FormData();
            document.forEach((file) => {
                formData.append('files', file);
            })

            await api.profile.updateDocument(
                formData,
                setIsLoadingDocument
            )

            formData = new FormData();
            familyPhotos.forEach((file) => {
                formData.append('files', file);
            })
            await api.profile.updateFamilyPhotos(
                formData,
                setIsLoadingFamilyPhotos
            )

            formData = new FormData();
            picture.forEach((file) => {
                formData.append('files', file);
            })

            await api.profile.updatePicture(
                formData,
                () => { }
            )

            setSuccess(true);
            setLoading(false);
            setTimeout(() => {
                setSuccess(false);
                setShowSaveButton(false)
            }, 2000);

            setCurrentOnboarding(steps.profile.end);
            setShowOnBoarding(true);
        }
    }

    useEffect(() => {
        const { fromRegister } = qs.parse(window.location.search, { ignoreQueryPrefix: true });
        if (fromRegister === "true") {
            setCurrentOnboarding(steps.profile.start)
            setShowOnBoarding(fromRegister);
        }

        history.push('/profile')

        return () => setCurrentOnboarding([]);
    }, [])

    useEffect(() => {
        async function getProfile() {
            const result = await api.profile.get(setScreenLoading)
            if (!result.hasError) {
                if (result.response.document)
                    setDocument([result.response.document].map(formatImageToView))

                if (result.response.picture)
                    setPicture([result.response.picture].map(formatImageToView))

                setFirstName(result.response.firstName)
                setSecondName(result.response.secondName)
                setNationality(result.response.nationality)
                setDateOfBirth(result.response.dateOfBirth)
                setEmail(result.response.email)
                setGenre(result.response.genre)
                setNativeLanguage(result.response.nativeLanguage)
                setLanguages(result.response.languages)
                if (result.response.location) {
                    setContinent(result.response.location.continent)
                    setCountry(result.response.location.country)
                    setState(result.response.location.state)
                    setRegion(result.response.location.region)
                }
                setFamilyPhotos(result.response.familyPhotos.map(formatImageToView))
                setInstagram(result.response.instagram)
                setTwitter(result.response.twitter)
                setFacebook(result.response.facebook)
                setLinkedin(result.response.linkedin)
                setDeficiency(result.response.deficiency)
                setHasDeficiency(!!result.response.deficiency)
                setHasReligion(!!result.response.religion)
                setReligion(result.response.religion)
                setStatus(result.response.status);
            }
        }

        getProfile()
    }, [])

    function addLanguage() {
        if (languages.filter(l => l.name === currentLanguage).length > 0)
            return;

        if (currentLanguage && level) {
            setLanguages([...languages, {
                name: currentLanguage,
                level: level
            }])
            setCurrentLanguage('')
            setLevel('');
        }
    }

    function removeLanguage(languageName) {
        setLanguages(languages.filter(l => l.name !== languageName))
    }
    const editInputProps = { ...getInputProps(), multiple: false };
    return (
        <DefaultWrapper>
            <div className={isMobile ? classes.containerMobile : classes.containerDesktop}>
                <Onboarding
                    run={showOnboarding}
                    customSteps={currentOnboarding.steps}
                    id={currentOnboarding.id}
                />
                <div className={isMobile ? classes.containerFormMobile : classes.containerFormDesktop}>
                    <Typography variant="h4" gutterBottom className="profile-title">Profile</Typography>
                    <div
                        style={{
                            marginBottom: 15,
                            width: '100%'
                        }}
                    >
                        <Typography>
                            Add your personal information so we can find hosts that look more like you</Typography>
                        <Typography>
                            Public info: Native language, Languages, First Name, Picture, Nationality, Date Of Birth and Country
                    </Typography>
                    </div>
                    <Paper style={{ width: '100%' }}>
                        <Typography className={classes.cardTitle} variant="h5" gutterBottom>Personal informations</Typography>
                        <div className={isMobile ? classes.topWrapperMobile : classes.topWrapperDesktop}>
                            <div className={classes.wrapAvatar}>
                                <div {...getRootProps()}>
                                    <input {...editInputProps} />
                                    <Link className={classes.editImageLink}>Edit picture</Link>
                                </div>
                                {
                                    picture.length > 0 ?
                                        <Avatar
                                            alt="user picture"
                                            src={picture[0].preview}
                                            className={classes.bigAvatar}
                                        />
                                        :
                                        <Avatar className={classes.bigAvatar}>
                                            <PermIdentityIcon style={{ fontSize: 120 }} />
                                        </Avatar>
                                }
                                <Typography>{status}</Typography>
                                <div>
                                    {/* <StarIcon />
                                <StarIcon />
                                <StarIcon />
                                <StarIcon /> */}
                                    <StarBorderIcon />
                                    <StarBorderIcon />
                                    <StarBorderIcon />
                                    <StarBorderIcon />
                                    <StarBorderIcon />
                                </div>
                            </div>
                            <div className={isMobile ? classes.formOneMobile : classes.formOneDesktop}>
                                <TextField
                                    error={errors && !firstName}
                                    helperText={(errors && !firstName) && "First name is required"}
                                    id="outlined-first-name"
                                    label="First Name"
                                    className={isMobile ? classes.textFieldMobile : classes.textFieldDesktop}
                                    value={firstName}
                                    onChange={e => setFirstName(e.target.value)}
                                    margin="normal"
                                    variant="outlined"
                                    disabled={disabled}
                                />

                                <TextField
                                    error={errors && !secondName}
                                    helperText={(errors && !secondName) && "Last name is required"}
                                    id="outlined-second-name"
                                    label="Last Name"
                                    className={isMobile ? classes.textFieldMobile : classes.textFieldDesktop}
                                    value={secondName}
                                    onChange={e => setSecondName(e.target.value)}
                                    margin="normal"
                                    variant="outlined"
                                    disabled={disabled}
                                />

                                <TextField
                                    error={errors && !nationality}
                                    helperText={(errors && !nationality) && "Nationality is required"}
                                    id="outlined-nationality"
                                    select
                                    label="Nationality"
                                    className={isMobile ? classes.textFieldMobile : classes.textFieldDesktop}
                                    value={nationality}
                                    onChange={e => setNationality(e.target.value)}
                                    margin="normal"
                                    variant="outlined"
                                    disabled={disabled}
                                >
                                    {Object.keys(countryAndCities).map(countryItem)}
                                </TextField>

                                <KeyboardDatePicker
                                    error={errors && !dateOfBirth}
                                    helperText={(errors && !dateOfBirth) && "Date of Birth is required"}
                                    autoOk
                                    variant="inline"
                                    openTo="year"
                                    maxDate={new Date(moment().subtract(18, 'years').calendar())}
                                    maxDateMessage={"vai se banha"}
                                    inputVariant="outlined"
                                    margin="normal"
                                    label="Date of Birth (MM/DD/YYYY)"
                                    className={isMobile ? classes.textFieldMobile : classes.textFieldDesktop}
                                    format="MM/dd/yyyy"
                                    value={dateOfBirth}
                                    InputAdornmentProps={{ position: "start" }}
                                    onChange={date => setDateOfBirth(date)}
                                    disabled={disabled}
                                />
                                <TextField
                                    error={errors && !email}
                                    helperText={(errors && !email) && "E-mail is required"}
                                    id="outlined-email"
                                    Date of label="E-mail"
                                    className={isMobile ? classes.textFieldMobile : classes.textFieldDesktop}
                                    value={email}
                                    onChange={e => setEmail(e.target.value)}
                                    margin="normal"
                                    variant="outlined"
                                    disabled={disabled}
                                />

                                <TextField
                                    error={errors && !genre}
                                    helperText={(errors && !genre) && "Genre is required"}
                                    id="outlined-select-genre"
                                    select
                                    label="Gender"
                                    className={isMobile ? classes.textFieldMobile : classes.textFieldDesktop}
                                    value={genre}
                                    onChange={e => setGenre(e.target.value)}
                                    SelectProps={{
                                        MenuProps: {
                                            className: classes.menu,
                                        },
                                    }}
                                    margin="normal"
                                    variant="outlined"
                                    disabled={disabled}
                                >
                                    {genres.map(option => (
                                        <MenuItem key={option.value} value={option.value}>
                                            {option.label}
                                        </MenuItem>
                                    ))}
                                </TextField>

                            </div>
                        </div>
                    </Paper>
                    <div className={isMobile ? classes.formTwoMobile : classes.formTwoDesktop}>
                        <Paper style={{ width: '32%' }}>
                            <Typography className={classes.cardTitle} variant="h5" gutterBottom>Languages</Typography>
                            <div className={isMobile ? classes.languageWrapperMobile : classes.languageWrapperDesktop}>
                                <TextField
                                    error={errors && !nativeLanguage}
                                    helperText={(errors && !nativeLanguage) && "Native language is required"}
                                    id="outlined-native-language"
                                    select
                                    label="Native language"
                                    style={{ width: '100%', paddingLeft: 8, paddingRight: 8 }}
                                    value={nativeLanguage}
                                    onChange={e => setNativeLanguage(e.target.value)}
                                    SelectProps={{
                                        MenuProps: {
                                            className: classes.menu,
                                        },
                                    }}
                                    margin="normal"
                                    variant="outlined"
                                    disabled={disabled}
                                >
                                    {allLanguages.map(option => (
                                        <MenuItem key={option.englishName} value={option.englishName}>
                                            {option.englishName}
                                        </MenuItem>
                                    ))}
                                </TextField>
                                <Typography
                                    style={{
                                        margin: '8px 8px 0px 8px',
                                        width: '100%',
                                        paddingLeft: 8
                                    }}
                                >
                                    Other languages
                                </Typography>
                                <SelectLanguage
                                    currentLanguage={currentLanguage}
                                    setCurrentLanguage={setCurrentLanguage}
                                    level={level}
                                    setLevel={setLevel}
                                    addLanguage={addLanguage}
                                />
                                <div>
                                    {
                                        languages.map((l) =>
                                            <Chip
                                                key={l.name}
                                                label={`${l.name} - ${l.level}`}
                                                onDelete={() => removeLanguage(l.name)}
                                                className={classes.chip}
                                                color="primary"
                                            />
                                        )
                                    }
                                </div>
                            </div>
                        </Paper>
                        <Paper
                            style={{
                                width: '32%',
                            }}
                        >
                            <Typography className={classes.cardTitle} variant="h5" gutterBottom>Your location</Typography>
                            <SelectRegion
                                errors={errors}
                                continentId={continent.id}
                                setContinent={setContinent}
                                countryId={country.id}
                                setCountry={setCountry}
                                stateId={state.id}
                                setState={setState}
                                regionId={region.id}
                                setRegion={setRegion}
                            />
                            {/* <div
                                style={{
                                    display: 'flex',
                                    flexDirection: 'column',
                                    width: '100%',
                                }}
                            >
                                <TextField
                                    error={errors && !country}
                                    helperText={(errors && !country) && "Country is required"}
                                    id="outlined-select-country"
                                    select
                                    label="Country"
                                    className={isMobile && classes.textFieldMobile}
                                    style={{ width: '100%', paddingLeft: 8, paddingRight: 8 }}
                                    value={country}
                                    onChange={e => setCountry(e.target.value)}
                                    margin="normal"
                                    variant="outlined"
                                    disabled={disabled}
                                >
                                    {Object.keys(countryAndCities).map(countryItem)}
                                </TextField>

                                <TextField
                                    error={errors && !city}
                                    helperText={(errors && !city) && "City is required"}
                                    id="outlined-select-city"
                                    style={{ width: '100%', paddingLeft: 8, paddingRight: 8 }}
                                    select
                                    label="City"
                                    className={isMobile && classes.textFieldMobile}
                                    value={city}
                                    onChange={e => setCity(e.target.value)}
                                    margin="normal"
                                    variant="outlined"
                                    disabled={disabled}
                                >
                                    {
                                        country &&
                                        countryAndCities[country].map(countryItem)
                                    }
                                </TextField>
                            </div> */}

                        </Paper>
                        <Paper
                            style={{
                                width: '32%'
                            }}
                        >
                            <Typography className={classes.cardTitle} variant="h5" gutterBottom>Social media</Typography>
                            <div
                                style={{
                                    display: 'flex',
                                    flexDirection: 'column',
                                    width: '100%',
                                }}
                            >
                                <TextField
                                    id="outlined-facebook"
                                    style={{ width: '100%', paddingLeft: 8, paddingRight: 8 }}
                                    label="Facebook"
                                    className={isMobile && classes.textFieldMobile}
                                    value={facebook}
                                    onChange={e => setFacebook(e.target.value)}
                                    margin="normal"
                                    variant="outlined"
                                    disabled={disabled}
                                />

                                <TextField
                                    id="outlined-twitter"
                                    style={{ width: '100%', paddingLeft: 8, paddingRight: 8 }}
                                    label="Twitter"
                                    className={isMobile && classes.textFieldMobile}
                                    value={twitter}
                                    onChange={e => setTwitter(e.target.value)}
                                    margin="normal"
                                    variant="outlined"
                                    disabled={disabled}
                                />

                                <TextField
                                    id="outlined-instagram"
                                    style={{ width: '100%', paddingLeft: 8, paddingRight: 8 }}
                                    label="Instagram"
                                    className={isMobile && classes.textFieldMobile}
                                    value={instagram}
                                    onChange={e => setInstagram(e.target.value)}
                                    margin="normal"
                                    variant="outlined"
                                    disabled={disabled}
                                />

                                <TextField
                                    id="outlined-linkedin"
                                    style={{ width: '100%', paddingLeft: 8, paddingRight: 8 }}
                                    label="Linkedin"
                                    className={isMobile && classes.textFieldMobile}
                                    value={linkedin}
                                    onChange={e => setLinkedin(e.target.value)}
                                    margin="normal"
                                    variant="outlined"
                                    disabled={disabled}
                                />
                            </div>
                        </Paper>
                    </div>
                    <Paper
                        style={{
                            display: 'flex',
                            flexDirection: 'column',
                            justifyContent: 'space-between',
                            width: '100%',
                            marginTop: 8,
                            padding: 8
                        }}
                    >
                        <Typography variant="h5" gutterBottom>Other information</Typography>
                        {/* <Typography>Passport photo or other official photo ID</Typography> */}
                        <div
                            style={{
                                display: 'flex',
                                flexDirection: 'row'
                            }}
                        >
                            <div
                                style={{
                                    display: 'flex',
                                    flexDirection: 'column',
                                    width: 300,
                                    margin: 8
                                }}
                            >
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            checked={hasDeficiency}
                                            onChange={() => setHasDeficiency(!hasDeficiency)}
                                            value={hasDeficiency}
                                            color="primary"
                                        />
                                    }
                                    label="Deficiency"
                                />
                                {
                                    hasDeficiency &&
                                    <TextField
                                        id="outlined-deficiency"
                                        label="Deficiency"
                                        fullWidth
                                        value={deficiency}
                                        onChange={e => setDeficiency(e.target.value)}
                                        margin="normal"
                                        variant="outlined"
                                    />
                                }
                            </div>
                            <div
                                style={{
                                    display: 'flex',
                                    flexDirection: 'column',
                                    width: 300,
                                    margin: 8
                                }}
                            >
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            checked={hasReligion}
                                            onChange={() => setHasReligion(!hasReligion)}
                                            value={hasReligion}
                                            color="primary"
                                        />
                                    }
                                    label="Religion"
                                />
                                {
                                    hasReligion &&
                                    <TextField
                                        id="outlined-religion"
                                        label="Religion"
                                        fullWidth
                                        value={religion}
                                        onChange={e => setReligion(e.target.value)}
                                        margin="normal"
                                        variant="outlined"
                                    />
                                }
                            </div>
                        </div>
                    </Paper>
                    <div
                        style={{
                            display: 'flex',
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            height: 300,
                            width: '100%',
                            marginTop: 8
                        }}
                    >
                        <Paper
                            style={{
                                width: '32%',
                                display: 'flex',
                                flexDirection: 'column',
                                padding: 8,
                            }}
                        >
                            <Typography variant="h5" gutterBottom>Upload document photo</Typography>
                            <Typography>Passport photo or other official photo ID</Typography>
                            <CustomDropzone
                                files={document}
                                setFiles={setDocument}
                                isLoading={isLoadingDocument}
                                multiple={false} />
                        </Paper>
                        <Paper
                            style={{
                                width: '66%',
                                display: 'flex',
                                flexDirection: 'column',
                                padding: 8,
                            }}
                        >
                            <Typography variant="h5" gutterBottom>Family Photos</Typography>
                            <Typography>Add a maximum of 6 photos</Typography>
                            <CustomDropzone
                                files={familyPhotos}
                                setFiles={setFamilyPhotos}
                                multiple
                                isLoading={isLoadingFamilyPhotos}
                                limitAmount={6}
                            />
                        </Paper>
                    </div>
                </div>
            </div>
            <ButtonSaveContext
                onSave={saveProfile}
                success={success}
                loading={loading}
            />
        </DefaultWrapper >
    )
}