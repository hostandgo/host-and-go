using System;
using System.Collections.Generic;
using WebApi.Domain.ValueObjects;

namespace WebApi.Domain.Models
{
    public class Accommodation : BaseEntity
    {
        public Guid HostId { get; private set; }
        private List<Guid> _travelsIds = new List<Guid>();
        public IReadOnlyCollection<Guid> TravelsIds => _travelsIds;
        public DateTime StartTrip { get; private set; }
        public DateTime FinalTrep { get; private set; }
        public Location Location { get; private set; }
    }
}