import { useState, useEffect } from 'react';
import api from '../api';

export default function useInterestList() {
    const [interestList, setInterestList] = useState([]);
    
    useEffect(() => {
        async function getDataInterests() {
            const result = await api.data.getInterests();
            setInterestList(result.response);
        }
        getDataInterests();
    }, []);

    return interestList;
}