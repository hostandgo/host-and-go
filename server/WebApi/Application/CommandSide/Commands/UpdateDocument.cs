using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using WebApi.Domain.Repositories;
using WebApi.Domain.Services;

namespace WebApi.Application.CommandSide.Commands
{
    public class UpdateDocumentCommand: IRequest
    {
        public Guid UserId {get; set; }
        public List<IFormFile> Files {get; set; }

        public UpdateDocumentCommand(Guid userId, List<IFormFile> files)
        {
            this.UserId = userId;
            this.Files = files;
        }
    }

    public class UpdateDocumentCommandHandler : IRequestHandler<UpdateDocumentCommand>
    {
        private readonly IProfileRepository _profileRepository;
        private readonly IFileService _fileService;

        public UpdateDocumentCommandHandler(IProfileRepository profileRepository, IFileService fileService)
        {
            _profileRepository = profileRepository;
            _fileService = fileService;
        }

        public async Task<Unit> Handle(UpdateDocumentCommand request, CancellationToken cancellationToken)
        {
            var profile = await _profileRepository.FindByUserId(request.UserId);
            var file = request.Files.FirstOrDefault();
            
            if(file == null)
                profile.UpdateDocument(null);
            else if(file.FileName != profile.Document)
                profile.UpdateDocument(await _fileService.UploadFileAsync(file));
            
            await _profileRepository.Update(profile);
            return Unit.Value;
        }
    }
}