import { toast } from 'react-toastify';

export default async function handleRequest(method, successMessage, errorMessage, setLoading = () => {}){
    try {
        setLoading(true);
        const response = await method();

        if(successMessage)
            toast.success(successMessage);
            
        return { hasError: false, response: response.data };
    } catch (error) {
        if(errorMessage)
            toast.error(errorMessage);
        
        return { hasError: true, error }
    } finally {
        setLoading(false);
    }
}