const Ids = {
    PROFILE_START: 1,
    PROFILE_END: 2,
    PREFERENCES_START: 3,
    PREFERENCES_END: 4,
    HOST_START: 5,
    HOST_END: 6
}

export const steps = {
    profile: {
        start: {
            id: Ids.PROFILE_START,
            steps: [
                {
                    title: 'Welcome to Becullen',
                    target: '.profile-title',
                    content: `
                    Your new way to share and acquire culture
                `,
                    placement: 'center',
                    showProgress: true,
                    locale: {
                        next: `Let's start`,
                    }
                },
                {
                    target: '.profile-title',
                    content: `
                        Let's start by filling in your personal data like name, language and other basic information.
                    `,
                    placement: 'center',
                    showProgress: true,
                },
                {
                    title: 'Botão salvar',
                    target: '#save-button',
                    content: `
                        After filling in your details just click here to save your information.
                    `,
                    disableBeacon: true,
                    disableOverlayClose: true,
                    placement: 'left',
                    spotlightClicks: false,
                    showProgress: true,
                    styles: {
                        options: {
                            zIndex: 10000,
                        },
                    },
                    locale: {
                        last: 'Ok, I got it'
                    }
                }
            ]
        },
        end: {
            id: Ids.PROFILE_END,
            steps: [
                {
                    title: 'Very good',
                    continuos: false,
                    target: '#side-bar-preferences-item',
                    content: `
                        Now that you have entered your personal information, let's go to your preferences.
                        Just click here to go to the Preferences page.
                    `,
                    disableBeacon: true,
                    disableOverlayClose: true,
                    placement: 'right',
                    spotlightClicks: false,
                    styles: {
                        options: {
                            zIndex: 10000,
                        },
                    },
                    locale: {
                        last: `Ok, I got it`
                    }
                }
            ]
        }
    },
    preferences: {
        start: {
            id: Ids.PREFERENCES_START,
            steps: [
                {
                    title: 'Preferences page',
                    target: '#preferences-title',
                    content: `
                        Take advantage of this space to talk a little about the things you like and what your topics of interest are
                    `,
                    placement: 'center',
                    locale: {
                        last: 'Ok'
                    }
                }
            ]
        },
        end: {
            id: Ids.PREFERENCES_END,
            steps: [
                {
                    title: 'Just a moment to complete',
                    target: '#side-bar-host-item',
                    content: `
                        The last step of your registration is inform your data as Host.
                        Click here to go to Host screen
                    `,
                    disableBeacon: true,
                    disableOverlayClose: true,
                    placement: 'right',
                    spotlightClicks: false,
                    styles: {
                        options: {
                            zIndex: 10000,
                        },
                    },
                    locale: {
                        last: `Ok, I got it`
                    },
                }
            ]
        }
    },
    host: {
        start: {
            id: Ids.HOST_START,
            steps: [
                {
                    title: 'Host page',
                    target: '#host-title',
                    content: `
                This form is used to find travels that are compatible with you, help us understand what your accommodation and tastes are like so you can have a great experience.
            `,
                    placement: 'center',
                    locale: {
                        last: 'Ok'
                    }
                }
            ]
        },
        end: {
            id: Ids.HOST_END,
            steps: [
                {
                    title: 'Host page',
                    target: '#host-title',
                    content: `
                        Now you are ready to receive a travel, as soon as a travel finds you we will notify you.
                    `,
                    placement: 'center',
                    locale: {
                        last: 'Ok, I got it'
                    }
                }
            ]
        }
    }
}