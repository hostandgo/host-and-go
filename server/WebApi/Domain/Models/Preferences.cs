using System;
using System.Collections.Generic;
using System.Linq;
using WebApi.Domain.Enums;

namespace WebApi.Domain.Models
{
    public class Preferences : BaseEntity
    {
        protected Preferences() { }
        public Preferences(Guid userId, string description)
        {
            this.UserId = userId;
            this.Description = description;
        }
        public Guid UserId { get; private set; }
        public string Description { get; private set; }
        private HashSet<RestrictionsTypes> _restrictions = new HashSet<RestrictionsTypes>();
        public IReadOnlyCollection<RestrictionsTypes> Restrictions => _restrictions;
        private HashSet<Interest> _interests = new HashSet<Interest>();
        public IReadOnlyCollection<Interest> Interests => _interests;

        public Preferences UpdateRestrictions(IEnumerable<RestrictionsTypes> newRestrictions)
        {
            _restrictions.Clear();
            _restrictions.AddRange(newRestrictions);
            return this;
        }

        public Preferences AddOrUpdateInterests(IEnumerable<Interest> newInterests)
        {
            _interests.Clear();
            _interests.AddRange(newInterests);
            return this;
        }

        public Preferences UpdateDescription(string description)
        {
            Description = description;
            return this;
        }
    }
}