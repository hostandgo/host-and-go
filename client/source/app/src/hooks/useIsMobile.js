import { useState, useEffect } from 'react';

//https://gist.github.com/gaearon/cb5add26336003ed8c0004c4ba820eae#file-usewindowwidth-js

export default function useIsMobile() {
    const [width, setWidth] = useState(window.innerWidth);
    const limitWidth = 500;    
    useEffect(() => {
      const handleResize = () => setWidth(window.innerWidth);
      window.addEventListener('resize', handleResize);
      return () => {
        window.removeEventListener('resize', handleResize);
      };
    });
    return width < limitWidth;
}