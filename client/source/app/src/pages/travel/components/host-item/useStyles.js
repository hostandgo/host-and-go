import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(theme => ({
    mediumAvatar: {
        margin: 10,
        width: 100,
        height: 100,
    },
    margin: {
        margin: theme.spacing(1),
      },
      extendedIcon: {
        marginRight: theme.spacing(1),
      },
}))