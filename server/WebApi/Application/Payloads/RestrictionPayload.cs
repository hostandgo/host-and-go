using System;
using WebApi.Domain.Enums;
using WebApi.Domain.Models;

namespace WebApi.Application.Payloads
{
    public class RestrictionPayload
    {
        public RestrictionsTypes Type { get; set; }
        public string Complement { get; set; }
        public static Func<RestrictionPayload, Restriction> ToModel = rp => new Restriction(rp.Type, rp.Complement);
    }

}