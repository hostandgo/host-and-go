using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using WebApi.Domain.Autentication;

namespace WebApi.Web.Utils
{
    public class HttpContextAcessorParamExtractor
    {
        private IHttpContextAccessor ContextAccessor;

        public HttpContextAcessorParamExtractor(IHttpContextAccessor contextAccessor)
        {
            ContextAccessor = contextAccessor;
        }

        public string GetToken()
        {
            var bearerToken = GetHeaderValue("Authorization");
            var tokenPart = 1;
            var whitespace = ' ';

            if (string.IsNullOrWhiteSpace(bearerToken))
            {
                return null;
            }
            return bearerToken.Split(whitespace)[tokenPart];
        }

        public Guid GetUserGuid() => Guid.TryParse(GetClaimValue("UserId"), out Guid userId) ? userId : Guid.Empty;

        public Guid GetRole() => Guid.TryParse(GetClaimValue(Roles.Key), out Guid companyId) ? companyId : Guid.Empty;

        private string GetHeaderValue(string search)
            => Headers.FirstOrDefault(header => search.Equals(header.Key)).Value.ToString();

        private string GetClaimValue(string search)
            => Claims.Where(claim => claim.Type == search).FirstOrDefault()?.Value;

        private IHeaderDictionary Headers => ContextAccessor.HttpContext.Request.Headers;

        private IEnumerable<Claim> Claims => ContextAccessor.HttpContext.User.Claims;

    }
}