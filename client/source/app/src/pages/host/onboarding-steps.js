const getInitialOnboardingSteps = {
    id: 5,
    steps: [
        {
            title: 'Host page',
            target: '#host-title',
            content: `
                This form is used to find travels that are compatible with you, help us understand what your accommodation and tastes are like so you can have a great experience.
            `,
            placement: 'center',
            locale: {
                last: 'Ok'
            }
        }
    ]
}

const getAfterSave = {
    id: 6,
    steps: [
        {
            title: 'Host page',
            target: '#host-title',
            content: `
                Now you are ready to receive a travel, as soon as a travel finds you we will notify you.
            `,
            placement: 'center',
            locale: {
                last: 'Ok, I got it'
            }
        }
    ]
}

export { getInitialOnboardingSteps, getAfterSave }
