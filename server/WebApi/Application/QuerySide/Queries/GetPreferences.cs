using System;
using System.Threading;
using System.Threading.Tasks;
using WebApi.Application.ViewModels;
using WebApi.Domain.Exceptions;
using WebApi.Infrastructure;
using MediatR;
using MongoDB.Driver;

namespace WebApi.Application.QuerySide.Queries
{
    public class GetPreferencesQuery : IRequest<PreferencesViewModel>
    {
        public GetPreferencesQuery(Guid userId)
        {
            this.UserId = userId;

        }
        public Guid UserId { get; set; }
    }

    public class GetPreferencesQueryHandler : IRequestHandler<GetPreferencesQuery, PreferencesViewModel>
    {
        private readonly MongoContext _context;

        public GetPreferencesQueryHandler(MongoContext context)
        {
            _context = context;
        }

        public async Task<PreferencesViewModel> Handle(GetPreferencesQuery request, CancellationToken cancellationToken)
        {
            var preferenceResult = await _context.Preferences.FindAsync(p => p.UserId == request.UserId);
            var preference = await preferenceResult.FirstOrDefaultAsync();

            if(preference == null)
                throw new NotFoundException("Preference not found");

            return new PreferencesViewModel(
                preference.Description,
                preference.Restrictions,
                preference.Interests
            );
        }
    }
}