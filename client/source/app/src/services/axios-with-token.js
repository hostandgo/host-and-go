import axios from 'axios';
import { getAuthorizationHeader } from './auth';

export default axios.create({
    headers: {
        ...getAuthorizationHeader(),
        'Cache-Control': 'no-cache, no-store, must-revalidate',
        Pragma: 'no-cache',
        'Access-Control-Allow-Origin': '*',
    },
});

export const cached = axios.create({ headers: getAuthorizationHeader() });