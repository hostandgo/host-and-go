const interestViewModelToState = interests =>
  interests.map(i => ({
    interest: i,
    complementInterest: i.complements
  }));

const interestStateToPayload = interests =>
  interests.map(i => ({
    interest: i.interest.type,
    complementInterest: i.complementInterest.map(ci => ci.type)
  }));
export { interestViewModelToState, interestStateToPayload };
