using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using WebApi.Domain.Repositories;
using WebApi.Domain.Services;

namespace WebApi.Application.CommandSide.Commands
{
    public class UpdateFamilyPhotosCommand: IRequest
    {
        public Guid UserId {get; set; }
        public List<IFormFile> Files {get; set; }

        public UpdateFamilyPhotosCommand(Guid userId, List<IFormFile> files)
        {
            this.UserId = userId;
            this.Files = files;
        }
    }

    public class UpdateFamilyPhotosCommandHandler : IRequestHandler<UpdateFamilyPhotosCommand>
    {
        private readonly IProfileRepository _profileRepository;
        private readonly IFileService _fileService;

        public UpdateFamilyPhotosCommandHandler(IProfileRepository profileRepository, IFileService fileService)
        {
            _profileRepository = profileRepository;
            _fileService = fileService;
        }

        public async Task<Unit> Handle(UpdateFamilyPhotosCommand request, CancellationToken cancellationToken)
        {
            var profile = await _profileRepository.FindByUserId(request.UserId);
            var filesToAdded = request.Files.GetFilesToAdd(profile.FamilyPhotos);
            var filesToRemove = request.Files.GetFileNamesToRemove(profile.FamilyPhotos);
    
            if(filesToAdded.Any())
            {
                var fotosToAdded = await _fileService.UploadRangeFileAsync(filesToAdded);
                profile.AddPhotos(fotosToAdded);
            }

            if(filesToRemove.Any())
            {
                await _fileService.RemoveRangeFilesAsync(filesToRemove);
                profile.RemovePhotos(filesToRemove);
            }
            
            await _profileRepository.Update(profile);
            return Unit.Value;
        }
    }
}