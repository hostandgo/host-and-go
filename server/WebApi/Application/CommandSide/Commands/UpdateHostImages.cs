using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using WebApi.Domain.Models;
using WebApi.Domain.Repositories;
using WebApi.Domain.Services;

namespace WebApi.Application.CommandSide.Commands
{
    public class UpdateHostImagesCommand : IRequest
    {
        public Guid UserId {get; set; }
        public List<IFormFile> Files {get; set; }

        public UpdateHostImagesCommand(Guid userId, List<IFormFile> files)
        {
            this.UserId = userId;
            this.Files = files;
        }
    }

    public class UpdateHostImagesCommandHandler : IRequestHandler<UpdateHostImagesCommand>
    {
        private readonly IHostRepository _hostRepository;
        private readonly IFileService _fileService;

        public UpdateHostImagesCommandHandler(IHostRepository hostRepository, IFileService fileService)
        {
            _hostRepository = hostRepository;
            _fileService = fileService;
        }

        public async Task<Unit> Handle(UpdateHostImagesCommand request, CancellationToken cancellationToken)
        {
            var host = await _hostRepository.FindByUserId(request.UserId);
            var filesToAdded = request.Files.GetFilesToAdd(host.Photos);
            var filesToRemove = request.Files.GetFileNamesToRemove(host.Photos);

            
            if(filesToAdded.Any())
            {
                var fotosToAdded = await _fileService.UploadRangeFileAsync(filesToAdded);
                host.AddPhotos(fotosToAdded);

            }

            if(filesToRemove.Any())
            {
                await _fileService.RemoveRangeFilesAsync(filesToRemove);
                host.RemovePhotos(filesToRemove);
            }
            
            await _hostRepository.Update(host);
            return Unit.Value;
        }
    }
}