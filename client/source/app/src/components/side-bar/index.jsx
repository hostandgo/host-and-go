import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import { useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import SentimentalIcon from '@material-ui/icons/SentimentSatisfied';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import AirplanemodeActiveIcon from '@material-ui/icons/AirplanemodeActiveOutlined';
import HomeIcon from '@material-ui/icons/HomeOutlined';
import PermIdentityIcon from '@material-ui/icons/PermIdentity';
import useIsMobile from '../../hooks/useIsMobile';
import history from '../../services/history';
import useStyles from './styles';

const labels = {
    user: 'User',
    host: 'Host',
    travel: 'Travel',
    preferences: 'Preferences',
    profile: 'Profile'

}

const pages = {
    user: 'user',
    host: 'host',
    travel: 'travel',
    preferences: 'preferences',
    profile: 'profile'
}

export default function SideBar(props) {
    const classes = useStyles();
    const theme = useTheme();
    const isMobile = useIsMobile();
    const [current, setCurrent] = useState('');

    const redirectToRoute = (route) => {
        history.push(`/${route}`)
        props.handleDrawerClose();
        setCurrent(route);
    }

    return (
        <Drawer
            variant={isMobile ? null : "permanent"}
            className={clsx(classes.drawer, {
                [classes.drawerOpen]: props.open,
                [classes.drawerClose]: !props.open,
            })}
            classes={{
                paper: clsx({
                    [classes.drawerOpen]: props.open,
                    [classes.drawerClose]: !props.open,
                }),
            }}
            open={props.open}
        >
            <div className={classes.toolbar}>
                <IconButton id={`${props.open ? 'open' : 'close'}-side-bar`} onClick={props.handleDrawerClose}>
                    {
                        theme.direction === 'rtl'
                            ? <ChevronRightIcon />
                            : <ChevronLeftIcon />
                    }
                </IconButton>
            </div>
            <Divider />
            <List>
                <ListItem selected={current === 'profile'} button key={labels.profile} onClick={() => redirectToRoute(pages.profile)}>
                    <ListItemIcon>
                        <PermIdentityIcon id="side-bar-profile-item" />
                    </ListItemIcon>
                    {
                        props.open &&
                        <ListItemText primary={labels.user} />
                    }
                </ListItem>
                <ListItem selected={current === 'preferences'} id="side-bar-preferences-item" button key={labels.preferences} onClick={() => redirectToRoute(pages.preferences)}>
                    <ListItemIcon>
                        <SentimentalIcon />
                    </ListItemIcon>
                    {
                        props.open &&
                        <ListItemText primary={labels.preferences} />
                    }
                </ListItem>
                <ListItem selected={current === 'host'} id="side-bar-host-item" button key={labels.host} onClick={() => redirectToRoute(pages.host)}>
                    <ListItemIcon>
                        <HomeIcon />
                    </ListItemIcon>
                    {
                        props.open &&
                        <ListItemText primary={labels.host} />
                    }
                </ListItem>
                <ListItem selected={current === 'travel'} id="side-bar-travel-item" disabled button key={labels.travel} onClick={() => redirectToRoute(pages.travel)}>
                    <ListItemIcon>
                        <AirplanemodeActiveIcon />
                    </ListItemIcon>
                    {
                        props.open &&
                        <ListItemText primary={labels.travel} />
                    }
                </ListItem>
            </List>
        </Drawer>
    )
}