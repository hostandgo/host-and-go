using System.Linq;
using WebApi.Domain.Models;
using MongoDB.Driver;

namespace WebApi.Infrastructure
{
    public class ReadOnlyContext
    {
        private readonly IMongoDatabase _database = null;   
        public ReadOnlyContext(IDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            _database = client.GetDatabase(settings.DatabaseName);
        }
         
        public IQueryable<User> Users => _database.GetCollection<User>("Users").AsQueryable();
        public IQueryable<Profile> Profiles => _database.GetCollection<Profile>("Profile").AsQueryable();
        public IQueryable<Preferences> Preferences => _database.GetCollection<Preferences>("Preferences").AsQueryable();

    }
}