using WebApi.Onboarding.Infrastructure.Mappers;

namespace WebApi.Infrastructure.Mappers
{
    public class MapEntities
    {
        public static void Configure()
        {
            UserMapper.Configure();
            ProfileMapper.Configure();
            HostMapper.Configure();
            PreferencesMapper.Configure();
            UsageFlowMapper.Configure();
        }
    }
}