using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using WebApi.Domain.Autentication;
using WebApi.Infrastructure;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.AspNetCore.Http;
using WebApi.Web.Utils;

public static class ServiceCollectionExtension
{
    public static IServiceCollection AddAutentication(this IServiceCollection services)
    {
        var key = Encoding.ASCII.GetBytes(Settings.Secret);
        services.AddAuthorization(options =>
        {
            options.AddPolicy("travel", policy => policy.RequireClaim("HostAndGo", "travel"));
            options.AddPolicy("host", policy => policy.RequireClaim("HostAndGo", "host"));
        });
        services.AddAuthentication(x =>
        {
            x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
        })
        .AddJwtBearer(x => 
        {
            x.RequireHttpsMetadata = false;
            x.SaveToken = true;
            x.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false
            };
        });
        return services;
    }

    public static IServiceCollection AddSwagger(this IServiceCollection services)
    {
        services.AddSwaggerGen(c =>
        {
            var serviceName = Assembly.GetEntryAssembly().GetName().Name;
                c.SwaggerDoc("v1", new OpenApiInfo { Title = $"{serviceName} API Docs", Version = "v1" });

                // var security = new Dictionary<string, IEnumerable<string>> { { "Bearer", new string[] { } } };
                var security = new OpenApiSecurityRequirement();
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "Example: \"Bearer {token}\"",
                    Name = "Authorization",
                    In = ParameterLocation.Header
                });
                c.AddSecurityRequirement(security);
        });
        return services;
    }

    public static IServiceCollection ConfigContext(this IServiceCollection services, IConfiguration configuration)
    {
        services.Configure<DatabaseSettings>(
            configuration.GetSection(nameof(DatabaseSettings))
        );

        services.AddSingleton<IDatabaseSettings>(sp =>
            sp.GetRequiredService<IOptions<DatabaseSettings>>().Value
        );

        services.AddSingleton<MongoContext>();
        services.AddSingleton<ReadOnlyContext>();
        return services;
    }

    public static IServiceCollection AddRequestContext(this IServiceCollection services)
    {
        services.AddTransient(provider =>
        {
            var service = provider.GetService<IHttpContextAccessor>();
            var extractor = new HttpContextAcessorParamExtractor(service);

            return new RequestContext(
                extractor.GetToken(), 
                extractor.GetUserGuid()
            );
        });
        return services;
    }
}