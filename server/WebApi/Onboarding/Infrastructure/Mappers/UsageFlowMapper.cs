using MongoDB.Bson.Serialization;
using WebApi.Onboarding.Domain.Models;

namespace WebApi.Onboarding.Infrastructure.Mappers
{
    public class UsageFlowMapper
    {
        public static void Configure()
        {
            BsonClassMap.RegisterClassMap<UsageFlow>(map =>
                {
                    map.AutoMap();
                    map.MapField(uf => uf.UserId);
                    map.MapField(uf => uf.FlowId);
                }
            );
        }
    }
}