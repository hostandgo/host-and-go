import React, { useEffect } from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import { Avatar, Typography, TextField, Paper } from '@material-ui/core';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import Chip from '@material-ui/core/Chip';
import StarIcon from '@material-ui/icons/Star';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import { KeyboardDatePicker } from "@material-ui/pickers";
import langmap from 'langmap';
import CustomDropzone from '../../../../components/custom-dropzone';
import useWindowWidth from '../../../../hooks/useWindowWidth';
import useStyles from './useStyles';
import api from '../../../../api';
import countryAndCities from '../../../../services/countries-and-cities'
import { formatImageToView } from '../../../../services/image';
import Onboarding from '../../../../components/onboarding';
import onboardingSteps from './onboarding-steps';
const genres = [
    {
        value: '1',
        label: 'Man',
    },
    {
        value: '2',
        label: 'Woman',
    },
    {
        value: '3',
        label: 'Non-Binary',
    }
];

const allLanguages = Object.values(langmap);

const levelLanguages = [
    {
        value: 'Beginner',
        label: 'Beginner',
    },
    {
        value: 'Intermediate',
        label: 'Intermediate',
    },
    {
        value: 'Fluent',
        label: 'Fluent',
    }
];

const user = {
    firstName: "Odilo Jr",
    picture: "/static/images/user-pic.jpeg",
    stars: 2,
    authentication: {
        Role: 'Host'
    }
}

const countryItem = (country) => {
    return (
        <MenuItem key={country} value={country}>
            {country}
        </MenuItem>
    )
}

export default function ProfileTab(props) {
    const {
        setFirstName,
        setSecondName,
        setNationality,
        setDateOfBirth,
        setEmail,
        setGenre,
        setProfission,
        setNativeLanguage,
        setLanguages,
        setCity,
        setCountry,
        setDocument,
        setInstagram,
        setTwitter,
        setFacebook,
        setLinkedin,
        setOthers,
        setScreenLoading,
        familyPhotos,
        setFamilyPhotos
    } = props;
    const classes = useStyles();
    const windowWidth = useWindowWidth();
    const isMobile = windowWidth <= 900;

    useEffect(() => {
        async function getProfile() {
            const result = await api.profile.get(setScreenLoading)
            if (!result.hasError) {
                if(result.response.document)
                    setDocument([result.response.document].map(formatImageToView))

                setFirstName(result.response.firstName)
                setSecondName(result.response.secondName)
                setNationality(result.response.nationality)
                setDateOfBirth(result.response.dateOfBirth)
                setEmail(result.response.email)
                setGenre(result.response.genre)
                setProfission(result.response.profission)
                setNativeLanguage(result.response.nativeLanguage)
                setLanguages(result.response.languages)
                setCity(result.response.city)
                setCountry(result.response.country)
                setFamilyPhotos(result.response.familyPhotos.map(formatImageToView))
                setInstagram(result.response.instagram)
                setTwitter(result.response.twitter)
                setFacebook(result.response.facebook)
                setLinkedin(result.response.linkedin)
                setOthers(result.response.others)
            }
        }

        getProfile()
    }, [setScreenLoading, setFirstName, setSecondName, setNationality, setDateOfBirth, setEmail, setGenre, setProfission, setNativeLanguage, setLanguages, setCity, setCountry, setDocument, setInstagram, setTwitter, setFacebook, setLinkedin, setOthers, setFamilyPhotos])

    function addLanguage() {
        if (props.currentLanguage && props.level) {
            props.setLanguages([...props.languages, {
                name: props.currentLanguage,
                level: props.level
            }])
            props.setCurrentLanguage('')
            props.setLevel('');
        }
    }

    function removeLanguage(languageName) {
        props.setLanguages(props.languages.filter(l => l.name !== languageName))
    }

    return (
        <div className={isMobile ? classes.containerMobile : classes.containerDesktop}>
            <Onboarding customSteps={onboardingSteps}/>
            <div className={isMobile ? classes.containerFormMobile : classes.containerFormDesktop}>
                <Paper
                    style={{
                        padding: 8,
                        height: 100,
                        width:'100%',
                        marginBottom: 8
                    }}
                >
                    <Typography className="profile-title">Profile</Typography>
                    <Typography>Adicione suas informações pessoais, para que possamos encontrar hosts mais parecidos com você!</Typography>
                    <Typography>Informações públicas: Native language, Languages, First Name, Picture, Nationality, Date Of Birth e Country</Typography>
                </Paper>
                <Paper style={{width: '100%'}}>
                    <Typography className={classes.cardTitle} variant="h5" gutterBottom>Personal informations</Typography>
                    <div className={isMobile ? classes.topWrapperMobile : classes.topWrapperDesktop}>
                        <div className={classes.wrapAvatar}>
                            <Avatar
                                alt={user.name}
                                src={user.picture}
                                className={classes.bigAvatar}
                            />
                            <Typography>{user.authentication.Role}</Typography>
                            <div>
                                <StarIcon />
                                <StarIcon />
                                <StarIcon />
                                <StarIcon />
                                <StarBorderIcon />
                            </div>
                        </div>
                        <div className={isMobile ? classes.formOneMobile : classes.formOneDesktop}>
                            <TextField
                                id="outlined-first-name"
                                label="First Name"
                                className={isMobile ? classes.textFieldMobile : classes.textFieldDesktop}
                                value={props.firstName}
                                onChange={e => props.setFirstName(e.target.value)}
                                margin="normal"
                                variant="outlined"
                                disabled={props.disabled}
                            />

                            <TextField
                                id="outlined-second-name"
                                label="Second Name"
                                className={isMobile ? classes.textFieldMobile : classes.textFieldDesktop}
                                value={props.secondName}
                                onChange={e => props.setSecondName(e.target.value)}
                                margin="normal"
                                variant="outlined"
                                disabled={props.disabled}
                            />

                            <TextField
                                id="outlined-nationality"
                                select
                                label="Nationality"
                                className={isMobile ? classes.textFieldMobile : classes.textFieldDesktop}
                                value={props.nationality}
                                onChange={e => props.setNationality(e.target.value)}
                                margin="normal"
                                variant="outlined"
                                disabled={props.disabled}
                            >
                                {Object.keys(countryAndCities).map(countryItem)}
                            </TextField>

                            <KeyboardDatePicker
                                autoOk
                                variant="inline"
                                inputVariant="outlined"
                                margin="normal"
                                label="Date of Birth (MM/DD/YYYY)"
                                className={isMobile ? classes.textFieldMobile : classes.textFieldDesktop}
                                format="MM/dd/yyyy"
                                value={props.dateOfBirth}
                                InputAdornmentProps={{ position: "start" }}
                                onChange={date => props.setDateOfBirth(date)}
                                disabled={props.disabled}
                            />
                            <TextField
                                id="outlined-email"
                                label="E-mail"
                                className={isMobile ? classes.textFieldMobile : classes.textFieldDesktop}
                                value={props.email}
                                onChange={e => props.setEmail(e.target.value)}
                                margin="normal"
                                variant="outlined"
                                disabled={props.disabled}
                            />

                            <TextField
                                id="outlined-select-genre"
                                select
                                label="Genre"
                                className={isMobile ? classes.textFieldMobile : classes.textFieldDesktop}
                                value={props.genre}
                                onChange={e => props.setGenre(e.target.value)}
                                SelectProps={{
                                    MenuProps: {
                                        className: classes.menu,
                                    },
                                }}
                                margin="normal"
                                variant="outlined"
                                disabled={props.disabled}
                            >
                                {genres.map(option => (
                                    <MenuItem key={option.value} value={option.value}>
                                        {option.label}
                                    </MenuItem>
                                ))}
                            </TextField>
                        </div>
                    </div>
                </Paper>
                <div className={isMobile ? classes.formTwoMobile : classes.formTwoDesktop}>
                    <Paper style={{ width: '32%' }}>
                        <Typography className={classes.cardTitle} variant="h5" gutterBottom>Languages</Typography>
                        <div className={isMobile ? classes.languageWrapperMobile : classes.languageWrapperDesktop}>
                            <TextField
                                id="outlined-native-language"
                                select
                                label="Native language"
                                style={{ width: '100%', paddingLeft: 8, paddingRight: 8 }}
                                value={props.nativeLanguage}
                                onChange={e => setNativeLanguage(e.target.value)}
                                SelectProps={{
                                    MenuProps: {
                                        className: classes.menu,
                                    },
                                }}
                                margin="normal"
                                variant="outlined"
                                disabled={props.disabled}
                            >
                                {allLanguages.map(option => (
                                    <MenuItem key={option.englishName} value={option.englishName}>
                                        {option.englishName}
                                    </MenuItem>
                                ))}
                            </TextField>
                            <div
                                style={{
                                    display: 'flex',
                                    alignItems: 'center',
                                    flexWrap: 'wrap',
                                    width: '100%',
                                    paddingRight: '8px',
                                    paddingLeft: '8px'
                                }}
                            >
                                <TextField
                                    id="outlined-language"
                                    select
                                    label="Language"
                                    style={{ width: '100%' }}
                                    value={props.currentLanguage}
                                    onChange={e => props.setCurrentLanguage(e.target.value)}
                                    SelectProps={{
                                        MenuProps: {
                                            className: classes.menu,
                                        },
                                    }}
                                    margin="normal"
                                    variant="outlined"
                                    disabled={props.disabled}
                                >
                                    {allLanguages.map(option => (
                                        <MenuItem key={option.englishName} value={option.englishName}>
                                            {option.englishName}
                                        </MenuItem>
                                    ))}
                                </TextField>
                                <div style={{ display: 'flex', width: '100%', alignItems: 'center', justifyContent: 'space-around' }}>
                                    <TextField
                                        id="outlined-select-language-level"
                                        select
                                        label="Nivel"
                                        style={{ width: '80%' }}
                                        value={props.level}
                                        onChange={e => props.setLevel(e.target.value)}
                                        SelectProps={{
                                            MenuProps: {
                                                className: classes.menu,
                                            },
                                        }}
                                        margin="normal"
                                        variant="outlined"
                                        disabled={props.disabled}
                                    >
                                        {levelLanguages.map(option => (
                                            <MenuItem key={option.value} value={option.value}>
                                                {option.label}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                    <Fab onClick={addLanguage} color="primary" aria-label="add" className={classes.fabLanguage}>
                                        <AddIcon />
                                    </Fab>
                                </div>
                            </div>
                            <div>
                                {
                                    props.languages.map((l) =>
                                        <Chip
                                            key={l.name}
                                            label={`${l.name} - ${l.level}`}
                                            onDelete={() => removeLanguage(l.name)}
                                            className={classes.chip}
                                            color="primary"
                                        />
                                    )
                                }
                            </div>
                        </div>
                    </Paper>
                    <Paper
                        style={{
                            width: '32%',
                        }}
                        >
                        <Typography className={classes.cardTitle} variant="h5" gutterBottom>Your location</Typography>                        
                        <div
                            style={{
                                display: 'flex',
                                flexDirection: 'column',
                                width: '100%',
                            }}
                        >
                            <TextField
                                id="outlined-select-country"
                                select
                                label="Country"
                                className={isMobile && classes.textFieldMobile}
                                style={{ width: '100%', paddingLeft: 8, paddingRight: 8 }}
                                value={props.country}
                                onChange={e => props.setCountry(e.target.value)}
                                margin="normal"
                                variant="outlined"
                                disabled={props.disabled}
                            >
                                {Object.keys(countryAndCities).map(countryItem)}
                            </TextField>

                            <TextField
                                id="outlined-select-city"
                                style={{ width: '100%', paddingLeft: 8, paddingRight: 8 }}
                                select
                                label="City"
                                className={isMobile && classes.textFieldMobile}
                                value={props.city}
                                onChange={e => props.setCity(e.target.value)}
                                margin="normal"
                                variant="outlined"
                                disabled={props.disabled}
                            >
                                {
                                    props.country &&
                                    countryAndCities[props.country].map(countryItem)}
                            </TextField>
                        </div>

                    </Paper>
                    <Paper
                        style={{
                            width: '32%'
                        }}
                    >
                        <Typography className={classes.cardTitle} variant="h5" gutterBottom>Social media</Typography>
                        <div
                            style={{
                                display: 'flex',
                                flexDirection: 'column',
                                width: '100%',
                            }}
                        >
                            <TextField
                                id="outlined-facebook"
                                style={{ width: '100%', paddingLeft: 8, paddingRight: 8 }}
                                label="Facebook"
                                className={isMobile && classes.textFieldMobile}
                                value={props.facebook}
                                onChange={e => props.setFacebook(e.target.value)}
                                margin="normal"
                                variant="outlined"
                                disabled={props.disabled}
                            />

                            <TextField
                                id="outlined-twitter"
                                style={{ width: '100%', paddingLeft: 8, paddingRight: 8 }}
                                label="Twitter"
                                className={isMobile && classes.textFieldMobile}
                                value={props.twitter}
                                onChange={e => props.setTwitter(e.target.value)}
                                margin="normal"
                                variant="outlined"
                                disabled={props.disabled}
                            />

                            <TextField
                                id="outlined-instagram"
                                style={{ width: '100%', paddingLeft: 8, paddingRight: 8 }}
                                label="Instagram"
                                className={isMobile && classes.textFieldMobile}
                                value={props.instagram}
                                onChange={e => props.setInstagram(e.target.value)}
                                margin="normal"
                                variant="outlined"
                                disabled={props.disabled}
                            />

                            <TextField
                                id="outlined-linkedin"
                                style={{ width: '100%', paddingLeft: 8, paddingRight: 8 }}
                                label="Linkedin"
                                className={isMobile && classes.textFieldMobile}
                                value={props.linkedin}
                                onChange={e => props.setLinkedin(e.target.value)}
                                margin="normal"
                                variant="outlined"
                                disabled={props.disabled}
                            />
                        </div>
                    </Paper>
                </div>
                <div
                    style={{
                        display: 'flex',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        height: 255,
                        width: '100%',
                        marginTop: 8
                    }}
                >
                    <Paper
                        style={{
                            width: '32%',
                            display: 'flex',
                            flexDirection: 'column',
                            padding: 8,
                        }}
                    >
                        <Typography variant="h5" gutterBottom>Upload document photo</Typography>
                        <CustomDropzone files={props.document} setFiles={setDocument} multiple={false} />
                    </Paper>
                    <Paper
                        style={{
                            width: '66%',
                            display: 'flex',
                            flexDirection: 'column',
                            padding: 8,
                        }}
                    >
                        <Typography variant="h5" gutterBottom>Family Photos</Typography>
                        <CustomDropzone files={familyPhotos} setFiles={setFamilyPhotos} multiple />
                    </Paper>
                </div>
            </div>
        </div>
    )
}