import jwt from 'jsonwebtoken'
import history from '../services/history';

export const secret = "h0s74ndg0p455w0rd";
export const TOKEN_KEY = "@hot-and-go-Token";
export const ID_KEY = "@hot-and-go-UserId"

export const getToken = () => localStorage.getItem(TOKEN_KEY);
export const getUserId = () => localStorage.getItem(ID_KEY);

export const getAuthorizationHeader = () => ({ Authorization: `Bearer ${getToken()}` });


export const isAuthenticated = () => {
  const token = getToken();
  if(token === null)
    return false;
  
  const decodeToken = jwt.decode(token)
  if(decodeToken.exp < new Date().getTime()/1000)
    return false;

  return true; 
}

export const logout = () => {
  localStorage.removeItem(TOKEN_KEY);
  localStorage.removeItem(ID_KEY);
  history.push('./login')
};

export const login = (token, userId) => {
    localStorage.setItem(TOKEN_KEY, token);
    localStorage.setItem(ID_KEY, userId);
};
