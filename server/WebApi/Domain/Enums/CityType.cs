using System.ComponentModel;

namespace WebApi.Domain.Enums
{
    public enum CityType
    {
        [Description("Metropolis")]
        Metropolis = 1,

        [Description("Large city")]
        LargeCity = 2,

        [Description("Small urban area")]
        SmallUrbanArea = 3,

        [Description("Village and small town")]
        VillageAndSmallTown = 4,

        [Description("Rural area")]
        RuralArea = 5
    }
}