import React, { useState, useEffect } from 'react';
import { defaultDateFormat } from '../../services/date';
import DefaultWrapper from '../../components/default-wrapper';
import MenuItem from '@material-ui/core/MenuItem';
import CheckBoxMultiple from '../../components/check-box-multiple';
import { Paper, TextField, Typography } from '@material-ui/core';
import Chip from '@material-ui/core/Chip';
import FormGroup from '@material-ui/core/FormGroup';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Fab from '@material-ui/core/Fab';
import SmokingRoomsIcon from '@material-ui/icons/SmokingRooms';
import SmokingRoomsOutlinedIcon from '@material-ui/icons/SmokingRoomsOutlined';
import AccessibleIcon from '@material-ui/icons/Accessible';
import AccessibleOutlinedIcon from '@material-ui/icons/AccessibleOutlined';
import FavoriteIcon from '@material-ui/icons/Favorite';
import FavoriteOutLinedIcon from '@material-ui/icons/FavoriteBorderOutlined';
import PetsOutlinedIcon from '@material-ui/icons/PetsOutlined';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { KeyboardDatePicker } from '@material-ui/pickers';
import AddIcon from '@material-ui/icons/Add';
import useStyles from './useStyles';
import useWindowWidth from '../../hooks/useWindowWidth';
import api from '../../api';
import Loader from '../../components/loader';
import CustomDropzone from '../../components/custom-dropzone';
import { formatImageToView } from '../../services/image';
import InterestsCombos from '../../components/interests-combos';
import ButtonSaveContext from '../../components/button-save-context';
import Onboarding from '../../components/onboarding';
import { getInitialOnboardingSteps, getAfterSave } from './onboarding-steps';
import SimpleModal from '../../components/simple-modal';
import { interestViewModelToState, interestStateToPayload } from '../../components/interests-combos/mappers';

const citiesType = [
    {
        value: 1,
        description: 'Metropolis',
        tooltip: 'With more than one million inhabitants'
    },
    {
        value: 2,
        description: 'Large city',
        tooltip: 'The population is less than 1 million but over 300,000'
    },
    {
        value: 3,
        description: 'Small urban area',
        tooltip: 'The population is less than 300,000 but over 50,000'
    },
    {
        value: 4,
        description: 'Village and small town',
        tooltip: 'The population is less than 50,000'
    },
    {
        value: 5,
        description: 'Rural area',
        tooltip: 'Rutal area'
    }
]

const attractionsTypes = [
    {
        value: 1,
        description: 'Beaches'
    },
    {
        value: 2,
        description: 'Parks'

    },
    {
        value: 3,
        description: 'Theme parks'

    },
    {
        value: 4,
        description: 'Night clubs'
    },
    {
        value: 5,
        description: 'Bars and Restaurants'
    },
    {
        value: 6,
        description: 'Theatre'
    },
    {
        value: 7,
        description: 'Malls'
    },
    {
        value: 8,
        description: 'Concerts'
    },
    {
        value: 9,
        description: 'Great for outdoor activities'
    },
    {
        value: 10,
        description: 'Arenas and stadiums'
    },
    {
        value: 11,
        description: 'Sport Venues'
    },
    {
        value: 12,
        description: 'Mountains'
    },
    {
        value: 13,
        description: 'Museums'
    },
    {
        value: 14,
        description: 'Universities'
    },
    {
        value: 15,
        description: 'Colleges and Language Schools'
    },
    {
        value: 16,
        description: 'Wildlife and forest'
    },
    {
        value: 17,
        description: 'Hiking'
    },
    {
        value: 18,
        description: 'Ice sports'
    },
    {
        value: 19,
        description: 'Waterfalls'
    },
    {
        value: 20,
        description: 'Public Pools'
    },
    {
        value: 21,
        description: 'Recreational areas'
    },
    {
        value: 22,
        description: 'River'
    },
    {
        value: 23,
        description: 'Zoo'
    }
]

const roomTypes = [
    {
        value: 1,
        label: 'Private'
    },
    {
        value: 2,
        label: 'Shared'
    }
]

const agePreferred = [
    {
        value: 1,
        description: `Don't have`
    },
    {
        value: 2,
        description: `18 - 25`
    },
    {
        value: 3,
        description: `26 - 35`
    },
    {
        value: 4,
        description: `36 - 50`
    },
    {
        value: 5,
        description: `Above 50`
    },
]

const restrictionTypes = [
    { label: 'Smoke', value: 1, checked: false, icon: <SmokingRoomsOutlinedIcon fontSize="large" />, checkedIcon: <SmokingRoomsIcon fontSize="large" /> },
    { label: 'Accessibility', value: 2, checked: false, icon: <AccessibleOutlinedIcon fontSize="large" />, checkedIcon: <AccessibleIcon fontSize="large" /> },
    { label: 'Religion', value: 3, checked: false, icon: <FavoriteOutLinedIcon fontSize="large" />, checkedIcon: <FavoriteIcon fontSize="large" /> },
    { label: 'Pet', value: 4, checked: false, icon: <PetsOutlinedIcon fontSize="large" />, checkedIcon: <PetsOutlinedIcon fontSize="large" /> }
]
export default function Host() {
    const classes = useStyles();
    const windowWidth = useWindowWidth();
    const isMobile = windowWidth <= 900;

    const [disabled, setDisabled] = useState(false);
    const [showSaveButton, setShowSaveButton] = useState(false);
    const [addres, setAddres] = useState('');
    const [description, setDescription] = useState('');
    const [roomType, setRoomType] = useState('');
    const [bathroomAccess, setBathroomAccess] = useState('');
    const [currentStartDate, setCurrentStartDate] = useState(null);
    const [currentEndDate, setCurrentEndDate] = useState(null);
    const [preferredAgeRange, setPreferredAgeRange] = useState([]);
    const [timesAvailableToReceive, setTimesAvailableToReceive] = useState([]);
    const [hoursInteractTravel, setHoursInteractTravel] = useState(0);
    const [describeWhatCanDoTogether, setDescribeWhatCanDoTogether] = useState('');
    const [describeWhatYourCity, setDescribeWhatYourCity] = useState('');
    const [attractions, setAttractions] = useState([]);
    const [restrictions, setRestrictions] = useState(restrictionTypes);
    const [interests, setInterests] = useState([]);
    const [success, setSuccess] = useState(false);
    const [loading, setLoading] = useState(false);
    const [cityType, setCityType] = useState([]);
    const [attraction] = useState([]);
    const [currentOnboarding, setCurrentOnboarding] = useState([])
    const [files, setFiles] = useState([])
    const [showOnboarding, setShowOnBoarding] = useState(false);
    const [globalLoading, setGlobalLoading] = useState(false);
    const [errors, setErrors] = useState(false);
    const [openRestrictionComplementModal, setOpenRestrictionComplementModal] = useState(false);
    const [currentRestrictionComplement, setCurrentRestrictionComplement] = useState('');
    const [isLoadingFiles, setIsLoadingFiles] = useState(false);
    function validateForm() {
        let hasError = false;

        if (!addres) hasError = true;
        if (!description) hasError = true;
        if (!roomType) hasError = true;
        if (!bathroomAccess) hasError = true;
        if (preferredAgeRange.length === 0) hasError = true;
        if (!hoursInteractTravel) hasError = true;
        if (!describeWhatCanDoTogether) hasError = true;
        if (!describeWhatYourCity) hasError = true;
        if (attractions.length === 0) hasError = true;

        return hasError;
    }

    useEffect(() => () => {
        // Make sure to revoke the data uris to avoid memory leaks
        files.forEach(file => URL.revokeObjectURL(file.preview));
    }, [files]);

    function removeadAtrraction(attractionToRemove) {
        setAttractions(attractions.filter(a =>
            a.type !== attractionToRemove.type
        ));
    }

    async function saveChanges() {
        setErrors(false);
        var hasError = validateForm();
        if (hasError) {
            setErrors(hasError);
            return;
        }

        if (!loading) {
            setSuccess(false);
            await api.host.update(
                {
                    addres,
                    description,
                    roomType: roomType ? roomType : 0,
                    bathroomAccess: bathroomAccess ? bathroomAccess : 0,
                    preferredAgeRange: preferredAgeRange.map(a => a.value),
                    timesAvailableToReceive,
                    hoursInteractTravel: parseInt(hoursInteractTravel),
                    describeWhatCanDoTogether,
                    describeWhatYourCity,
                    cityType: cityType.map(a => a.value),
                    interests: interestStateToPayload(interests),
                    attractions: attractions.map(a => a.value),
                    restrictions: restrictions.filter(r => r.checked).map(r => ({ type: r.value, complement: r.complement })),
                },
                setLoading
            )

            let formData = new FormData();
            files.forEach((file) => {
                formData.append('files', file);
            }
            )
            await api.host.updateImages(formData, setIsLoadingFiles)
            setSuccess(true);
            setTimeout(() => {
                setShowSaveButton(false);
                setSuccess(false);
            }, 2000);

            setCurrentOnboarding(getAfterSave);
            setShowOnBoarding(true);
        }
    }

    useEffect(() => {
        async function getHostDetails() {
            const result = await api.host.get(setGlobalLoading);
            if (!result.hasError) {
                setAddres(result.response.addres)
                setDescription(result.response.description)
                setRoomType(result.response.roomType)
                setBathroomAccess(result.response.bathroomAccess)
                setPreferredAgeRange(result.response.preferredAgeRange);
                setTimesAvailableToReceive(result.response.timesAvailableToReceive.map(t => ({ start: new Date(t.start), end: new Date(t.end) })));
                setHoursInteractTravel(result.response.hoursInteractTravel);
                setDescribeWhatCanDoTogether(result.response.describeWhatCanDoTogether);
                setRestrictions(restrictionTypes.map(r => result.response.restrictions.includes(r.value) ? { ...r, checked: true } : r))
                setCityType(result.response.cityType);
                setDescribeWhatYourCity(result.response.describeWhatYourCity)
                setAttractions(result.response.attractions);
                const mapInterestToViewModel = interestViewModelToState(result.response.interests);
                setInterests(mapInterestToViewModel.length > 0 ? mapInterestToViewModel : []);
            } else {
                setCurrentOnboarding(getInitialOnboardingSteps)
                setShowOnBoarding(true);
            }
        }

        getHostDetails();
    }, [])

    useEffect(() => {
        async function getImage() {
            var result = await api.host.getPhotos(() => { });
            if (!result.hasError) {
                setFiles(result.response.map(formatImageToView))
            }
        }
        getImage();
    }, [])

    const handleSaveRestrictionComplement = () => {
        setRestrictions(restrictions.map(r => r.label === 'Religion' ? { ...r, complement: currentRestrictionComplement } : r));
        setCurrentRestrictionComplement('');
        setOpenRestrictionComplementModal(false);
    }

    const handleRestrictionsCheckBoxes = name => event => {
        if (name === 'Religion')
            setOpenRestrictionComplementModal(true);

        setRestrictions(restrictions.map(r => r.label === name ? { ...r, checked: event.target.checked, complement: '' } : r));
    };

    function removeDate(date) {
        setTimesAvailableToReceive(timesAvailableToReceive.filter(
            d => d.start !== date.start
                && d.end !== date.end
        ))
    }

    function addDate() {
        if (currentStartDate && currentEndDate) {
            setTimesAvailableToReceive([...timesAvailableToReceive,
            {
                start: currentStartDate,
                end: currentEndDate
            }])
            setCurrentStartDate(null)
            setCurrentEndDate(null)
        }
    }

    function handleHoursInteractTravel(value) {
        if (value < 0)
            return;

        setHoursInteractTravel(value)
    }

    function handleCheckboxMulti(item, set) {
        const itemsWithoutDuplicates = item.filter(i => item.filter(_i => i.value === _i.value).length <= 1)
        set(itemsWithoutDuplicates)
    }

    return (
        <DefaultWrapper>
            <Onboarding run={showOnboarding} customSteps={currentOnboarding} />

            <div className={classes.root}>
                <Typography id="host-title" variant="h4" gutterBottom>Host</Typography>
                <div
                    style={{
                        marginBottom: 15,
                        width: '100%'
                    }}
                >
                    <Typography>
                        This information will be displayed to travelers, the more information you add, the more likely a traveler with the same characteristics will find you.
                    </Typography>
                </div>
                <Paper style={{ width: '100%' }}>
                    <Typography style={{ padding: 8 }} variant="h5" gutterBottom>Accommodation Details</Typography>
                    <div className={isMobile ? classes.paperMobile : classes.paperDesktop}>

                        <div className={isMobile ? classes.mobileItem : classes.firstRow}>
                            <div className={isMobile ? classes.mobileItem : classes.addresDescription}>
                                <TextField
                                    error={errors && !addres}
                                    helperText={(errors && !addres) && "Addres is required"}
                                    id="outlined-addres"
                                    label="Addres"
                                    className={isMobile ? classes.textFieldMobile : classes.textFieldCustom}
                                    value={addres}
                                    onChange={e => setAddres(e.target.value)}
                                    margin="normal"
                                    variant="outlined"
                                    disabled={disabled}
                                />
                                <TextField
                                    error={errors && !roomType}
                                    helperText={(errors && !roomType) && "Type of room is required"}
                                    id="outlined-select-room-type"
                                    select
                                    label="Type of room"
                                    className={isMobile ? classes.textFieldMobile : classes.comboCustom}
                                    value={roomType}
                                    onChange={e => setRoomType(e.target.value)}
                                    SelectProps={{
                                        MenuProps: {
                                            className: classes.menu,
                                        },
                                    }}
                                    margin="normal"
                                    variant="outlined"
                                    disabled={disabled}
                                >
                                    {roomTypes.map(option => (
                                        <MenuItem key={option.value} value={option.value}>
                                            {option.label}
                                        </MenuItem>
                                    ))}
                                </TextField>

                                <TextField
                                    error={errors && !bathroomAccess}
                                    helperText={(errors && !bathroomAccess) && "Bathroom access is required"}
                                    id="outlined-select-bathroom-access"
                                    select
                                    label="Bathroom access"
                                    className={isMobile ? classes.textFieldMobile : classes.comboCustom}
                                    value={bathroomAccess}
                                    onChange={e => setBathroomAccess(e.target.value)}
                                    SelectProps={{
                                        MenuProps: {
                                            className: classes.menu,
                                        },
                                    }}
                                    margin="normal"
                                    variant="outlined"
                                    disabled={disabled}
                                >
                                    {roomTypes.map(option => (
                                        <MenuItem key={option.value} value={option.value}>
                                            {option.label}
                                        </MenuItem>
                                    ))}
                                </TextField>

                                <TextField
                                    error={errors && !description}
                                    placeholder="Describe what accommodation, rounding and other information you think are important."
                                    helperText={(errors && !description) && "Description is required"}
                                    id="outlined-description"
                                    label="Description"
                                    className={isMobile ? classes.textFieldMobile : classes.textFieldCustom}
                                    value={description}
                                    onChange={e => setDescription(e.target.value)}
                                    multiline
                                    rows="4"
                                    rowsMax="4"
                                    margin="normal"
                                    variant="outlined"
                                    disabled={disabled}
                                />
                                <CheckBoxMultiple
                                    className={isMobile ? classes.textFieldMobile : classes.comboCustom}
                                    label='Preferred age range'
                                    collection={agePreferred}
                                    errors={errors}
                                    key={'value'}
                                    menuValue={'value'}
                                    description={'description'}
                                    value={preferredAgeRange}
                                    onChange={e => handleCheckboxMulti(e.target.value, setPreferredAgeRange)}
                                />
                            </div>
                        </div>
                        <div className={isMobile ? classes.mobileItem : classes.secondRow}>
                            <div className={isMobile ? classes.mobileItem : classes.datesSection}>
                                <FormLabel style={{ marginRight: 8, marginLeft: 8 }} component="legend">Time available to receive</FormLabel>
                                <div className={isMobile ? classes.mobileItem : classes.dataPickers}>
                                    <KeyboardDatePicker
                                        disabled={disabled}
                                        autoOk
                                        inputVariant="outlined"
                                        variant="inline"
                                        className={isMobile ? classes.textFieldMobile : classes.datePicker}
                                        format="MM/dd/yyyy"
                                        margin="normal"
                                        id="date-picker-inline"
                                        label="Start"
                                        value={currentStartDate}
                                        onChange={value => setCurrentStartDate(value)}
                                    />
                                    <KeyboardDatePicker
                                        disabled={disabled}
                                        autoOk
                                        inputVariant="outlined"
                                        variant="inline"
                                        className={isMobile ? classes.textFieldMobile : classes.datePicker}
                                        format="MM/dd/yyyy"
                                        margin="normal"
                                        id="date-picker-inline"
                                        label="End"
                                        value={currentEndDate}
                                        onChange={value => setCurrentEndDate(value)}
                                    />
                                    <Fab onClick={addDate} color="primary" aria-label="add" className={classes.fabLanguage}>
                                        <AddIcon />
                                    </Fab>
                                </div>
                                <div>
                                    {
                                        timesAvailableToReceive.map((d) =>
                                            <Chip
                                                label={`From: ${defaultDateFormat(d.start)} Until: ${defaultDateFormat(d.end)}`}
                                                onDelete={() => removeDate(d)}
                                                className={classes.chip}
                                                color="primary"
                                            />
                                        )
                                    }
                                </div>
                            </div>
                            <div
                                className={isMobile ? classes.mobileItem : classes.restrictions}
                            >

                                <FormControl component="fieldset" >
                                    <FormLabel component="legend">It has restriction with:</FormLabel>
                                    <FormGroup aria-label="position" name="position" row>
                                        {
                                            restrictions.map((restriction) =>
                                                <FormControlLabel
                                                    key={restriction.value}
                                                    control={
                                                        <Checkbox
                                                            icon={restriction.icon}
                                                            checkedIcon={restriction.checkedIcon}
                                                            value={restriction.value}
                                                            onChange={handleRestrictionsCheckBoxes(restriction.label)}
                                                            checked={restriction.checked}
                                                        />
                                                    }
                                                    label={restriction.label}
                                                    labelPlacement="bottom"
                                                />
                                            )
                                        }
                                    </FormGroup>
                                </FormControl>
                            </div>
                        </div>
                    </div>
                </Paper>
                <Paper className={isMobile ? classes.paperMobile : classes.paperDesktopTwo}>
                    <Typography variant="h5" gutterBottom>Enter your city/region details</Typography>
                    <div className={isMobile ? classes.mobileItem : classes.combosSection}>
                        <InterestsCombos
                            interests={interests}
                            setInterests={setInterests}
                            disabled={disabled}
                        />
                        <CheckBoxMultiple
                            label='City type'
                            className={isMobile ? classes.textFieldMobile : classes.comboFieldDesktop}
                            collection={citiesType}
                            key={'value'}
                            errors={errors}
                            menuValue={'value'}
                            description={'description'}
                            value={cityType}
                            onChange={e => handleCheckboxMulti(e.target.value, setCityType)}
                        />
                        <CheckBoxMultiple
                            label='Attraction'
                            collection={attractionsTypes}
                            key={'value'}
                            errors={errors}
                            className={isMobile ? classes.textFieldMobile : classes.comboFieldDesktop}
                            menuValue={'value'}
                            description={'description'}
                            value={attractions}
                            onChange={e => handleCheckboxMulti(e.target.value, setAttractions)}
                        />
                    </div>
                    <TextField
                        error={errors && !describeWhatYourCity}
                        helperText={(errors && !describeWhatYourCity) && "This field is required"}
                        id="outlined-describe-what-your-city."
                        label="Describe what your city is like"
                        className={isMobile ? classes.textFieldMobile : classes.textFieldDescribe}
                        value={describeWhatYourCity}
                        onChange={e => setDescribeWhatYourCity(e.target.value)}
                        multiline
                        rows="4"
                        rowsMax="4"
                        margin="normal"
                        variant="outlined"
                        disabled={disabled}
                    />
                </Paper>
                <Paper className={isMobile ? classes.paperMobile : classes.paperDesktopTwo}>
                    <Typography variant="h5" gutterBottom>Interact with travel</Typography>
                    <div className={isMobile ? classes.mobileItem : classes.hourInputLabel}>
                        <Typography variant="subtitle1" gutterBottom>Time available per day to interact with travel: </Typography>
                        <TextField
                            error={errors && !hoursInteractTravel}
                            helperText={(errors && !hoursInteractTravel) && "Hours is required"}
                            id="standard-number"
                            label="Hours"
                            value={hoursInteractTravel}
                            onChange={e => handleHoursInteractTravel(e.target.value)}
                            type="number"
                            variant="outlined"
                            className={classes.textFieldHours}
                            margin="normal"
                            disabled={disabled}
                        />
                    </div>
                    <TextField
                        error={errors && !describeWhatCanDoTogether}
                        helperText={(errors && !describeWhatCanDoTogether) && "This field is required"}
                        id="outlined-describe-can-do-together."
                        label="Describe what you can do together."
                        className={isMobile ? classes.textFieldMobile : classes.textFieldDescribe}
                        value={describeWhatCanDoTogether}
                        onChange={e => setDescribeWhatCanDoTogether(e.target.value)}
                        multiline
                        rows="4"
                        rowsMax="4"
                        margin="normal"
                        variant="outlined"
                        disabled={disabled}
                    />
                </Paper>
                <Paper className={isMobile ? classes.paperMobile : classes.paperDesktopTwo}>
                    <Typography variant="h5" gutterBottom>Photos</Typography>
                    <Typography>Show how beautiful your home is (add a maximum of 10 photos)</Typography>
                    <CustomDropzone
                        files={files}
                        setFiles={setFiles}
                        limitAmount={10}
                        isLoading={isLoadingFiles}
                        multiple />
                </Paper>
            </div>
            {
                globalLoading &&
                <Loader
                    style={{
                        top: '45%',
                        left: '50%',
                        position: 'absolute'
                    }}
                    size={80}
                />
            }

            <ButtonSaveContext
                onSave={saveChanges}
                success={success}
                loading={loading}
            />
            <SimpleModal
                open={openRestrictionComplementModal}
                onClose={handleSaveRestrictionComplement}
                title={'Complement'}
                textSecondary={'Inform a complement about this restriction.'}
                onConfirm={handleSaveRestrictionComplement}
                onCancel={handleSaveRestrictionComplement}
            >
                <TextField
                    id="complement-restriction"
                    fullWidth
                    value={currentRestrictionComplement}
                    onChange={e => setCurrentRestrictionComplement(e.target.value)}
                    margin="normal"
                />
            </SimpleModal>
        </DefaultWrapper>
    )
}