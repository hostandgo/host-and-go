using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebApi.Application.Payloads;
using WebApi.Domain.Enums;
using WebApi.Domain.Models;
using WebApi.Domain.Repositories;
using WebApi.Domain.ValueObjects;
using MediatR;

namespace WebApi.Application.CommandSide.Commands
{
    public class UpdateHostDetailsCommand : IRequest
    {
        public Guid UserId { get; set; }
        public string Addres { get; set; }
        public string Description { get; set; }
        public TypeOfRoom? RoomType { get; set; }
        public BathroomAccess? BathroomAccess { get; set; }
        public IEnumerable<PreferredAgeRange> PreferredAgeRange { get; set; }
        public IEnumerable<TimeAvailableToReceive> TimesAvailableToReceive { get; set; }
        public int HoursInteractTravel { get; set; }
        public string DescribeWhatCanDoTogether { get; set; }
        public string DescribeWhatYourCity { get; set; }
        public IEnumerable<CityType> CityType { get; set; }
        public IEnumerable<RestrictionPayload> Restrictions { get; set; }
        public IEnumerable<InterestPayload> Interests { get; set; }
        public IEnumerable<AttractionType> Attractions { get; set; }
    }

    public class UpdateHostDetailsCommandHandler : IRequestHandler<UpdateHostDetailsCommand>
    {
        private readonly IHostRepository _hostRepository;

        public UpdateHostDetailsCommandHandler(IHostRepository hostRepository)
        {
            _hostRepository = hostRepository;
        }

        public async Task<Unit> Handle(UpdateHostDetailsCommand request, CancellationToken cancellationToken)
        {
            var host = await _hostRepository.FindByUserId(request.UserId);
            var interestsToUpdate = request.Interests
                .Select(item => new Interest(
                    item.Interest,
                    item.ComplementInterest
                        .Select(ci => 
                            new ComplementInterest(ci))
                )
            );

            if (host == null)
            {
                var newHost = new Host(
                    request.UserId,
                    request.Addres,
                    request.Description,
                    request.RoomType,
                    request.BathroomAccess,
                    request.HoursInteractTravel,
                    request.DescribeWhatCanDoTogether,
                    request.DescribeWhatYourCity,
                    request.PreferredAgeRange,
                    request.CityType,
                    request.Attractions,
                    interestsToUpdate,
                    request.TimesAvailableToReceive,
                    request.Restrictions.Select(RestrictionPayload.ToModel)
                );
                await _hostRepository.Insert(newHost);
            }
            else
            {
                host.Update(
                    request.Addres,
                    request.Description,
                    request.RoomType,
                    request.BathroomAccess,
                    request.HoursInteractTravel,
                    request.DescribeWhatCanDoTogether,
                    request.DescribeWhatYourCity,
                    request.PreferredAgeRange,
                    request.CityType,
                    request.Attractions,
                    interestsToUpdate,
                    request.TimesAvailableToReceive,
                    request.Restrictions.Select(RestrictionPayload.ToModel)
                );
                await _hostRepository.Update(host);
            }
            return Unit.Value;
        }
    }
}