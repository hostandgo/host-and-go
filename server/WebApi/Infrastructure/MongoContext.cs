using System;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using WebApi.Domain.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using WebApi.Onboarding.Domain.Models;

namespace WebApi.Infrastructure
{
    public class MongoContext
    {
        private readonly IMongoDatabase _database = null;
        public MongoContext(IDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            _database = client.GetDatabase(settings.DatabaseName);
        }

        public IMongoCollection<User> Users => _database.GetCollection<User>("Users");
        public IMongoCollection<Profile> Profiles => _database.GetCollection<Profile>("Profiles");
        public IMongoCollection<Preferences> Preferences => _database.GetCollection<Preferences>("Preferences");
        public IMongoCollection<Host> Hosts => _database.GetCollection<Host>("Hosts");
        public IMongoCollection<UsageFlow> UsageFlows => _database.GetCollection<UsageFlow>("UsageFlow"); 
    }
}