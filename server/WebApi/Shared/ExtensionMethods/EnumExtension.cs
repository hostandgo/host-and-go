using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

public static class EnumExtension
{
    public static bool EnumIsValid<TEnum>(this TEnum valorEnum)
    {
        return Enum.IsDefined(typeof(TEnum), valorEnum);
    }

    public static string GetDescription(this Enum value)
    => value
        .GetType()
        .GetMember(value.ToString())
        .FirstOrDefault()
        ?.GetCustomAttribute<DescriptionAttribute>()
        ?.Description;      
}
