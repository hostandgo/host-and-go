using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebApi.Application.Payloads;
using WebApi.Domain.Enums;
using WebApi.Domain.Models;
using WebApi.Domain.Repositories;
using MediatR;

namespace WebApi.Application.CommandSide.Commands
{
    public class UpdatePreferencesCommand : IRequest
    {
        public Guid UserId { get; set; }
        public string Description { get; set; }
        public IEnumerable<RestrictionsTypes> Restrictions { get; set; }
        public IEnumerable<InterestPayload> Interests { get; set; }
    }
    public class UpdatePreferencesCommandHandler : IRequestHandler<UpdatePreferencesCommand>
    {
        private readonly IPreferencesRepository _preferencesRepository;

        public UpdatePreferencesCommandHandler(IPreferencesRepository preferencesRepository)
        {
            _preferencesRepository = preferencesRepository;
        }

        public async Task<Unit> Handle(UpdatePreferencesCommand request, CancellationToken cancellationToken)
        {
            var preferences = await _preferencesRepository.FindByUserId(request.UserId);
            var interestsToUpdate = request.Interests
                .Select(item => new Interest(
                    item.Interest,
                    item.ComplementInterest
                        .Select(ci => new ComplementInterest(ci))
                )
            );

            if (preferences == null)
            {
                preferences = new Preferences(request.UserId, request.Description)
                    .AddOrUpdateInterests(interestsToUpdate);

                await _preferencesRepository.Insert(preferences);
            }
            else
            {
                preferences
                    .UpdateDescription(request.Description)
                    .AddOrUpdateInterests(interestsToUpdate);

                await _preferencesRepository.Update(preferences);
            }
            return Unit.Value;
        }
    }
}