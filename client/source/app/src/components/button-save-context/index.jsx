import React from 'react';
import Fab from '@material-ui/core/Fab';
import EditIcon from '@material-ui/icons/Edit';
import SaveIcon from '@material-ui/icons/Save';
import CheckIcon from '@material-ui/icons/Check';
import CircularProgress from '@material-ui/core/CircularProgress';
import useStyles from './useStyles';

export default function ButtonSaveContext({
    onSave,
    showSaveButton,
    success,
    loading,
    edit
}) {
    const classes = useStyles();
    return (
        <div
            style={{
                position: 'fixed',
                top: 10,
                right: 10,
                marginTop: 100
            }}
        >
            <div className={classes.wrapperSaveButton}>
                <Fab id="save-button" onClick={onSave} color="primary" aria-label="add" className={classes.fab}>
                    {success ? <CheckIcon /> : <SaveIcon />}
                </Fab>
                {loading && <CircularProgress size={68} className={classes.fabProgress} />}
            </div>
            {/* <Fab onClick={edit} color="secondary" aria-label="add" className={classes.fab}>
                <EditIcon />
            </Fab> */}
        </div>
    )
}