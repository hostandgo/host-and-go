using System;

namespace WebApi.Domain.Models
{
    public class BaseEntity
    {
        public BaseEntity() 
        {
            this.Id = Guid.NewGuid();
            CreatedAt = DateTime.Now;
        }
        
        public Guid Id { get; private set; }
        public DateTime CreatedAt { get; set; }
    }
}