using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using WebApi.Domain.Models;

namespace WebApi.Domain.Services
{
    public interface IFileService
    {
        Task<List<string>> UploadRangeFileAsync(IEnumerable<IFormFile> formFiles);
        Task<string> UploadFileAsync(IFormFile file);
        Task RemoveRangeFilesAsync(IEnumerable<string> keys);
    }
}