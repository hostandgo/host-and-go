import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'auto',
        maxWidth: 710,
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
    gridList: {
        flexWrap: 'nowrap !important',
        // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
        transform: 'translateZ(0)',
        width: '90%'
    },
    titleBar: {
        background: 'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
    },
    title: {
        color: 'white',
    },
    item: {
        margin: 10
    },
    modal: {
        width: '70%', 
        margin: '10px auto auto auto', 
        maxWidth:800, 
        backgroundColor: theme.palette.background.paper
    },
    img: {
        objectFit: 'cover',
        textAlign: 'center',
        display: 'block',
        height: 176,
        width: '250px !important',
    }
}))