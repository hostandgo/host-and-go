import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(theme => ({
    root: {
        height: '100vh',
    },
    image: {
        backgroundImage: 'url(https://source.unsplash.com/featured/?nature)',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    },
    paper: {
        margin: theme.spacing(8, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: 30,
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    box: {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column'
    },
    forgotPassword: {
        width: '100%',
        marginTop: 60,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column'
    },
    forgotPasswordText: {
        display: 'flex',
        alignItems: 'flex-start',
        flexDirection: 'column',
    }
}));