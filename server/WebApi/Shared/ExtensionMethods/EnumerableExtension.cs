using System;
using System.Collections.Generic;
using WebApi.Shared.Utils;

public static class EnumerableExtension
{
    public static HashSet<T> ToHashSet<T>(this IEnumerable<T> source)
    {
        return new HashSet<T>(source);
    }

    public static HashSet<T> ToHashSet<T>(
        this IEnumerable<T> source,
        Func<T, T, bool> metodoEquals, 
        Func<T, int> metodoGetHashCode)
    {
        return new HashSet<T>(source, GenericComparer<T>.Create(metodoEquals, metodoGetHashCode));
    }
}
