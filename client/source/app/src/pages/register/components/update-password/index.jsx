import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import api from '../../../../api';
import history from '../../../../services/history';
import Typography from '@material-ui/core/Typography';
import ButtonWithLoading from '../../../../components/button-with-loading';
import { login } from '../../../../services/auth';

export default function UpdatePassword(props){
    const { classes } = props;
    const [password, setPassword] = useState('');
    const [hasError] = useState(false);
    const [confirmPassword, setConfirmPassword] = useState('');
    const [loading, setLoading] = useState(false);

    async function updatePassword(){
        const result = await api.authentication.updatePassword({ email: props.email, password }, setLoading)
        if(!result.hasError)
        {
            const { token, userId } = result.response;
            login(token, userId);
            history.push('/profile');
        }
    }

    return(
        <form className={classes.form} noValidate >
            <div className={classes.forgotPasswordText}>
                <Typography component="h3">
                    Enter your new password
                </Typography>
            </div>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <TextField
                        variant="outlined"
                        required
                        fullWidth
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                        name="password"
                        label="Password"
                        type="password"
                        id="password"
                        autoComplete="current-password"
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        error={hasError}
                        variant="outlined"
                        required
                        fullWidth
                        value={confirmPassword}
                        onChange={e => setConfirmPassword(e.target.value)}
                        helperText={hasError && "Incorrect confirmation"}
                        name="confirmPassword"
                        label="Confirm password"
                        type="password"
                        id="confirmPassword"
                        autoComplete="current-password"
                    />
                </Grid>
            </Grid>
            <ButtonWithLoading
                loading={loading}
                handleButtonClick={async () => await updatePassword()}
                label={'Save new password'}        
            />
        </form>
    )
}