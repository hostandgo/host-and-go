const getInitialOnboardingSteps = {
    id: 1,
    steps: [
        {
            title: 'Welcome to Becullen',
            target: '.profile-title',
            content: `
            Your new way to share and acquire culture
        `,
            placement: 'center',
            showProgress: true,
            locale: {
                next: `Let's start`,
            }
        },
        {
            target: '.profile-title',
            content: `
                Let's start by filling in your personal data like name, language and other basic information.
            `,
            placement: 'center',
            showProgress: true,
        },
        {
            title: 'Botão salvar',
            target: '#save-button',
            content: `
                After filling in your details just click here to save your information.
            `,
            disableBeacon: true,
            disableOverlayClose: true,
            placement: 'left',
            spotlightClicks: false,
            showProgress: true,
            styles: {
                options: {
                    zIndex: 10000,
                },
            },
            locale: {
                last: 'Ok, I got it'
            }
        }
    ]
}

const getAfterSave = {
    id: 2,
    steps: [
        {
            title: 'Very good',
            continuos: false,
            target: '#side-bar-preferences-item',
            content: `
                Now that you have entered your personal information, let's go to your preferences.
                Just click here to go to the Preferences page.
            `,
            disableBeacon: true,
            disableOverlayClose: true,
            placement: 'right',
            spotlightClicks: false,
            styles: {
                options: {
                    zIndex: 10000,
                },
            },
            locale: {
                last: `Ok, I got it`
            }
        }
    ]
} 

export { getInitialOnboardingSteps, getAfterSave }