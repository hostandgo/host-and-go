using WebApi.Domain.Repositories;
using WebApi.Infrastructure.Repositories;
using WebApi.Shared.Email;
using Microsoft.Extensions.DependencyInjection;
using WebApi.Domain.Services;
using WebApi.Application.Services;

namespace HostAndGo.Api
{
    public static class DependencyInjection
    {
        public static IServiceCollection ConfigureDependencies(this IServiceCollection services)
        {
            
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IProfileRepository, ProfileRepository>();
            services.AddScoped<IPreferencesRepository, PreferencesRepository>();
            services.AddScoped<IHostRepository, HostRepository>();
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<IFileService, S3FileService>();
            return services;
        }
    }
}