using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using WebApi.Infrastructure;
using WebApi.Onboarding.Domain.Models;
using WebApi.Web.Utils;

namespace WebApi.Onboarding.Api
{
    [Route("api/onboarding/usage")]
    [ApiController]
    public class UsageController: ControllerBase
    {
        private readonly MongoContext _context;
        private readonly RequestContext _requestContext;
        public UsageController(MongoContext context, RequestContext requestContext)
        {
            _context = context;
            _requestContext = requestContext;
        }

        [HttpGet("passed-flow/{flowId}")]
        public async Task<ActionResult<bool>> PassedThroughTheFlow(int flowId)
        {
            var flowResult = await _context.UsageFlows.FindAsync(uf 
                => uf.UserId == _requestContext.UserId
                && uf.FlowId == flowId
            );
            var flow = await flowResult.FirstOrDefaultAsync();
            return Ok(flow != null);
        }

        [HttpPost("use-flow")]
        public async Task<ActionResult> UseFlow([FromBody] UseFlowPayload payload)
        {
            var usageFlow = new UsageFlow(_requestContext.UserId, payload.FlowId);
            await _context.UsageFlows.InsertOneAsync(usageFlow);
            return Ok();
        }
    }

    public class UseFlowPayload 
    {
        public int FlowId { get; set; }
    }
}