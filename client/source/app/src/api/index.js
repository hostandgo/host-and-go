import axios from 'axios';
import { apiBaseUrl }  from '../config/url'
import { getToken } from '../services/auth';
import handleRequest from './handleRequest';

const PROFILE = `${apiBaseUrl}/profile`;
const PREFERENCES = `${apiBaseUrl}/preference`;
const AUTHENTICATION = `${apiBaseUrl}/authentication`;
const ONBOARDING_USAGE = `${apiBaseUrl}/onboarding/usage`
const HOST = `${apiBaseUrl}/host`;
const USER = `${apiBaseUrl}/user`;
const DATA = `${apiBaseUrl}/data`;

export default {
    authentication: {
        register: async (payload, setLoading) => await handleRequest(
            () => axios.post(`${AUTHENTICATION}/register`, payload), 
            undefined,
            'An error occured try again',
            setLoading
        ),

        login: async (payload, setLoading) => await handleRequest(
            () => axios.post(`${AUTHENTICATION}/login`, payload),
            'Welcome back',
            'Incorrect login or password',
            setLoading
        ),
        
        requestNewPassword: async (payload, setLoading) => await handleRequest(
            () => axios.post(`${AUTHENTICATION}/request-new-password`, payload),
            'You should receive your new password soon.',
            'An error occured try again',
            setLoading
        ),

        updatePassword: async (payload, setLoading) => await handleRequest(
            () => axios.post(`${AUTHENTICATION}/update-password`, payload),
            'Password updated successfully',
            'An error occured try again',
            setLoading
        )
    },
    profile : {
        update: async (payload, setLoading) => await handleRequest(
            () => axios.put(`${PROFILE}/update`, payload, {
                headers: {'Authorization': "Bearer " + getToken() }
            }),
            undefined,
            undefined,
            setLoading
        ),
        get: async (setLoading) => await handleRequest(
            () => axios.get(`${PROFILE}`, {
                headers: {'Authorization': "Bearer " + getToken() }
            }),
            undefined,
            undefined,
            setLoading
        ),
        updateFamilyPhotos: async (payload, setLoading) => await handleRequest(
            () => axios.post(`${PROFILE}/update-family-photos`, payload, {
                headers: {'Authorization': "Bearer " + getToken() }
            }),
            undefined,
            undefined,
            setLoading
        ),
        updateDocument: async (payload, setLoading) => await handleRequest(
            () => axios.post(`${PROFILE}/update-document`, payload, {
                headers: {'Authorization': "Bearer " + getToken() }
            }),
            undefined,
            undefined,
            setLoading
        ),
        updatePicture: async (payload, setLoading) => await handleRequest(
            () => axios.post(`${PROFILE}/update-picture`, payload, {
                headers: {'Authorization': "Bearer " + getToken() }
            }),
            'Information updated successfully',
            undefined,
            setLoading
        )
    },
    preferences : {
        update: async (payload, setLoading) => await handleRequest(
            () => axios.put(`${PREFERENCES}/update`, payload, {
                headers: {'Authorization': "Bearer " + getToken() }
            }),
            'Information updated successfully',
            'An error occured, please try again later.',
            setLoading
        ),
        get: async (setLoading) => await handleRequest(
            () => axios.get(`${PREFERENCES}`, {
                headers: {'Authorization': "Bearer " + getToken() }
            }),
            undefined,
            undefined,
            setLoading
        )
    },
    host : {
        update: async (payload, setLoading) => await handleRequest(
            () => axios.put(`${HOST}/update`, payload, {
                headers: {'Authorization': "Bearer " + getToken() }
            }),
            'Information updated successfully',
            'An error occured, please try again later.',
            setLoading
        ),
        updateImages: async (payload, setLoading) => await handleRequest(
            () => axios.post(`${HOST}/update-images`, payload, {
                headers: {'Authorization': "Bearer " + getToken() }
            }),
            undefined,
            undefined,
            setLoading
        ),
        get: async (setLoading) => await handleRequest(
            () => axios.get(`${HOST}`, {
                headers: {'Authorization': "Bearer " + getToken() }
            }),
            undefined,
            undefined,
            setLoading
        ),
        getPhotos: async (setLoading) => await handleRequest(
            () => axios.get(`${HOST}/get-photos`, {
                headers: {'Authorization': "Bearer " + getToken() }
            }),
            undefined,
            undefined,
            setLoading
        )
    },
    user : {
        getWeeks: async () => await handleRequest(
            () => axios.get(`${USER}/get-weeks`, {
                headers: {'Authorization': "Bearer " + getToken() }
            })
        )
    },
    data : {
        getInterests: async () => await handleRequest(
            () => axios.get(`${DATA}/get-interests`, {
                headers: {'Authorization': "Bearer " + getToken() }
            }),
        )
    },
    onboarding: {
        usage: {
            passedThroughTheFlow: async ({flowId}) => await handleRequest(
                () => axios.get(`${ONBOARDING_USAGE}/passed-flow/${flowId}`, {
                    headers: {'Authorization': "Bearer " + getToken() }
                })
            ),
            useFlow: async ({flowId}) => await handleRequest(
                () => axios.post(`${ONBOARDING_USAGE}/use-flow`, { flowId }, {
                    headers: {'Authorization': "Bearer " + getToken() }
                })
            ),
        }
    }
}