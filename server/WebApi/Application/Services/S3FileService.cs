using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.AspNetCore.Http;
using WebApi.Domain.Models;
using WebApi.Domain.Services;

namespace WebApi.Application.Services
{
    public class S3FileService : IFileService
    {
        private static String accessKey = "AKIAJ6Z5OXZA54NA22VQ";
        private static String accessSecret = "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";
        private static String bucket = "becullen-webapi";

        private readonly IAmazonS3 _amazonClient;

        public S3FileService()
        {
            _amazonClient = new AmazonS3Client(accessKey, accessSecret, Amazon.RegionEndpoint.USEast2);
        }

        public async Task<List<string>> UploadRangeFileAsync(IEnumerable<IFormFile> formFiles)
        {
            var photos = new List<string>(10);
            foreach (var file in formFiles)
            {
                var fileName = await UploadFileAsync(file);
                if(fileName != null)
                    photos.Add(fileName);
            }
            return photos;
        }

        public async Task<string> UploadFileAsync(IFormFile file)
        {
            if (file != null && file.Length > 0)
            {
                byte[] fileBytes = new Byte[file.Length];
                file.OpenReadStream().Read(fileBytes, 0, Int32.Parse(file.Length.ToString()));
                
                // create unique file name for prevent the mess
                var fileName = Guid.NewGuid() + file.FileName;
                using (var stream = new MemoryStream(fileBytes))
                {
                    var request = new PutObjectRequest
                    {
                        BucketName = bucket,
                        Key = fileName,
                        InputStream = stream,
                        ContentType = file.ContentType,
                    };

                    var response = await _amazonClient.PutObjectAsync(request);
                    if(response.HttpStatusCode == HttpStatusCode.OK)
                        return fileName;
                    else 
                        return null;
                };
            }
            return null;
        }

        public async Task RemoveRangeFilesAsync(IEnumerable<string> keys)
        {
            var objectsToDelete = keys.Select(k => new KeyVersion() {Key = k}).ToList();
            if(objectsToDelete.Any())
            {
                var request = new DeleteObjectsRequest()
                {
                    BucketName = bucket,
                    Objects = objectsToDelete
                };
                await _amazonClient.DeleteObjectsAsync(request);
            }
        }
    }
}