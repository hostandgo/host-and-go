using System.Collections.Generic;
using WebApi.Domain.Enums;

namespace WebApi.Application.Payloads
{
    public class InterestPayload
    {
        public InterestType Interest { get; set; }
        public IEnumerable<ComplementInterestType> ComplementInterest { get; set; }
    }
}