namespace WebApi.Domain.ValueObjects
{
    public class Language
    {
        public Language(string name, string level)
        {
            this.Name = name;
            this.Level = level;

        }
        public string Name { get; set; }
        public string Level { get; set; }
    }
}