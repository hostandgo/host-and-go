using System;
using System.Threading.Tasks;
using WebApi.Application.CommandSide.Commands;
using WebApi.Application.QuerySide.Queries;
using WebApi.Application.ViewModels;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using WebApi.Web.Utils;

namespace HostAndGo.Web.Controllers
{
    [Route("api/host")]
    [ApiController]
    public class HostController: ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly RequestContext _requestContext;

        public HostController(IMediator mediator, RequestContext requestContext)
        {
            _mediator = mediator;
            _requestContext = requestContext;
        }

        [HttpPut("update")]
        public async Task<ActionResult> UpdateDetails([FromBody] UpdateHostDetailsCommand command)
        {
            command.UserId = _requestContext.UserId;
            await _mediator.Send(command);
            return Ok();
        }

        [HttpPost("update-images")]
        public async Task<ActionResult> UpdateImages(List<IFormFile> files)
        {
            var command = new UpdateHostImagesCommand(_requestContext.UserId, files);
            await _mediator.Send(command);
            return Ok();
        }

        [HttpGet]
        public async Task<ActionResult<GetHostDetailsViewModel>> Get()
        {
            var result = await _mediator.Send(new GetHostDetailsQuery(_requestContext.UserId));
            return Ok(result);
        }

        [HttpGet("find-hosts/country/{countryName}/startDate/{startDate}/endDate/{endDate}")]
        public async Task<ActionResult<List<FindHostViewModel>>> FindHosts()
        {
            var result = await _mediator.Send(new FindHostsQuery());
            return Ok(result);  
        }

        [HttpGet("get-photos")]
        public async Task<ActionResult<string[]>> GetPhotos()
        {
            var result = await _mediator.Send(new GetPhotosQuery(_requestContext.UserId));
            return Ok(result);
        }
    }
}