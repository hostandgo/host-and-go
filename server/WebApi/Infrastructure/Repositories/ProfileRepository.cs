using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WebApi.Domain.Models;
using WebApi.Domain.Repositories;
using MongoDB.Driver;

namespace WebApi.Infrastructure.Repositories
{
    public class ProfileRepository : IProfileRepository
    {
        private readonly MongoContext _context;

        public ProfileRepository(MongoContext context)
        {
            _context = context;
        }

        public async Task<Profile> FindByFilter(Expression<Func<Profile, bool>> filter)
        {
            var result = await _context.Profiles.FindAsync(filter);
            return await result.FirstOrDefaultAsync();
        }

        public async Task<Profile> FindByUserId(Guid userId)
        {
            var result = await _context.Profiles.FindAsync(p => p.UserId == userId);
            return await result.FirstOrDefaultAsync();
        }

        public async Task Insert(Profile profile)
        => await _context.Profiles.InsertOneAsync(profile);

        public async Task<IEnumerable<Profile>> ListByFilter(Expression<Func<Profile, bool>> filter)
        {
            var result = await _context.Profiles.FindAsync(filter);
            return result.Current;
        }

        public async Task Remove(Profile profile)
        => await _context
            .Profiles
            .DeleteOneAsync(i => i.Id == profile.Id);

        public async Task<Profile> Update(Profile profile)
        {
            await _context.Profiles.ReplaceOneAsync(i => i.Id == profile.Id, profile);
            return profile;
        }
    }
}