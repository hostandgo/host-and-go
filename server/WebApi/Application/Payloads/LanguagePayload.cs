namespace WebApi.Application.Payloads
{
    public class LanguagePayload
    {
        public string Name { get; set; }
        public string Level { get; set; }
    }
}