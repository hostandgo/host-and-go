import { makeStyles } from "@material-ui/core/styles";

export default makeStyles(theme => ({
  containerMobile: {
    marginTop: 40,
    padding: 10
  },
  containerDesktop: {
    margin: 35,
    display: "flex",
    flexDirection: "column",
    width: "100%"
  },
  textFieldDescribe: {
    margin: theme.spacing(1),
    width: "95%"
  },
  textFieldMobile: {
    margin: theme.spacing(1),
    width: "90%"
  },
  paperMobile: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    paddingBottom: 20,
    marginTop: 20
  },
  paperDesktop: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    paddingBottom: 20
  },
  paperDesktopCustom: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    paddingBottom: 20,
    paddingTop: 40,
    marginTop: 20,
    justifyContent: "space-around"
  },
  languageWrapperDesktop: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    width: "50%"
  },
  textFieldInterest: {
    width: "40%",
    margin: theme.spacing(1)
  },
  chip: {
    margin: theme.spacing(1)
  },
  formContainerDesktop: {
    display: "flex",
    flexDirection: "column",
    width: "80%"
  },
  formContainerMobile: {
    flexDirection: "column",
    display: "flex",
    width: "100%",
    alignItems: "center"
  },
  title: {
    margin: theme.spacing(1)
  }
}));
