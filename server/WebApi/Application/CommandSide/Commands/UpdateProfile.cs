using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebApi.Application.Payloads;
using WebApi.Domain.Models;
using WebApi.Domain.Repositories;
using WebApi.Domain.ValueObjects;
using MediatR;

namespace WebApi.Application.CommandSide.Commands
{
    public class UpdateProfileCommand : IRequest
    {
        public string Deficiency { get; set; }
        public Guid UserId { get; set; }
        public int Stars { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Email { get; set; }
        public string Status { get; set; }
        public int? Genre { get; set; }
        public string NativeLanguage { get; set; }
        public LanguagePayload[] Languages { get; set; }
        public string Nationality { get; set; }
        public LocationItem Continent { get; set; }
        public LocationItem Country { get; set; }
        public LocationItem State { get; set; }
        public LocationItem Region { get; set; }
        public string Instagram { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Linkedin { get; set; }
        public string Others { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Religion { get; set; }
    }

    public class UpdateProfileCommandHandler : IRequestHandler<UpdateProfileCommand>
    {
        private readonly IProfileRepository _profileRepository;

        public UpdateProfileCommandHandler(IProfileRepository profileRepository)
        {
            _profileRepository = profileRepository;
        }

        public async Task<Unit> Handle(UpdateProfileCommand request, CancellationToken cancellationToken)
        {
            var profile = await _profileRepository.FindByUserId(request.UserId);
            var name = new Name(request.FirstName, request.SecondName);
            var socialMedia = new SocialMedia()
            {
                Facebook = request.Facebook,
                Instagram = request.Instagram,
                Linkedin = request.Linkedin,
                Twitter = request.Twitter
            };

            var location = new Location(request.Continent, request.Country, request.State, request.Region);
            if (profile == null)
            {
                var newProfile = new Profile(
                    request.UserId,
                    name,
                    request.Genre,
                    request.NativeLanguage,
                    request.Nationality,
                    location,
                    socialMedia,
                    request.Deficiency,
                    request.Religion,
                    request.DateOfBirth,
                    request.Languages.Select(l => new Language(l.Name, l.Level))
                );
                await _profileRepository.Insert(newProfile);
            }
            else
            {
                profile.Update(
                    name,
                    request.Genre,
                    request.NativeLanguage,
                    request.Nationality,
                    location,
                    socialMedia,
                    request.Deficiency,
                    request.Religion,
                    request.DateOfBirth,
                    request.Languages.Select(l => new Language(l.Name, l.Level))
                );
                await _profileRepository.Update(profile);
            }

            return Unit.Value;
        }
    }
}