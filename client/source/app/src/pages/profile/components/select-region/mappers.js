export const resultToViewModel = result => result.geonames
    .map(geoName => ({ id: geoName.geonameId, name: geoName.name }))