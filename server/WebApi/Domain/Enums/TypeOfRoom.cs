namespace WebApi.Domain.Enums
{
    public enum TypeOfRoom
    {
        Private = 1,
        Shared = 2
    }
}