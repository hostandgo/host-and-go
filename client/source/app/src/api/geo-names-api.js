import axios from "axios";
import { geoNamesBaseUrlWithParam } from "../config/url";

const continentGeoNameId = "6295630";

export default {
    getContinents: async () =>
        (await axios.get(geoNamesBaseUrlWithParam(continentGeoNameId))).data,
    getCountries: async countryGeoNameId =>
        (await axios.get(geoNamesBaseUrlWithParam(countryGeoNameId))).data,
    getStates: async stateGeoNameId =>
        (await axios.get(geoNamesBaseUrlWithParam(stateGeoNameId))).data,
    getRegions: async regionGeoNameId =>
        (await axios.get(geoNamesBaseUrlWithParam(regionGeoNameId))).data
};
