using System;
using System.Threading;
using System.Threading.Tasks;
using WebApi.Application.ViewModels;
using WebApi.Domain.Repositories;
using MediatR;

namespace WebApi.Application.CommandSide.Commands
{
    public class UpdatePasswordCommand: IRequest<UserAuthenticateViewModel>
    {
        public string Email { get; set; }
        public string NewPassword { get; set; }
    }

    public class UpdatePasswordCommandHandler : IRequestHandler<UpdatePasswordCommand, UserAuthenticateViewModel>
    {
        private readonly IUserRepository _userRepository;

        public UpdatePasswordCommandHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<UserAuthenticateViewModel> Handle(UpdatePasswordCommand request, CancellationToken cancellationToken)
        {
            var user = await _userRepository.FindByFilter(u => u.Authentication.Email == request.Email);
            if(user == null)
                throw new Exception();
            
            user.Authentication.UpdatePassword(request.NewPassword);
            await _userRepository.Update(user);
            return new UserAuthenticateViewModel(user.Id, user.Authentication.Token, user.Authentication.ForgotPassword);
        }
    }
}