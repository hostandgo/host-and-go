using WebApi.Domain.Models;
using MongoDB.Bson.Serialization;

namespace WebApi.Infrastructure.Mappers
{
    public class HostMapper
    {
        public static void Configure()
        {
            BsonClassMap.RegisterClassMap<Host>(map => 
            {
                map.AutoMap();
                map.MapField(p => p.UserId);
                map.MapCreator(h => new Host(
                    h.UserId,
                    h.Addres,
                    h.Description,
                    h.TypeOfRoom,
                    h.BathroomAccess,
                    h.HoursInteractTravel,
                    h.DescribeWhatCanDoTogether,
                    h.DescribeWhatYourCity,
                    h.PreferredAgeRange,
                    h.City,
                    h.Attractions,
                    h.Interests,
                    h.TimesAvailableToReceive,
                    h.Restrictions
                ));
                map.MapField(h => h.City);
                map.MapField(h => h.PreferredAgeRange);
                map.MapField(h => h.Interests);
                map.MapField(h => h.Attractions);
                map.MapField(h => h.Restrictions);
                map.MapField(h => h.TimesAvailableToReceive);
                map.MapField("_photos").SetElementName("Photos");
            });
        }
    }
}