using System.ComponentModel;

namespace WebApi.Domain.Enums
{
    public enum AttractionType
    {
        [Description("Beaches")]
        Beaches = 1,
        [Description("Parks")]
        Parks = 2,
        [Description("Theme parks")]
        ThemeParks = 3,
        [Description("Night clubs")]
        NightClubs = 4,
        [Description("Bars and Restaurants")]
        BarsAndRestaurants = 5,
        [Description("Theatre")]
        Theatre = 6,
        [Description("Malls")]
        Malls = 7,
        [Description("Concerts")]
        Concerts = 8,
        [Description("Great for outdoor activities")]
        GreatForOutdoorActivities = 9,
        [Description("Arenas and stadiums")]
        ArenasAndStadiums = 10,
        [Description("Sport Venues")]
        SportVenues = 11,
        [Description("Mountains")]
        Mountains = 12,
        [Description("Museums")]
        Museums = 13,
        [Description("Universities")]
        Universities = 14,
        [Description("Colleges and Language Schools")]
        CollegesAndLanguageSchools = 15,
        [Description("Wildlife and forest")]
        WildlifeAndForest = 16,
        [Description("Hiking")]
        Hiking = 17,
        [Description("Ice sports")]
        IceSports = 18,
        [Description("Waterfalls")]
        Waterfalls = 19,
        [Description("Public Pools")]
        PublicPools = 20,
        [Description("Recreational areas")]
        RecreationalAreas = 21,
        [Description("River")]
        River = 22,
        [Description("Zoo")]
        Zoo = 23
    }
}