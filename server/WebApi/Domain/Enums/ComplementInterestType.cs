using System.ComponentModel;

namespace WebApi.Domain.Enums
{
    public enum ComplementInterestType
    {
        [Description("Boxe")]
        Boxe,

        [Description("Jiu Jitsu")]
        JiuJitsu,

        [Description("Taekwondo")]
        Taekwondo,

        [Description("Judo")]
        Judo,

        [Description("Karate")]
        Karate,

        [Description("Muay Thai")]
        MuayThai,

        [Description("Krav Maga")]
        KravMaga,

        [Description("MMA")]
        MMA,

        [Description("Kickboxing")]
        Kickboxing,

        [Description("Soccer")]
        Soccer,

        [Description("AmericanFootball")]
        AmericanFootball,

        [Description("Volleyball")]
        Volleyball,

        [Description("Basketball")]
        Basketball,

        [Description("Handball")]
        Handball,


        [Description("Swimming")]
        Swimming,

        [Description("Synchronized Swimming")]
        SynchronizedSwimming,

        [Description("Water Polo")]
        WaterPolo,

        [Description("Ornamental Jump")]
        OrnamentalJump,

        [Description("Canoeing /Rowing")]
        Canoeing_Rowing,

        [Description("Surf")]
        Surf,

        [Description("Bodyboarding")]
        Bodyboarding,

        [Description("Candle")]
        Candle,

        [Description("Stand up paddle")]
        Stand_Up_Paddle,

        [Description("Water_Skiing")]
        Water_Skiing,

        [Description("Rafting")]
        Rafting,

        [Description("Diving")]
        Diving,

        [Description("Olympic Gymnastics")]
        Olympic_Gymnastics,

        [Description("Workout")]
        Workout,

        [Description("Cross fit")]
        Cross_fit,

        [Description("Bodybuilding")]
        Bodybuilding,

        [Description("Yoga")]
        Yoga,

        [Description("Tenis")]
        Tenis,

        [Description("Badminton")]
        Badminton,

        [Description("Squash")]
        Squash,

        [Description("walking")]
        Walking,

        [Description("Running")]
        Running,

        [Description("Skateboard")]
        Skateboard,

        [Description("Hiking and trails")]
        Hiking_And_Trails,

        [Description("Cycling and mountain_biking")]
        Cycling_And_Mountain_biking,

        [Description("Table tennis")]
        Table_Tennis,

        [Description("Chess and board games")]
        Chess_And_Board_Games,

        [Description("Snooker")]
        Snooker,

        [Description("Bowling")]
        Bowling,

        [Description("Hockey")]
        Hockey,

        [Description("Tchoukball")]
        Tchoukball,

        [Description("Fencing")]
        Fencing,

        [Description("Ski")]
        Ski,

        [Description("Snowboard")]
        Snowboard,

        [Description("Skating")]
        Skating,

        [Description("Ice Hockey")]
        Ice_Hockey,

        [Description("Sledding")]
        Sledding,

        [Description("Skydiving hang gliding")]
        Skydiving_Hang_Gliding,

        [Description("Rappelling and mountaineering")]
        Rappelling_And_Mountaineering,

        [Description("Football soccer")]
        Football_Soccer,

        [Description("Cricket")]
        Cricket,

        [Description("Rugby")]
        Rugby,

        [Description("Golf")]
        Golf,

        [Description("Baseball")]
        Baseball,

        [Description("Lacrosse")]
        Lacrosse,

        [Description("Equestrianism")]
        Equestrianism,

        [Description("Athletics")]
        Athletics,

        [Description("Electronics")]
        Electronics,

        [Description("Country")]
        Country,

        [Description("POP")]
        POP,

        [Description("Reggae")]
        Reggae,

        [Description("HIPHOP")]
        HIPHOP,

        [Description("Classical")]
        Classical,

        [Description("Rap / Funk / Soul")]
        RapFunkSoul,

        [Description("Jazz")]
        Jazz,

        [Description("Folk")]
        Folk,

        [Description("Rock")]
        Rock,

        [Description("Blues")]
        Blues,

        [Description("Religious")]
        Religious,

        [Description("Classica")]
        Classica,

        [Description("Street")]
        Street,

        [Description("Ballroom")]
        Ballroom,

        [Description("Volunteering")]
        Volunteering,

        [Description("Internship")]
        Internship,

        [Description("Cooking")]
        Cooking,

        [Description("Patisserie")]
        Patisserie,

        [Description("Wine")]
        Wine,

        [Description("Beer")]
        Beer,

        [Description("Coffee")]
        Coffee,

        [Description("Literature")]
        Literature,

        [Description("Painting")]
        Painting,

        [Description("Plastic arts / Sculptures")]
        PlasticArts_Sculptures,

        [Description("MovieTheater")]
        MovieTheater,

        [Description("Theater")]
        Theater,

        [Description("Television")]
        Television,

        [Description("Counter Striker")]
        CounterStriker,

        [Description("League Of Legends")]
        LeagueOfLegends,

        [Description("DOTA")]
        DOTA,

        [Description("Fortnite")]
        Fortnite,

        [Description("Abkhaz")]
        Abkhaz,
        [Description("Afar")]
        Afar,
        [Description("Afrikaans")]
        Afrikaans,
        [Description("Akan")]
        Akan,
        [Description("Albanian")]
        Albanian,
        [Description("Amharic")]
        Amharic,
        [Description("Arabic")]
        Arabic,
        [Description("Aragonese")]
        Aragonese,
        [Description("Armenian")]
        Armenian,
        [Description("Assamese")]
        Assamese,
        [Description("Avaric")]
        Avaric,
        [Description("Avestan")]
        Avestan,
        [Description("Aymara")]
        Aymara,
        [Description("Azerbaijani")]
        Azerbaijani,
        [Description("Bambara")]
        Bambara,
        [Description("Bashkir")]
        Bashkir,
        [Description("Basque")]
        Basque,
        [Description("Belarusian")]
        Belarusian,
        [Description("Bengali")]
        Bengali,
        [Description("Bihari")]
        Bihari,
        [Description("Bislama")]
        Bislama,
        [Description("Bosnian")]
        Bosnian,
        [Description("Breton")]
        Breton,
        [Description("Bulgarian")]
        Bulgarian,
        [Description("Burmese")]
        Burmese,
        [Description("Catalan")]
        Catalan,
        [Description("Chamorro")]
        Chamorro,
        [Description("Chechen")]
        Chechen,
        [Description("Chichewa")]
        Chichewa,
        [Description("Chinese")]
        Chinese,
        [Description("Chuvash")]
        Chuvash,
        [Description("Cornish")]
        Cornish,
        [Description("Corsican")]
        Corsican,
        [Description("Cree")]
        Cree,
        [Description("Croatian")]
        Croatian,
        [Description("Czech")]
        Czech,
        [Description("Danish")]
        Danish,
        [Description("Divehi")]
        Divehi,
        [Description("Dutch")]
        Dutch,
        [Description("English")]
        English,
        [Description("Esperanto")]
        Esperanto,
        [Description("Estonian")]
        Estonian,
        [Description("Ewe")]
        Ewe,
        [Description("Faroese")]
        Faroese,
        [Description("Fijian")]
        Fijian,
        [Description("Finnish")]
        Finnish,
        [Description("French")]
        French,
        [Description("Fula")]
        Fula,
        [Description("Galician")]
        Galician,
        [Description("Georgian")]
        Georgian,
        [Description("German")]
        German,
        [Description("Greek")]
        Greek,
        [Description("Guaraní")]
        Guaraní,
        [Description("Gujarati")]
        Gujarati,
        [Description("Haitian")]
        Haitian,
        [Description("Hausa")]
        Hausa,
        [Description("Hebrew")]
        Hebrew,
        [Description("Herero")]
        Herero,
        [Description("Hindi")]
        Hindi,
        [Description("Hiri")]
        Hiri,
        [Description("Hungarian")]
        Hungarian,
        [Description("Interlingua")]
        Interlingua,
        [Description("Indonesian")]
        Indonesian,
        [Description("Interlingue")]
        Interlingue,
        [Description("Irish")]
        Irish,
        [Description("Igbo")]
        Igbo,
        [Description("Inupiaq")]
        Inupiaq,
        [Description("Ido")]
        Ido,
        [Description("Icelandic")]
        Icelandic,
        [Description("Italian")]
        Italian,
        [Description("Inuktitut")]
        Inuktitut,
        [Description("Japanese")]
        Japanese,
        [Description("Javanese")]
        Javanese,
        [Description("Kalaallisut")]
        Kalaallisut,
        [Description("Kannada")]
        Kannada,
        [Description("Kanuri")]
        Kanuri,
        [Description("Kashmiri")]
        Kashmiri,
        [Description("Kazakh")]
        Kazakh,
        [Description("Khmer")]
        Khmer,
        [Description("Kikuyu")]
        Kikuyu,
        [Description("Kinyarwanda")]
        Kinyarwanda,
        [Description("Kirghiz")]
        Kirghiz,
        [Description("Komi")]
        Komi,
        [Description("Kongo")]
        Kongo,
        [Description("Korean")]
        Korean,
        [Description("Kurdish")]
        Kurdish,
        [Description("Kwanyama")]
        Kwanyama,
        [Description("Latin")]
        Latin,
        [Description("Luxembourgish")]
        Luxembourgish,
        [Description("Luganda")]
        Luganda,
        [Description("Limburgish")]
        Limburgish,
        [Description("Lingala")]
        Lingala,
        [Description("Lao")]
        Lao,
        [Description("Lithuanian")]
        Lithuanian,
        [Description("Luba")]
        Luba,
        [Description("Latvian")]
        Latvian,
        [Description("Manx")]
        Manx,
        [Description("Macedonian")]
        Macedonian,
        [Description("Malagasy")]
        Malagasy,
        [Description("Malay")]
        Malay,
        [Description("Malayalam")]
        Malayalam,
        [Description("Maltese")]
        Maltese,
        [Description("Māori")]
        Māori,
        [Description("Marathi")]
        Marathi,
        [Description("Marshallese")]
        Marshallese,
        [Description("Mongolian")]
        Mongolian,
        [Description("Nauru")]
        Nauru,
        [Description("Navajo")]
        Navajo,
        [Description("Norwegian")]
        Norwegian,
        [Description("North")]
        North,
        [Description("Nepali")]
        Nepali,
        [Description("Ndonga")]
        Ndonga,
        [Description("Nuosu")]
        Nuosu,
        [Description("South")]
        South,
        [Description("Occitan")]
        Occitan,
        [Description("Ojibwe")]
        Ojibwe,
        [Description("Old")]
        Old,
        [Description("Oromo")]
        Oromo,
        [Description("Oriya")]
        Oriya,
        [Description("Ossetian")]
        Ossetian,
        [Description("Panjabi")]
        Panjabi,
        [Description("Pāli")]
        Pāli,
        [Description("Persian")]
        Persian,
        [Description("Polish")]
        Polish,
        [Description("Pashto")]
        Pashto,
        [Description("Portuguese")]
        Portuguese,
        [Description("Quechua")]
        Quechua,
        [Description("Romansh")]
        Romansh,
        [Description("Kirundi")]
        Kirundi,
        [Description("Romanian")]
        Romanian,
        [Description("Russian")]
        Russian,
        [Description("Sanskrit")]
        Sanskrit,
        [Description("Sardinian")]
        Sardinian,
        [Description("Sindhi")]
        Sindhi,
        [Description("Northern")]
        Northern,
        [Description("Samoan")]
        Samoan,
        [Description("Sango")]
        Sango,
        [Description("Serbian")]
        Serbian,
        [Description("Scottish")]
        Scottish,
        [Description("Shona")]
        Shona,
        [Description("Sinhala")]
        Sinhala,
        [Description("Slovak")]
        Slovak,
        [Description("Slovene")]
        Slovene,
        [Description("Somali")]
        Somali,
        [Description("Southern")]
        Southern,
        [Description("Spanish")]
        Spanish,
        [Description("Sundanese")]
        Sundanese,
        [Description("Swahili")]
        Swahili,
        [Description("Swati")]
        Swati,
        [Description("Swedish")]
        Swedish,
        [Description("Tamil")]
        Tamil,
        [Description("Telugu")]
        Telugu,
        [Description("Tajik")]
        Tajik,
        [Description("Thai")]
        Thai,
        [Description("Tigrinya")]
        Tigrinya,
        [Description("Tibetan")]
        Tibetan,
        [Description("Turkmen")]
        Turkmen,
        [Description("Tagalog")]
        Tagalog,
        [Description("Tswana")]
        Tswana,
        [Description("Tonga")]
        Tonga,
        [Description("Turkish")]
        Turkish,
        [Description("Tsonga")]
        Tsonga,
        [Description("Tatar")]
        Tatar,
        [Description("Twi")]
        Twi,
        [Description("Tahitian")]
        Tahitian,
        [Description("Uighur")]
        Uighur,
        [Description("Ukrainian")]
        Ukrainian,
        [Description("Urdu")]
        Urdu,
        [Description("Uzbek")]
        Uzbek,
        [Description("Venda")]
        Venda,
        [Description("Vietnamese")]
        Vietnamese,
        [Description("Volapük")]
        Volapük,
        [Description("Walloon")]
        Walloon,
        [Description("Welsh")]
        Welsh,
        [Description("Wolof")]
        Wolof,
        [Description("Western")]
        Western,
        [Description("Xhosa")]
        Xhosa,
        [Description("Yiddish")]
        Yiddish,
        [Description("Yoruba")]
        Yoruba,
        [Description("Zhuang")]
        Zhuang,

    }
}