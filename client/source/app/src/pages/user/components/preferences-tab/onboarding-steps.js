export default [
    {
        title: 'Esta é sua página de preferencias',
        target: '.preferences-title',
        content: `
            Aqui vão ficar as informações que outros hosts 
        `,
        placement: 'center',
        locale: {
            close: 'Vamos começar'
        }
    },
    {
        target: '.profile-title',
        content: `
            Vamos iniciar preenchendo seus dados pessoas, como nome, linguagem e outras informações básicas
        `,
        placement: 'center',
        locale: {
            close: 'Ok'
        }
    },
    {
        title: 'Botão salvar',
        target: '.save-button',
        content: `
            Após preencher seus dados basta clicar aqui para salvar suas informações
        `,
          disableBeacon: true,
          disableOverlayClose: true,
          placement: 'left',
          spotlightClicks: false,
          styles: {
            options: {
              zIndex: 10000,
            },
          },
          locale: {
            close: 'Ok, Entendi'
        }
    }
]