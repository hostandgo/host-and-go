export const apiBaseUrl = process.env.REACT_APP_API_URL;
export const bucketBaseUrl =
  "https://becullen-webapi.s3.us-east-2.amazonaws.com";
export const geoNamesBaseUrl = "http://www.geonames.org";
export const getGeoNamesParam = geoNameId =>
  `childrenJSON?geonameId=${geoNameId}&style=long&noCacheIE=1582593395037`;
export const geoNamesBaseUrlWithParam = geoNameId =>
  `${geoNamesBaseUrl}/${getGeoNamesParam(geoNameId)}`;
// export const apiBaseUrl = 'https://becullen-api.herokuapp.com/api';
