using System;

namespace WebApi.Application.ViewModels
{
    public class FindHostViewModel
    {
        public FindHostViewModel(Guid userId, string profileFirstName, string profileSecondName, string profileDescription)
        {
            this.UserId = userId;
            this.ProfileName = profileFirstName;
            this.ProfileDescription = profileDescription;
            this.ProfileSecondName = profileSecondName;
        }
        public Guid UserId { get; set; }
        public string ProfileName { get; set; }
        public string ProfileSecondName { get; set; }
        public string ProfileDescription { get; set; }

    }
}