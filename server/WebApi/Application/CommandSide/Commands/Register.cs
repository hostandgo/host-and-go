using System.Threading;
using System.Threading.Tasks;
using WebApi.Application.ViewModels;
using WebApi.Domain.Models;
using WebApi.Domain.Repositories;
using MediatR;

namespace WebApi.Application.CommandSide.Commands
{
    public class RegisterCommand: IRequest<UserAuthenticateViewModel>
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }

    public class RegisterCommandHandler : IRequestHandler<RegisterCommand, UserAuthenticateViewModel>
    {
        private readonly IUserRepository _userRepository;
        private readonly IMediator _mediator;

        public RegisterCommandHandler(IUserRepository userRepository, IMediator mediator)
        {
            _userRepository = userRepository;
            _mediator =  mediator;
        }

        public async Task<UserAuthenticateViewModel> Handle(RegisterCommand request, CancellationToken cancellationToken)
        {
            var user = new User(request.Email, request.Password);
            await _userRepository.Insert(user);
            var autenticateCommand = new AuthenticateCommand() 
            {
                Email = request.Email,
                Password = request.Password
            };
            return await _mediator.Send(autenticateCommand);
        }
    }
}