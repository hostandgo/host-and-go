import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

function getModalStyle() {
    const top = 50;
    const left = 50;

    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`
    };
}

const useStyles = makeStyles(theme => ({
    paper: {
        position: "absolute",
        width: 400,
        backgroundColor: theme.palette.background.paper,
        border: "2px solid #000",
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3)
    },
    card: {
        position: "absolute",
        width: 345,
        maxWidth: 345,
    },
    media: {
        height: 140,
    },
}));

export default function SimpleModal({
    children,
    withMedia,
    image,
    imageTitle,
    open,
    handleClose,
    title,
    textSecondary,
    onConfirm,
    onCancel
}) {
    const classes = useStyles();
    // getModalStyle is not a pure function, we roll the style only on the first render
    const [modalStyle] = React.useState(getModalStyle);
    const hasButton = onConfirm !== undefined && onCancel !== undefined;
    return (
        <Modal
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            open={open}
            onClose={handleClose}
        >
            <Card style={modalStyle} className={classes.card}>
                {
                    withMedia &&
                    <CardMedia
                        className={classes.media}
                        image={image}
                        title={imageTitle}
                    />

                }
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                        {title}
                    </Typography>
                    <Typography variant="body2" color="textSecondary" component="p">
                        {textSecondary}
                    </Typography>
                    {children}
                </CardContent>
                {
                    hasButton &&
                    <CardActions>
                        {
                            onCancel &&
                            <Button
                                variant="contained"
                                onClick={onCancel}
                                size="small"
                            >
                                Cancel
                            </Button>
                        }
                        {
                            onConfirm &&
                            <Button
                                variant="contained"
                                onClick={onConfirm}
                                size="small"
                                color="primary"
                            >
                                Confirm
                            </Button>
                        }
                    </CardActions>
                }
            </Card>
        </Modal>
    );
}
