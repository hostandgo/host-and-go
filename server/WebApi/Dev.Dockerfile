FROM mcr.microsoft.com/dotnet/core/sdk:3.0 AS build-env
WORKDIR /app

COPY . ./

RUN dotnet restore "./WebApi.csproj"
RUN ASPNETCORE_ENVIRONMENT=Development dotnet publish "./WebApi.csproj" -c Release -o out


FROM mcr.microsoft.com/dotnet/core/aspnet:3.0 AS runtime
WORKDIR /app
COPY --from=build-env /app/out .
# CMD ASPNETCORE_URLS=http://*:$PORT dotnet WebApi.dll
CMD ["dotnet", "HostAndGo.Api.dll"]
