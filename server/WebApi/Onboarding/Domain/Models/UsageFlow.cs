using System;
using WebApi.Domain.Models;

namespace WebApi.Onboarding.Domain.Models
{
    public class UsageFlow: BaseEntity
    {
        public UsageFlow(Guid userId, int flowId)
        {
            UserId = userId;
            FlowId = flowId;
        }

        public Guid UserId { get; set; }
        public int FlowId { get; set; }
    }
}