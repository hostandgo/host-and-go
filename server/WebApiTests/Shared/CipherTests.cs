using FluentAssertions;
using WebApi.Shared.Utils;
using Xunit;

namespace WebApiTests.Shared
{
    public class CipherTests
    {
        [Fact]
        public void Decrypt_ShouldBeDecryptEncryptedString()
        {
            var password = "MyPassword";
            //Generate with crypto-js
            var passwordDecrypt = Cipher.OpenSSLDecrypt("U2FsdGVkX1+bva7j08kqT/HqZBXIxF0RBG32vzlAqGk=");
            password.Should().Be(passwordDecrypt);
        }
    }
}