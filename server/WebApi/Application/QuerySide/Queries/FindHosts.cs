using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebApi.Application.ViewModels;
using WebApi.Infrastructure;
using MediatR;
using MongoDB.Driver;
using System;
using System.Collections.Generic;

namespace WebApi.Application.QuerySide.Queries
{
    public class FindHostsQuery: IRequest<List<FindHostViewModel>>
    {
        public Guid UserId { get; set; }
    }

    public class FindHostsQueryHandler: IRequestHandler<FindHostsQuery, List<FindHostViewModel>>
    {
        private readonly MongoContext _context;

        public FindHostsQueryHandler(MongoContext context)
        {
            _context = context;
        }

        public async Task<List<FindHostViewModel>> Handle(FindHostsQuery request, CancellationToken token)
        {
            var userPreferencesResult = await _context.Preferences
                .Find(p => p.UserId == request.UserId)
                .FirstOrDefaultAsync();

            var hosts = await _context.Hosts
                .Find(
                    h => h.Interests.Any(i => userPreferencesResult.Interests.Contains(i))
                )
                .SortByDescending(
                    h => h.Interests.Count(i => userPreferencesResult.Interests.Contains(i))
                )
                .Limit(5)
                .ToListAsync();

            return await _context.Profiles
                .Find(
                    p => hosts.Select(h => h.UserId).Contains(p.Id)
                )
                .Project(p => new FindHostViewModel(p.UserId, p.Name.FirstName, p.Name.SecondName, hosts.FirstOrDefault(h => h.UserId == p.UserId).Description))
                .ToListAsync();
        }
    }
}