import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(theme => ({
    avatar: {
        margin: 10,
    },
    bigAvatar: {
        margin: 10,
        width: 150,
        height: 150,
    },
    textFieldMobile: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: '90%'
    },
    textFieldDesktop: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: '30%'
    },
    chip: {
        margin: theme.spacing(1),
    },
    fab: {
        margin: theme.spacing(1),
    },
    fabLanguage: {
        margin: theme.spacing(1),
    },
    wrapAvatar: {
        display: 'flex', 
        alignItems: 'center', 
        flexDirection: 'column'
    },
    containerMobile: {
        marginTop: 60,
        padding: 10
    },
    containerDesktop: {
        display: 'flex',
        margin: 35,
        width: '100%'
    },
    containerFormMobile: {
        display: 'flex',
        width: '100%', 
        alignItems: 'center', 
        flexDirection: 'column'
    },
    containerFormDesktop: {
        display: 'flex', 
        alignItems: 'center', 
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    formOneMobile: {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
        width: '100%'
    },
    formOneDesktop: {
        width: '85%',
        marginLeft: 18
    },
    formTwoMobile: {
        marginTop: theme.spacing(1),
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
        width: '100%'
    },  
    formTwoDesktop: {
        marginTop: theme.spacing(1),
        display: 'flex',
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
    },
    languageWrapperMobile : {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
        width: '100%'
    },
    languageWrapperDesktop: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        width: '100%'
    },
    formThreeMobile: {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
        width: '100%'
    },
    formThreeDesktop: {
        width: '66%',
        display: 'flex',
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    topWrapperMobile: {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
        width: '100%'
    },
    topWrapperDesktop: {
        display: 'flex',
        width: '100%',
        justifyContent: 'space-between'
    },
    wrapperSaveButton: {
        margin: theme.spacing(1),
        position: 'relative',
    },
    fabProgress: {
        color: theme.palette.primary,
        position: 'absolute',
        top: -6,
        left: -6,
        zIndex: 1,
        margin: 8
    },
    fabProgressCentralDesktop: {
        color: theme.palette.primary,
        position: 'fixed',
        top: '40%',
        left: '45%',
        zIndex: 1,
        margin: 8
    },
    fabProgressCentralMobile: {
        color: theme.palette.primary,
        position: 'fixed',
        top: '20%',
        left: '34%',
        zIndex: 1,
        margin: 8
    },
    cardTitle : {
        padding: theme.spacing(1)
    },
    editImageLink: {
        cursor: 'pointer'
    }
}));