using System.ComponentModel;

namespace WebApi.Domain.Enums
{
    public enum PreferredAgeRange
    {
        [Description("Don't have")]
        DontHave = 1,
        
        [Description("18 - 25")]
        _18_25 = 2,
        
        [Description("26 - 35")]
        _26_35 = 3,
        
        [Description("36 - 50")]
        _36_50 = 4,
        
        [Description("Above 50")]
        Above_50 = 5
    }
}