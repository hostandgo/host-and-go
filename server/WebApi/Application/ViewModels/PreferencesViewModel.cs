using System;
using System.Collections.Generic;
using WebApi.Domain.Enums;
using WebApi.Domain.Models;

namespace WebApi.Application.ViewModels
{
    public class PreferencesViewModel
    {
        public PreferencesViewModel(string description, IEnumerable<RestrictionsTypes> restrictions, IEnumerable<Interest> interests)
        {
            Description = description;
            Restrictions = restrictions;
            Interests = interests;
        }

        public string Description { get; set; }
        public IEnumerable<RestrictionsTypes> Restrictions { get; set; }
        public IEnumerable<Interest> Interests { get; set; }
    }
}