const getInitialOnboardingSteps = {
    id: 3,
    steps:[
        {
            title: 'Preferences page',
            target: '#preferences-title',
            content: `
                Take advantage of this space to talk a little about the things you like and what your topics of interest are
            `,
            placement: 'center',
            locale: {
                last: 'Ok'
            }
        }
    ]
}

const getAfterSave = {
    id: 4,
    steps: [
        {
            title: 'Just a moment to complete',
            target: '#side-bar-host-item',
            content: `
                The last step of your registration is inform your data as Host.
                Click here to go to Host screen
            `,
            disableBeacon: true,
            disableOverlayClose: true,
            placement: 'right',
            spotlightClicks: false,
            styles: {
                options: {
                    zIndex: 10000,
                },
            },
            locale: {
                last: `Ok, I got it`
            },
        }
    ]
}

export { getInitialOnboardingSteps, getAfterSave }
