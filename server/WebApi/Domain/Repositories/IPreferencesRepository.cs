using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WebApi.Domain.Models;

namespace WebApi.Domain.Repositories
{
    public interface IPreferencesRepository
    {
        Task<Preferences> FindByUserId(Guid userId);
        Task<Preferences> FindByFilter(Expression<Func<Preferences, bool>> filter);
        Task<IEnumerable<Preferences>> ListByFilter(Expression<Func<Preferences, bool>> filter);
        Task Insert(Preferences preferences);
        Task Remove(Preferences preferences);
        Task<Preferences> Update(Preferences preferences);
    }
}