import React from 'react';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import useStyles from './useStyles';
import StarIcon from '@material-ui/icons/Star';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import GridImageList from '../../../../components/grid-image-list';
import { Avatar, Typography } from '@material-ui/core';

export default function HostDrawer({open, onClose, user}){
    const classes = useStyles();

    return(
        <Drawer 
            className={classes.drawer}
            anchor="right" 
            open={open} 
            onClose={onClose}
            classes={{
                paper: classes.drawerPaper,
            }}
        >
            <div className={classes.drawerHeader}>
                <IconButton onClick={onClose}>
                    <ChevronRightIcon />
                </IconButton>
            </div>
            <Divider />
            <div className={classes.firstSection}>
                <div className={classes.wrapAvatar}>
                    <Avatar
                        alt={user.name}
                        src={user.profile.picture}
                        className={classes.bigAvatar}
                        />
                    <div>
                        <StarIcon />
                        <StarIcon />
                        <StarIcon />
                        <StarIcon />
                        <StarBorderIcon />
                    </div>
                </div>
                <div>
                    <Typography variant="h5" gutterBottom>{user.profile.name}</Typography>
                    <Typography>{user.profile.description}</Typography>
                </div>
            </div>
            <div className={classes.secondSection}>
                <div>
                    <Typography variant="h5" gutterBottom>Languages</Typography>
                    {
                        user.profile.languages.map(l => 
                            <Typography>{`${l.name} - ${l.level}`}</Typography>
                        )
                    }
                </div>
                <div>
                    <Typography variant="h5" gutterBottom>Interests</Typography>
                    {
                        user.profile.interests.map(i => 
                            <Typography>{`${i.interest} - ${i.complementInterest}`}</Typography>
                        )
                    }
                </div>
                <div>
                    <Typography variant="h5" gutterBottom>Resctrictions</Typography>
                    {
                        user.host.restrictions.map(r => 
                            <Typography>{r.description}</Typography>
                        )
                    }
                </div>
            </div>
            
            <div className={classes.threeSection}>
                <Typography>{`Your host will be able to devote to you ${user.host.hoursInteractTravel} hours a day and you can do things like:`}</Typography>
                <Typography>{user.host.describeWhatCanDoTogether}</Typography>
            </div>
            <Divider />
            <div className={classes.threeSection}>
                <Typography variant="h5" gutterBottom>{user.profile.country}</Typography>
                <Typography variant="h5" gutterBottom>Accommodation description</Typography>
                <Typography>{user.host.description}</Typography>
                <Typography style={{marginTop: 8}} variant="h5" gutterBottom>City description</Typography>
                <Typography>{user.host.cityDescription}</Typography>
            </div>
            <div style={{paddingBottom: 30}} className={classes.secondSection}>
                <div>
                    <Typography variant="h5" gutterBottom>Type of room</Typography>
                    <Typography>{user.host.typeOfRoom}</Typography>
                </div>
                <div>
                    <Typography variant="h5" gutterBottom>Bathroom Access</Typography>
                    <Typography>{user.host.bathroomAccess}</Typography>
                </div>
                <div>
                    <Typography variant="h5" gutterBottom>City type</Typography>
                    <Typography>{user.host.cityType}</Typography>
                </div>
                <div>
                    <Typography variant="h5" gutterBottom>Attractions</Typography>
                    {
                        user.host.attractions.map(a => 
                            <Typography>{a.description}</Typography>
                        )
                    }
                </div>
            </div>
            <GridImageList 
                files={user.host.photos}
            />
        </Drawer>
    )
}