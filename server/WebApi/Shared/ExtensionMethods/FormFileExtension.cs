using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using WebApi.Shared.Utils;

public static class FormFileExtension
{
    public static List<IFormFile> GetFilesToAdd(this List<IFormFile> formFile, IEnumerable<string> collection)
    {
        var filesToAdd = formFile
            .Select(f => f.FileName)
            .Except(collection);

        return formFile.Where(file => filesToAdd.Contains(file.FileName)).ToList();
    }

    public static List<string> GetFileNamesToRemove(this List<IFormFile> formFile, IEnumerable<string> collection)
    => collection.Except(formFile.Select(f => f.FileName)).ToList();
}
