namespace WebApi.Domain.Enums
{
    public enum RestrictionsTypes
    {
        Smoke = 1,
        Deficient = 2,
        Religion = 3,
        Pets = 4,
        Sound = 5,
        Children = 6
    }
}