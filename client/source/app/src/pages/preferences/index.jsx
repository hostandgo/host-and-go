import React, { useEffect, useState } from 'react';
import useStyles from './useStyles';
import useWindowWidth from '../../hooks/useWindowWidth';
import { Paper, TextField, Typography } from '@material-ui/core';
import api from '../../api';
import InterestsCombos from '../../components/interests-combos';
import DefaultWrapper from '../../components/default-wrapper';
import ButtonSaveContext from '../../components/button-save-context';
import Onboarding from '../../components/onboarding';
import { getInitialOnboardingSteps, getAfterSave } from './onboarding-steps';
import { interestViewModelToState, interestStateToPayload } from '../../components/interests-combos/mappers';

export default function Preferences() {
    const classes = useStyles();
    const windowWidth = useWindowWidth();
    const isMobile = windowWidth <= 900;

    const [interests, setInterests] = useState([]);
    const [description, setDescription] = useState('');
    const [loading, setLoading] = useState(false);
    const [success, setSuccess] = useState(false);
    const [screenLoading, setScreenLoading] = useState('');
    const [currentOnboarding, setCurrentOnboarding] = useState({
        id: 0,
        steps: []
    })
    const [showOnboarding, setShowOnBoarding] = useState(false);

    async function savePreferences() {
        if (!loading) {
            setSuccess(false);
            await api.preferences.update(
                {
                    description: description,
                    interests: interestStateToPayload(interests),
                },
                setLoading
            )
            setSuccess(true);
            setTimeout(() => {
                setSuccess(false);
            }, 2000);

            setCurrentOnboarding(getAfterSave)
            setShowOnBoarding(true);
        }
    }

    useEffect(() => {
        async function getProfile() {
            const result = await api.preferences.get(setScreenLoading)
            if (!result.hasError) {
                const mapInterestToViewModel = interestViewModelToState(result.response.interests);
                setInterests(mapInterestToViewModel.length > 0 ? mapInterestToViewModel : []);
                setDescription(result.response.description)
            } else {
                setCurrentOnboarding(getInitialOnboardingSteps)
                setShowOnBoarding(true);
            }
        };
        getProfile()
    }, [
        setInterests,
        setDescription,
        setScreenLoading,
        setShowOnBoarding,
        setCurrentOnboarding
    ])

    return (
        <DefaultWrapper>
            <Onboarding
                run={showOnboarding}
                customSteps={currentOnboarding.steps}
                id={currentOnboarding.id}
            />
            <div className={isMobile ? classes.containerMobile : classes.containerDesktop}>
                <Typography id="preferences-title" variant="h4" gutterBottom>Preferences</Typography>
                <div
                    style={{
                        marginBottom: 15,
                        width: '100%'
                    }}
                >
                    <Typography>
                        Add your personal preferences, these will be used to find travelers and can also be used to find profile hosts near you.
                </Typography>
                </div>
                <Paper className={isMobile ? classes.paperMobile : classes.paperDesktop}>
                    <Typography className={classes.title}>Describe your profile in this field, talk about what you like to do, listen to, see and any other information you find relevant</Typography>
                    <div className={isMobile ? classes.formContainerMobile : classes.formContainerDesktop}>
                        <TextField
                            // disabled={disabled}
                            id="outlined-description"
                            label="Description"
                            className={isMobile ? classes.textFieldMobile : classes.textFieldDescribe}
                            value={description}
                            onChange={e => setDescription(e.target.value)}
                            multiline
                            rows="4"
                            rowsMax="4"
                            margin="normal"
                            variant="outlined"
                        />
                        <InterestsCombos
                            interests={interests}
                            setInterests={setInterests}
                        // disabled={disabled}
                        />
                    </div>
                </Paper>
            </div>
            <ButtonSaveContext
                onSave={savePreferences}
                success={success}
                loading={loading}
            />
        </DefaultWrapper>
    )
}