import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

const ColorCircularProgress = withStyles(theme => ({
  root: {
    color: theme.palette.primary.main,
  },
}))(CircularProgress);

export default function Loader(props) {
    return(
        <ColorCircularProgress disableShrink style={props.style} size={props.size} thickness={3} />
    )
}