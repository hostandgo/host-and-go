using WebApi.Domain.Enums;

namespace WebApi.Domain.Models
{
    public class ComplementInterest
    {
        public ComplementInterest(ComplementInterestType type)
        {
            this.Type = type;
            this.Description = type.GetDescription();

        }
        public ComplementInterestType Type { get; set; }
        public string Description { get; set; }
    }
}