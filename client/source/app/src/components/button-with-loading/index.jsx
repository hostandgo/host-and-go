import React from 'react';
import clsx from 'clsx';
import useStyles from './useStyles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';

export default function ButtonWithLoading(props){
    const classes = useStyles();
    
    const buttonClassname = clsx({
        [classes.buttonSuccess]: props.success,
    });
    return(
        <div className={classes.wrapper}>
            <Button
            style={{width: '100%', ...props.styles}}
            variant="contained"
            color="primary"
            className={buttonClassname}
            disabled={props.loading}
            onClick={props.handleButtonClick}
            >
                {props.label}
                {props.loading && <CircularProgress size={24} className={classes.buttonProgress} />}
            </Button>
        </div>
    )
}