import React from 'react';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Copyright from '../copyright';

export default function FooterBox(props){
    const { classes } = props; 
    return(
        <Box className={classes.box} mt={5}>
            <Button onClick={props.onClick}>
                {props.label}
            </Button>
            <Copyright />
        </Box>
    )
}