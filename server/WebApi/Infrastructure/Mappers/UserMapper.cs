using WebApi.Domain.Models;
using MongoDB.Bson.Serialization;

namespace WebApi.Infrastructure.Mappers
{
    public class UserMapper
    {
        public static void Configure()
        {
            BsonClassMap.RegisterClassMap<User>(map => 
            {
                map.AutoMap();
                map.MapCreator(u => new User(u.Authentication.Email, u.Authentication.Password));
            });
        }

    }
}