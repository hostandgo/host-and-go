using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using WebApi.Domain.Models;

namespace WebApi.Web.Controllers
{
    [Route("api/data")]
    [ApiController]
    public class DataController: ControllerBase
    {
        
        [HttpGet("get-interests")]
        public ActionResult<IEnumerable<Interest>> GetInterestList()
        => Ok(Interest.GetInterests());
    }
}