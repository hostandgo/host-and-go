namespace WebApi.Domain.ValueObjects
{
    public class Name
    {
        public Name(string firstName, string secondName)
        {
            this.FirstName = firstName;
            this.SecondName = secondName;

        }
        public string FirstName { get; private set; }
        public string SecondName { get; private set; }
        public string FullName => $"{FirstName} {SecondName}";
    }
}