import React, { useEffect, useState } from 'react';
import { TextField, MenuItem } from '@material-ui/core';
import geoNamesApi from '../../../../api/geo-names-api';
import { resultToViewModel } from './mappers';
import useStyles from './useStyles';

const countryItem = (item) => {
    return (
        <MenuItem key={item.id} value={item.id}>
            {item.name}
        </MenuItem>
    )
}

export default function SelectRegion({
    errors,
    continentId,
    setContinent,
    countryId,
    setCountry,
    stateId,
    setState,
    regionId,
    setRegion,
}) {
    const classes = useStyles();
    const [continents, setContinents] = useState([]);
    const [countries, setCountries] = useState([]);
    const [states, setStates] = useState([]);
    const [regions, setRegions] = useState([]);

    useEffect(() => {
        async function getContinents() {
            const result = await geoNamesApi.getContinents();
            const continentsViewModel = resultToViewModel(result);
            setContinents(continentsViewModel);
        }
        getContinents();
    }, [])

    useEffect(() => {
        async function getCountries() {
            const result = await geoNamesApi.getCountries(continentId);
            const countriesViewModel = resultToViewModel(result);
            setCountries(countriesViewModel);
        }
        if (continentId)
            getCountries();
    }, [continentId])

    useEffect(() => {

        async function getStates() {
            const result = await geoNamesApi.getStates(countryId);
            const statesViewModel = resultToViewModel(result);
            setStates(statesViewModel);
        }

        if (countryId)
            getStates();
    }, [countryId])

    useEffect(() => {
        async function getRegions() {
            const result = await geoNamesApi.getRegions(stateId);
            const regionsViewModel = resultToViewModel(result);
            setRegions(regionsViewModel);
        }
        if (stateId)
            getRegions();
    }, [stateId])

    function handleSelect(setter, collection, id) {
        const name = collection.find(item => item.id === id).name;
        setter({ id, name });
    }

    return (
        <div className={classes.container}>
            <TextField
                error={errors && !continentId}
                helperText={(errors && !continentId) && "Continent is required"}
                id="outlined-select-continent"
                select
                label="Select your continent"
                className={classes.textFieldRegion}
                value={continentId}
                onChange={e => handleSelect(setContinent, continents, e.target.value)}
                margin="normal"
                variant="outlined"
            >
                {continents.map(countryItem)}
            </TextField>
            <TextField
                error={errors && !countryId}
                helperText={(errors && !countryId) && "Country is required"}
                id="outlined-select-country"
                select
                label="Select your country"
                className={classes.textFieldRegion}
                value={countryId}
                onChange={e => handleSelect(setCountry, countries, e.target.value)}
                margin="normal"
                variant="outlined"
                disabled={continentId === ''}
            >
                {countries.map(countryItem)}
            </TextField>

            <TextField
                error={errors && !stateId}
                helperText={(errors && !stateId) && "State is required"}
                id="outlined-select-state"
                select
                label="Select your state"
                className={classes.textFieldRegion}
                value={stateId}
                onChange={e => handleSelect(setState, states, e.target.value)}
                margin="normal"
                variant="outlined"
                disabled={countryId === ''}
            >
                {states.map(countryItem)}
            </TextField>

            <TextField
                error={errors && !regionId}
                helperText={(errors && !regionId) && "Region is required"}
                id="outlined-select-region"
                select
                label="Select your city/region"
                className={classes.textFieldRegion}
                value={regionId}
                onChange={e => handleSelect(setRegion, regions, e.target.value)}
                margin="normal"
                variant="outlined"
                disabled={stateId === ''}
            >
                {regions.map(countryItem)}
            </TextField>
        </div>
    )
}