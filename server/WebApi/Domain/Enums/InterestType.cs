using System.ComponentModel;

namespace WebApi.Domain.Enums
{
    public enum InterestType
    {

        [Description("Martial Arts")]
        MartialArts = 1,

        [Description("Sports With Ball")]
        SportsWithBall = 2,

        [Description("Pool Sports")]
        PoolSports = 3,

        [Description("Water Sports")]
        WaterSports = 4,

        [Description("Olympic Gymnastics")]
        OlympicGymnastics = 5,

        [Description("Gym")]
        Gym = 6,

        [Description("Racket Sports")]
        RacketSports = 7,

        [Description("Outdoor Sports")]
        OutdoorSports = 8,

        [Description("BoardGames")]
        BoardGames = 9,

        [Description("Indoor")]
        Indoor = 10,

        [Description("On Ice")]
        OnIce = 11,

        [Description("Radical")]
        Radical = 12,

        [Description("Field")]
        Field = 13,

        [Description("Music")]
        Music = 14,

        [Description("Dance")]
        Dance = 15,

        [Description("Volunteer Training")]
        VolunteerTraining = 16,

        [Description("Gastronomy Beverages")]
        GastronomyBeverages = 17,

        [Description("Arts")]
        Arts = 18,

        [Description("Electronic Games")]
        ElectronicGames = 19,

        [Description("Language")]
        Language = 20
    }
}