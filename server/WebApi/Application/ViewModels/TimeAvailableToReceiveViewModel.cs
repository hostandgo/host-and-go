using System;

namespace WebApi.Application.ViewModels
{
    public class TimeAvailableToReceiveViewModel
    {
        public TimeAvailableToReceiveViewModel(DateTime start, DateTime end)
        {
            this.Start = start;
            this.End = end;

        }
        public DateTime Start { get; private set; }
        public DateTime End { get; private set; }
    }
}