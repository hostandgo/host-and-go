using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using WebApi.Domain.Repositories;

namespace WebApi.Application.QuerySide.Queries
{
    public class GetPhotosQuery : IRequest<string[]>
    {
        public Guid UserId;

        public GetPhotosQuery(Guid userId)
        {
            this.UserId = userId;
        }
    }

    public class GetPhotosQueryHandler : IRequestHandler<GetPhotosQuery, string[]>
    {
        private readonly IHostRepository _hostRepository;

        public GetPhotosQueryHandler(IHostRepository hostRepository)
        {
            _hostRepository = hostRepository;
        }

        public async Task<string[]> Handle(GetPhotosQuery request, CancellationToken cancellationToken)
        {
            var host = await _hostRepository.FindByUserId(request.UserId);
            return host.Photos.ToArray();
        }
    }
}