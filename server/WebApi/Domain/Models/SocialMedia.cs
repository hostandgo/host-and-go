namespace WebApi.Domain.Models
{
    public class SocialMedia: BaseEntity
    {
        public string Instagram { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Linkedin { get; set; }
    }
}