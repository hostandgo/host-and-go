import React from 'react';
import { TextField, MenuItem, Fab } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import useStyles from './useStyles';
import langmap from 'langmap';

const levelLanguages = [
    {
        value: 'Beginner',
        label: 'Beginner',
    },
    {
        value: 'Intermediate',
        label: 'Intermediate',
    },
    {
        value: 'Fluent',
        label: 'Fluent',
    }
];

export default function SelectLanguage({
    currentLanguage,
    setCurrentLanguage,
    level,
    setLevel,
    addLanguage
}) {

    const allLanguages = Object.values(langmap);
    const classes = useStyles();
    return (
        <div className={classes.container}>
            <TextField
                id="outlined-language"
                select
                label="Language"
                style={{ width: '100%' }}
                value={currentLanguage}
                onChange={e => setCurrentLanguage(e.target.value)}
                margin="normal"
                variant="outlined"
            >
                {allLanguages.map(option => (
                    <MenuItem key={option.englishName} value={option.englishName}>
                        {option.englishName}
                    </MenuItem>
                ))}
            </TextField>
            <div style={{ display: 'flex', width: '100%', alignItems: 'center', justifyContent: 'space-around' }}>
                <TextField
                    id="outlined-select-language-level"
                    select
                    label="Nivel"
                    style={{ width: '80%' }}
                    value={level}
                    onChange={e => setLevel(e.target.value)}
                    margin="normal"
                    variant="outlined"
                >
                    {levelLanguages.map(option => (
                        <MenuItem key={option.value} value={option.value}>
                            {option.label}
                        </MenuItem>
                    ))}
                </TextField>
                <Fab onClick={addLanguage} color="primary" aria-label="add" className={classes.fabLanguage}>
                    <AddIcon />
                </Fab>
            </div>
        </div>
    )
}