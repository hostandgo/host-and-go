using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WebApi.Domain.Models;

namespace WebApi.Domain.Repositories
{
    public interface IHostRepository
    {
        Task<Host> FindByUserId(Guid userId);
        Task<Host> FindByFilter(Expression<Func<Host, bool>> filter);
        Task<IEnumerable<Host>> ListByFilter(Expression<Func<Host, bool>> filter);
        Task Insert(Host host);
        Task Remove(Host host);
        Task<Host> Update(Host host);    
    }
}