import React, { useState } from 'react';
import { KeyboardDatePicker } from '@material-ui/pickers';
import { TextField, MenuItem } from '@material-ui/core';
import DefaultWrapper from '../../components/default-wrapper';
import Fab from '@material-ui/core/Fab';
import List from '@material-ui/core/List';
import HostItem from './components/host-item';
import SearchIcon from '@material-ui/icons/Search';
import useWindowWidth from '../../hooks/useWindowWidth';
import useStyles from './useStyles';
import HostDrawer from './components/host-drawer';
import Loader from '../../components/loader';
import countryAndCities from '../../services/countries-and-cities';

const emptyUser = {
    id: '',
    profile: {
        description: '',
        picture: '',
        name: '',
        country: '',
        languages: [],
        interests: []
    },
    host: {
        description: '',
        restrictions: [],
        cityDescription: '',
        typeOfRoom: '',
        bathroomAccess: '',
        hoursInteractTravel: 0,
        cityType: '',
        attractions: [],
        describeWhatCanDoTogether: '',
        photos: []
    }
}

const fakeUsers = [
    {
        id: '456958b0-d4c6-4e70-9fcf-b61ae2fbdb70',
        profile: {
            description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer ',
            picture: "/static/images/user-pic.jpeg",
            name: 'Odilo',
            country: 'Brasil',
            languages: [
                {
                    name: 'Portugues',
                    level: 'Native'
                }
            ],
            interests: [
                {
                    interest: 'Martial Arts',
                    complementInterest: 'Kung Fu'
                }
            ]
        },
        host: {
            description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer ',
            restrictions: [
                {
                    description: 'Smoke'
                }
            ],
            cityDescription: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem',
            typeOfRoom: 'Private',
            bathroomAccess: 'Shared',
            hoursInteractTravel: 3,
            cityType: 'Center',
            attractions: [
                {
                    description: 'Shopping'
                },
                {
                    description: 'Park',
                }
            ],
            describeWhatCanDoTogether: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer'
        }
    },
    {
        id: 'd783d226-33b7-4415-95d9-b7c38bf5b78d',
        profile: {
            description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer ',
            picture: "/static/images/carlos.jpg",
            name: 'Carlos',
            country: 'Brasil',
            languages: [
                {
                    name: 'Portugues',
                    level: 'Native'
                }
            ],
            interests: [
                {
                    interest: 'Martial Arts',
                    complementInterest: 'Kung Fu'
                }
            ]
        },
        host: {
            description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer ',
            restrictions: [
                {
                    description: 'Smoke'
                }
            ],
            cityDescription: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem',
            typeOfRoom: 'Private',
            bathroomAccess: 'Shared',
            hoursInteractTravel: 3,
            cityType: 'Center',
            attractions: [
                {
                    description: 'Shopping'
                },
                {
                    description: 'Park',
                }
            ],
            describeWhatCanDoTogether: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer'    
        }    
    },
    {
        id: '883e7a8c-3bcd-4520-ad22-2488961baed8',
        profile: {
            description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer ',
            picture: "/static/images/leo.jpg",
            name: 'Leonardo',
            country: 'Brasil',
            languages: [
                {
                    name: 'Portugues',
                    level: 'Native'
                }
            ],
            interests: [
                {
                    interest: 'Martial Arts',
                    complementInterest: 'Kung Fu'
                }
            ]
        },
        host: {
            description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer ',
            restrictions: [
                {
                    description: 'Smoke'
                }
            ],
            cityDescription: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem',
            typeOfRoom: 'Private',
            bathroomAccess: 'Shared',
            hoursInteractTravel: 3,
            cityType: 'Center',
            attractions: [
                {
                    description: 'Shopping'
                },
                {
                    description: 'Park',
                }
            ],
            describeWhatCanDoTogether: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer'
        }
    }
]

const countryItem = (country) => {
    return (
        <MenuItem key={country} value={country}>
            {country}
        </MenuItem>
    )
}

export default function Travel() {

    const classes = useStyles();
    const windowWidth = useWindowWidth();
    const isMobile = windowWidth <= 900;

    const [currentStartDate, setCurrentStartDate] = useState(null);
    const [currentEndDate, setCurrentEndDate] = useState(null);
    const [open, setOpen] = useState(false);
    const [startError, setStartError] = useState(false);
    const [endError, setEndError] = useState(false);
    const [countryError, setCountryError] = useState(false);
    const [hosts, setHosts] = useState([]);
    const [userToShowInfo, setUserToShowInfo] = useState(emptyUser);
    const [country, setCountry] = useState('');
    const [isLoading, setIsLoading] = useState(false);

    function openModal(user) {
        setOpen(true);
        setUserToShowInfo(user);
    }

    function search() {
        var erros = validateSearch();
        if(erros.length > 0){
            erros.map(e => e());
            return;
        }
        setIsLoading(true);
        setTimeout(() => {
            setIsLoading(false);
            setHosts(fakeUsers);
        }, 3000)
        
    }

    function validateSearch(){
        setStartError(false);
        setEndError(false);
        setCountryError(false);
        var errorsHandlers = [];
        
        if(!currentStartDate)
            errorsHandlers.push(() => setStartError(true));
            
        if(!currentEndDate)
            errorsHandlers.push(() => setEndError(true));

        if(!country)
            errorsHandlers.push(() => setCountryError(true));

        return errorsHandlers;
    }

    return (
        <DefaultWrapper>
            <div className={classes.root}>
                <div className={classes.inputs}>
                    <KeyboardDatePicker
                        error={startError}
                        helperText={startError && "Is Required"}
                        autoOk
                        inputVariant="outlined"
                        variant="inline"
                        className={isMobile ? classes.textFieldMobile : classes.textFieldDesktop}
                        format="MM/dd/yyyy"
                        margin="normal"
                        id="date-picker-inline"
                        label="Start"
                        value={currentStartDate}
                        onChange={value => setCurrentStartDate(value)}
                    />
                    <KeyboardDatePicker
                        error={endError}
                        helperText={endError && "Is Required"}
                        autoOk
                        inputVariant="outlined"
                        variant="inline"
                        className={isMobile ? classes.textFieldMobile : classes.textFieldDesktop}
                        format="MM/dd/yyyy"
                        margin="normal"
                        id="date-picker-inline"
                        label="End"
                        value={currentEndDate}
                        onChange={value => setCurrentEndDate(value)}
                    />
                    <TextField
                        error={countryError}
                        helperText={countryError && "Is Required"}
                        id="outlined-select-country"
                        select
                        label="Country"
                        className={isMobile ? classes.textFieldMobile : classes.textFieldDesktop}
                        value={country}
                        onChange={e => setCountry(e.target.value)}
                        SelectProps={{
                            MenuProps: {
                                className: classes.menu,
                            },
                        }}
                        margin="normal"
                        variant="outlined"
                    >
                        {Object.keys(countryAndCities).map(countryItem)}
                    </TextField>
                    <Fab onClick={search} variant="extended" color="primary" aria-label="add" className={classes.margin}>
                        <SearchIcon className={classes.extendedIcon} />
                        Find Hosts
                    </Fab>
                </div>
                <div className={classes.listWrapper}>
                    <List>
                        {
                            isLoading ? 
                                <Loader 
                                    style={{
                                        top: '45%',
                                        left: '35%',
                                        position: 'absolute'
                                    }}
                                    size={80}
                                />
                            :
                            hosts.map(u => 
                                <HostItem 
                                    user={u} 
                                    handleRemove={() => {}}
                                    infoOnClick={() => openModal(u)}
                                />
                            )
                        }
                    </List>
                </div>
            </div>
            <HostDrawer 
                open={open}
                onClose={() => setOpen(false)}
                user={userToShowInfo}
            />
        </DefaultWrapper>
    )
}