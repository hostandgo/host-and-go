using System;
using System.Threading;
using System.Threading.Tasks;
using WebApi.Domain.Exceptions;
using WebApi.Infrastructure;
using MediatR;
using MongoDB.Driver;

namespace WebApi.Application.QuerySide.Queries
{
    public class GetWeeksQuery: IRequest<int>
    {
        public GetWeeksQuery(Guid userId)
        {
            UserId = userId;
        }

        public Guid UserId { get; set; }
    }

    public class GetWeeksQueryHandler : IRequestHandler<GetWeeksQuery, int>
    {
        private readonly MongoContext _context;

        public GetWeeksQueryHandler(MongoContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(GetWeeksQuery request, CancellationToken cancellationToken)
        {
            var userResult = await _context.Users.FindAsync(u => u.Id == request.UserId);
            var user = await userResult.FirstOrDefaultAsync();
            if(user == null)
                throw new NotFoundException("user not found");
            
            return user.Weeks;
        }
    }
}