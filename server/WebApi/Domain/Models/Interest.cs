using System.Collections.Generic;
using System.Linq;
using WebApi.Domain.Enums;

namespace WebApi.Domain.Models
{
    public class Interest
    {
        public Interest(InterestType type, IEnumerable<ComplementInterest> complementInterests)
        {
            this.Type = type;
            this.Description = type.GetDescription();
            this.Complements = complementInterests.ToHashSet();
        }

        public Interest(InterestType type, HashSet<ComplementInterest> complementInterests)
        {
            this.Type = type;
            this.Description = type.GetDescription();
            this.Complements = complementInterests;
        }
        public InterestType Type { get; private set; }
        public string Description { get; private set; }
        public HashSet<ComplementInterest> Complements { get; private set; }

        public Interest UpdateComplements(HashSet<ComplementInterest> complements)
        {
            Complements = complements;
            return this;
        }

        public static IEnumerable<Interest> GetInterests()
        {
            yield return new Interest(InterestType.MartialArts, GetComplementInterestTypes(InterestType.MartialArts));
            yield return new Interest(InterestType.SportsWithBall, GetComplementInterestTypes(InterestType.SportsWithBall));
            yield return new Interest(InterestType.PoolSports, GetComplementInterestTypes(InterestType.PoolSports));
            yield return new Interest(InterestType.WaterSports, GetComplementInterestTypes(InterestType.WaterSports));
            yield return new Interest(InterestType.OlympicGymnastics, GetComplementInterestTypes(InterestType.OlympicGymnastics));
            yield return new Interest(InterestType.Gym, GetComplementInterestTypes(InterestType.Gym));
            yield return new Interest(InterestType.RacketSports, GetComplementInterestTypes(InterestType.RacketSports));
            yield return new Interest(InterestType.OutdoorSports, GetComplementInterestTypes(InterestType.OutdoorSports));
            yield return new Interest(InterestType.BoardGames, GetComplementInterestTypes(InterestType.BoardGames));
            yield return new Interest(InterestType.Indoor, GetComplementInterestTypes(InterestType.Indoor));
            yield return new Interest(InterestType.OnIce, GetComplementInterestTypes(InterestType.OnIce));
            yield return new Interest(InterestType.Radical, GetComplementInterestTypes(InterestType.Radical));
            yield return new Interest(InterestType.Field, GetComplementInterestTypes(InterestType.Field));
            yield return new Interest(InterestType.Music, GetComplementInterestTypes(InterestType.Music));
            yield return new Interest(InterestType.Dance, GetComplementInterestTypes(InterestType.Dance));
            yield return new Interest(InterestType.VolunteerTraining, GetComplementInterestTypes(InterestType.VolunteerTraining));
            yield return new Interest(InterestType.GastronomyBeverages, GetComplementInterestTypes(InterestType.GastronomyBeverages));
            yield return new Interest(InterestType.Arts, GetComplementInterestTypes(InterestType.Arts));
            yield return new Interest(InterestType.ElectronicGames, GetComplementInterestTypes(InterestType.ElectronicGames));
            yield return new Interest(InterestType.Language, GetComplementInterestTypes(InterestType.Language));
        }

        private static IEnumerable<ComplementInterest> GetComplementInterestTypes(InterestType interestType)
        {
            if (interestType == InterestType.MartialArts)
            {
                yield return new ComplementInterest(ComplementInterestType.Boxe);
                yield return new ComplementInterest(ComplementInterestType.JiuJitsu);
                yield return new ComplementInterest(ComplementInterestType.Taekwondo);
                yield return new ComplementInterest(ComplementInterestType.Judo);
                yield return new ComplementInterest(ComplementInterestType.Karate);
                yield return new ComplementInterest(ComplementInterestType.MuayThai);
                yield return new ComplementInterest(ComplementInterestType.KravMaga);
                yield return new ComplementInterest(ComplementInterestType.MMA);
                yield return new ComplementInterest(ComplementInterestType.Kickboxing);
            }
            else if (interestType == InterestType.SportsWithBall)
            {
                yield return new ComplementInterest(ComplementInterestType.Soccer);
                yield return new ComplementInterest(ComplementInterestType.AmericanFootball);
                yield return new ComplementInterest(ComplementInterestType.Volleyball);
                yield return new ComplementInterest(ComplementInterestType.Basketball);
                yield return new ComplementInterest(ComplementInterestType.Handball);
            }
            else if (interestType == InterestType.PoolSports)
            {
                yield return new ComplementInterest(ComplementInterestType.Swimming);
                yield return new ComplementInterest(ComplementInterestType.SynchronizedSwimming);
                yield return new ComplementInterest(ComplementInterestType.WaterPolo);
                yield return new ComplementInterest(ComplementInterestType.OrnamentalJump);
            }
            else if (interestType == InterestType.WaterSports)
            {
                yield return new ComplementInterest(ComplementInterestType.Canoeing_Rowing);
                yield return new ComplementInterest(ComplementInterestType.Surf);
                yield return new ComplementInterest(ComplementInterestType.Bodyboarding);
                yield return new ComplementInterest(ComplementInterestType.Candle);
                yield return new ComplementInterest(ComplementInterestType.Stand_Up_Paddle);
                yield return new ComplementInterest(ComplementInterestType.Water_Skiing);
                yield return new ComplementInterest(ComplementInterestType.Rafting);
                yield return new ComplementInterest(ComplementInterestType.Diving);
            }
            else if (interestType == InterestType.OlympicGymnastics)
            {

            }
            else if (interestType == InterestType.Gym)
            {
                yield return new ComplementInterest(ComplementInterestType.Workout);
                yield return new ComplementInterest(ComplementInterestType.Cross_fit);
                yield return new ComplementInterest(ComplementInterestType.Bodybuilding);
                yield return new ComplementInterest(ComplementInterestType.Yoga);

            }
            else if (interestType == InterestType.RacketSports)
            {
                yield return new ComplementInterest(ComplementInterestType.Tenis);
                yield return new ComplementInterest(ComplementInterestType.Badminton);
                yield return new ComplementInterest(ComplementInterestType.Squash);

            }
            else if (interestType == InterestType.OutdoorSports)
            {
                yield return new ComplementInterest(ComplementInterestType.Walking);
                yield return new ComplementInterest(ComplementInterestType.Running);
                yield return new ComplementInterest(ComplementInterestType.Skateboard);
                yield return new ComplementInterest(ComplementInterestType.Hiking_And_Trails);
                yield return new ComplementInterest(ComplementInterestType.Cycling_And_Mountain_biking);

            }
            else if (interestType == InterestType.BoardGames)
            {
                yield return new ComplementInterest(ComplementInterestType.Table_Tennis);
                yield return new ComplementInterest(ComplementInterestType.Chess_And_Board_Games);
                yield return new ComplementInterest(ComplementInterestType.Snooker);
                yield return new ComplementInterest(ComplementInterestType.Bowling);

            }
            else if (interestType == InterestType.Indoor)
            {
                yield return new ComplementInterest(ComplementInterestType.Hockey);
                yield return new ComplementInterest(ComplementInterestType.Tchoukball);
                yield return new ComplementInterest(ComplementInterestType.Volleyball);
                yield return new ComplementInterest(ComplementInterestType.Basketball);
                yield return new ComplementInterest(ComplementInterestType.Handball);

            }
            else if (interestType == InterestType.OnIce)
            {
                yield return new ComplementInterest(ComplementInterestType.Ski);
                yield return new ComplementInterest(ComplementInterestType.Snowboard);
                yield return new ComplementInterest(ComplementInterestType.Skating);
                yield return new ComplementInterest(ComplementInterestType.Ice_Hockey);
                yield return new ComplementInterest(ComplementInterestType.Sledding);

            }
            else if (interestType == InterestType.Radical)
            {
                yield return new ComplementInterest(ComplementInterestType.Skydiving_Hang_Gliding);
                yield return new ComplementInterest(ComplementInterestType.Rafting);
                yield return new ComplementInterest(ComplementInterestType.Rappelling_And_Mountaineering);

            }
            else if (interestType == InterestType.Field)
            {
                yield return new ComplementInterest(ComplementInterestType.Soccer);
                yield return new ComplementInterest(ComplementInterestType.AmericanFootball);
                yield return new ComplementInterest(ComplementInterestType.Cricket);
                yield return new ComplementInterest(ComplementInterestType.Rugby);
                yield return new ComplementInterest(ComplementInterestType.Golf);
                yield return new ComplementInterest(ComplementInterestType.Baseball);
                yield return new ComplementInterest(ComplementInterestType.Lacrosse);
                yield return new ComplementInterest(ComplementInterestType.Equestrianism);
                yield return new ComplementInterest(ComplementInterestType.Athletics);

            }
            else if (interestType == InterestType.Music)
            {
                yield return new ComplementInterest(ComplementInterestType.Electronics);
                yield return new ComplementInterest(ComplementInterestType.Country);
                yield return new ComplementInterest(ComplementInterestType.Reggae);
                yield return new ComplementInterest(ComplementInterestType.HIPHOP);
                yield return new ComplementInterest(ComplementInterestType.Classical);
                yield return new ComplementInterest(ComplementInterestType.RapFunkSoul);
                yield return new ComplementInterest(ComplementInterestType.Jazz);
                yield return new ComplementInterest(ComplementInterestType.Folk);
                yield return new ComplementInterest(ComplementInterestType.Rock);
                yield return new ComplementInterest(ComplementInterestType.Blues);
                yield return new ComplementInterest(ComplementInterestType.Religious);

            }
            else if (interestType == InterestType.Dance)
            {
                yield return new ComplementInterest(ComplementInterestType.Classical);
                yield return new ComplementInterest(ComplementInterestType.Street);
                yield return new ComplementInterest(ComplementInterestType.Ballroom);

            }
            else if (interestType == InterestType.VolunteerTraining)
            {
                yield return new ComplementInterest(ComplementInterestType.Volunteering);
                yield return new ComplementInterest(ComplementInterestType.Internship);

            }
            else if (interestType == InterestType.GastronomyBeverages)
            {
                yield return new ComplementInterest(ComplementInterestType.Cooking);
                yield return new ComplementInterest(ComplementInterestType.Patisserie);
                yield return new ComplementInterest(ComplementInterestType.Wine);
                yield return new ComplementInterest(ComplementInterestType.Beer);
                yield return new ComplementInterest(ComplementInterestType.Coffee);

            }
            else if (interestType == InterestType.Arts)
            {
                yield return new ComplementInterest(ComplementInterestType.Literature);
                yield return new ComplementInterest(ComplementInterestType.Painting);
                yield return new ComplementInterest(ComplementInterestType.PlasticArts_Sculptures);
                yield return new ComplementInterest(ComplementInterestType.MovieTheater);
                yield return new ComplementInterest(ComplementInterestType.Theater);
                yield return new ComplementInterest(ComplementInterestType.Television);

            }
            else if (interestType == InterestType.ElectronicGames)
            {
                yield return new ComplementInterest(ComplementInterestType.CounterStriker);
                yield return new ComplementInterest(ComplementInterestType.LeagueOfLegends);
                yield return new ComplementInterest(ComplementInterestType.DOTA);
                yield return new ComplementInterest(ComplementInterestType.Fortnite);

            }
            else if (interestType == InterestType.Language)
            {
                yield return new ComplementInterest(ComplementInterestType.Abkhaz);
                yield return new ComplementInterest(ComplementInterestType.Afar);
                yield return new ComplementInterest(ComplementInterestType.Afrikaans);
                yield return new ComplementInterest(ComplementInterestType.Akan);
                yield return new ComplementInterest(ComplementInterestType.Albanian);
                yield return new ComplementInterest(ComplementInterestType.Amharic);
                yield return new ComplementInterest(ComplementInterestType.Arabic);
                yield return new ComplementInterest(ComplementInterestType.Aragonese);
                yield return new ComplementInterest(ComplementInterestType.Armenian);
                yield return new ComplementInterest(ComplementInterestType.Assamese);
                yield return new ComplementInterest(ComplementInterestType.Avaric);
                yield return new ComplementInterest(ComplementInterestType.Avestan);
                yield return new ComplementInterest(ComplementInterestType.Aymara);
                yield return new ComplementInterest(ComplementInterestType.Azerbaijani);
                yield return new ComplementInterest(ComplementInterestType.Bambara);
                yield return new ComplementInterest(ComplementInterestType.Bashkir);
                yield return new ComplementInterest(ComplementInterestType.Basque);
                yield return new ComplementInterest(ComplementInterestType.Belarusian);
                yield return new ComplementInterest(ComplementInterestType.Bengali);
                yield return new ComplementInterest(ComplementInterestType.Bihari);
                yield return new ComplementInterest(ComplementInterestType.Bislama);
                yield return new ComplementInterest(ComplementInterestType.Bosnian);
                yield return new ComplementInterest(ComplementInterestType.Breton);
                yield return new ComplementInterest(ComplementInterestType.Bulgarian);
                yield return new ComplementInterest(ComplementInterestType.Burmese);
                yield return new ComplementInterest(ComplementInterestType.Catalan);
                yield return new ComplementInterest(ComplementInterestType.Chamorro);
                yield return new ComplementInterest(ComplementInterestType.Chechen);
                yield return new ComplementInterest(ComplementInterestType.Chichewa);
                yield return new ComplementInterest(ComplementInterestType.Chinese);
                yield return new ComplementInterest(ComplementInterestType.Chuvash);
                yield return new ComplementInterest(ComplementInterestType.Cornish);
                yield return new ComplementInterest(ComplementInterestType.Corsican);
                yield return new ComplementInterest(ComplementInterestType.Cree);
                yield return new ComplementInterest(ComplementInterestType.Croatian);
                yield return new ComplementInterest(ComplementInterestType.Czech);
                yield return new ComplementInterest(ComplementInterestType.Danish);
                yield return new ComplementInterest(ComplementInterestType.Divehi);
                yield return new ComplementInterest(ComplementInterestType.Dutch);
                yield return new ComplementInterest(ComplementInterestType.English);
                yield return new ComplementInterest(ComplementInterestType.Esperanto);
                yield return new ComplementInterest(ComplementInterestType.Estonian);
                yield return new ComplementInterest(ComplementInterestType.Ewe);
                yield return new ComplementInterest(ComplementInterestType.Faroese);
                yield return new ComplementInterest(ComplementInterestType.Fijian);
                yield return new ComplementInterest(ComplementInterestType.Finnish);
                yield return new ComplementInterest(ComplementInterestType.French);
                yield return new ComplementInterest(ComplementInterestType.Fula);
                yield return new ComplementInterest(ComplementInterestType.Galician);
                yield return new ComplementInterest(ComplementInterestType.Georgian);
                yield return new ComplementInterest(ComplementInterestType.German);
                yield return new ComplementInterest(ComplementInterestType.Greek);
                yield return new ComplementInterest(ComplementInterestType.Guaraní);
                yield return new ComplementInterest(ComplementInterestType.Gujarati);
                yield return new ComplementInterest(ComplementInterestType.Haitian);
                yield return new ComplementInterest(ComplementInterestType.Hausa);
                yield return new ComplementInterest(ComplementInterestType.Hebrew);
                yield return new ComplementInterest(ComplementInterestType.Herero);
                yield return new ComplementInterest(ComplementInterestType.Hindi);
                yield return new ComplementInterest(ComplementInterestType.Hiri);
                yield return new ComplementInterest(ComplementInterestType.Hungarian);
                yield return new ComplementInterest(ComplementInterestType.Interlingua);
                yield return new ComplementInterest(ComplementInterestType.Indonesian);
                yield return new ComplementInterest(ComplementInterestType.Interlingue);
                yield return new ComplementInterest(ComplementInterestType.Irish);
                yield return new ComplementInterest(ComplementInterestType.Igbo);
                yield return new ComplementInterest(ComplementInterestType.Inupiaq);
                yield return new ComplementInterest(ComplementInterestType.Ido);
                yield return new ComplementInterest(ComplementInterestType.Icelandic);
                yield return new ComplementInterest(ComplementInterestType.Italian);
                yield return new ComplementInterest(ComplementInterestType.Inuktitut);
                yield return new ComplementInterest(ComplementInterestType.Japanese);
                yield return new ComplementInterest(ComplementInterestType.Javanese);
                yield return new ComplementInterest(ComplementInterestType.Kalaallisut);
                yield return new ComplementInterest(ComplementInterestType.Kannada);
                yield return new ComplementInterest(ComplementInterestType.Kanuri);
                yield return new ComplementInterest(ComplementInterestType.Kashmiri);
                yield return new ComplementInterest(ComplementInterestType.Kazakh);
                yield return new ComplementInterest(ComplementInterestType.Khmer);
                yield return new ComplementInterest(ComplementInterestType.Kikuyu);
                yield return new ComplementInterest(ComplementInterestType.Kinyarwanda);
                yield return new ComplementInterest(ComplementInterestType.Kirghiz);
                yield return new ComplementInterest(ComplementInterestType.Komi);
                yield return new ComplementInterest(ComplementInterestType.Kongo);
                yield return new ComplementInterest(ComplementInterestType.Korean);
                yield return new ComplementInterest(ComplementInterestType.Kurdish);
                yield return new ComplementInterest(ComplementInterestType.Kwanyama);
                yield return new ComplementInterest(ComplementInterestType.Latin);
                yield return new ComplementInterest(ComplementInterestType.Luxembourgish);
                yield return new ComplementInterest(ComplementInterestType.Luganda);
                yield return new ComplementInterest(ComplementInterestType.Limburgish);
                yield return new ComplementInterest(ComplementInterestType.Lingala);
                yield return new ComplementInterest(ComplementInterestType.Lao);
                yield return new ComplementInterest(ComplementInterestType.Lithuanian);
                yield return new ComplementInterest(ComplementInterestType.Luba);
                yield return new ComplementInterest(ComplementInterestType.Latvian);
                yield return new ComplementInterest(ComplementInterestType.Manx);
                yield return new ComplementInterest(ComplementInterestType.Macedonian);
                yield return new ComplementInterest(ComplementInterestType.Malagasy);
                yield return new ComplementInterest(ComplementInterestType.Malay);
                yield return new ComplementInterest(ComplementInterestType.Malayalam);
                yield return new ComplementInterest(ComplementInterestType.Maltese);
                yield return new ComplementInterest(ComplementInterestType.Māori);
                yield return new ComplementInterest(ComplementInterestType.Marathi);
                yield return new ComplementInterest(ComplementInterestType.Marshallese);
                yield return new ComplementInterest(ComplementInterestType.Mongolian);
                yield return new ComplementInterest(ComplementInterestType.Nauru);
                yield return new ComplementInterest(ComplementInterestType.Navajo);
                yield return new ComplementInterest(ComplementInterestType.Norwegian);
                yield return new ComplementInterest(ComplementInterestType.North);
                yield return new ComplementInterest(ComplementInterestType.Nepali);
                yield return new ComplementInterest(ComplementInterestType.Ndonga);
                yield return new ComplementInterest(ComplementInterestType.Nuosu);
                yield return new ComplementInterest(ComplementInterestType.South);
                yield return new ComplementInterest(ComplementInterestType.Occitan);
                yield return new ComplementInterest(ComplementInterestType.Ojibwe);
                yield return new ComplementInterest(ComplementInterestType.Old);
                yield return new ComplementInterest(ComplementInterestType.Oromo);
                yield return new ComplementInterest(ComplementInterestType.Oriya);
                yield return new ComplementInterest(ComplementInterestType.Ossetian);
                yield return new ComplementInterest(ComplementInterestType.Panjabi);
                yield return new ComplementInterest(ComplementInterestType.Pāli);
                yield return new ComplementInterest(ComplementInterestType.Persian);
                yield return new ComplementInterest(ComplementInterestType.Polish);
                yield return new ComplementInterest(ComplementInterestType.Pashto);
                yield return new ComplementInterest(ComplementInterestType.Portuguese);
                yield return new ComplementInterest(ComplementInterestType.Quechua);
                yield return new ComplementInterest(ComplementInterestType.Romansh);
                yield return new ComplementInterest(ComplementInterestType.Kirundi);
                yield return new ComplementInterest(ComplementInterestType.Romanian);
                yield return new ComplementInterest(ComplementInterestType.Russian);
                yield return new ComplementInterest(ComplementInterestType.Sanskrit);
                yield return new ComplementInterest(ComplementInterestType.Sardinian);
                yield return new ComplementInterest(ComplementInterestType.Sindhi);
                yield return new ComplementInterest(ComplementInterestType.Northern);
                yield return new ComplementInterest(ComplementInterestType.Samoan);
                yield return new ComplementInterest(ComplementInterestType.Sango);
                yield return new ComplementInterest(ComplementInterestType.Serbian);
                yield return new ComplementInterest(ComplementInterestType.Scottish);
                yield return new ComplementInterest(ComplementInterestType.Shona);
                yield return new ComplementInterest(ComplementInterestType.Sinhala);
                yield return new ComplementInterest(ComplementInterestType.Slovak);
                yield return new ComplementInterest(ComplementInterestType.Slovene);
                yield return new ComplementInterest(ComplementInterestType.Somali);
                yield return new ComplementInterest(ComplementInterestType.Southern);
                yield return new ComplementInterest(ComplementInterestType.Spanish);
                yield return new ComplementInterest(ComplementInterestType.Sundanese);
                yield return new ComplementInterest(ComplementInterestType.Swahili);
                yield return new ComplementInterest(ComplementInterestType.Swati);
                yield return new ComplementInterest(ComplementInterestType.Swedish);
                yield return new ComplementInterest(ComplementInterestType.Tamil);
                yield return new ComplementInterest(ComplementInterestType.Telugu);
                yield return new ComplementInterest(ComplementInterestType.Tajik);
                yield return new ComplementInterest(ComplementInterestType.Thai);
                yield return new ComplementInterest(ComplementInterestType.Tigrinya);
                yield return new ComplementInterest(ComplementInterestType.Tibetan);
                yield return new ComplementInterest(ComplementInterestType.Turkmen);
                yield return new ComplementInterest(ComplementInterestType.Tagalog);
                yield return new ComplementInterest(ComplementInterestType.Tswana);
                yield return new ComplementInterest(ComplementInterestType.Tonga);
                yield return new ComplementInterest(ComplementInterestType.Turkish);
                yield return new ComplementInterest(ComplementInterestType.Tsonga);
                yield return new ComplementInterest(ComplementInterestType.Tatar);
                yield return new ComplementInterest(ComplementInterestType.Twi);
                yield return new ComplementInterest(ComplementInterestType.Tahitian);
                yield return new ComplementInterest(ComplementInterestType.Uighur);
                yield return new ComplementInterest(ComplementInterestType.Ukrainian);
                yield return new ComplementInterest(ComplementInterestType.Urdu);
                yield return new ComplementInterest(ComplementInterestType.Uzbek);
                yield return new ComplementInterest(ComplementInterestType.Venda);
                yield return new ComplementInterest(ComplementInterestType.Vietnamese);
                yield return new ComplementInterest(ComplementInterestType.Volapük);
                yield return new ComplementInterest(ComplementInterestType.Walloon);
                yield return new ComplementInterest(ComplementInterestType.Welsh);
                yield return new ComplementInterest(ComplementInterestType.Wolof);
                yield return new ComplementInterest(ComplementInterestType.Western);
                yield return new ComplementInterest(ComplementInterestType.Xhosa);
                yield return new ComplementInterest(ComplementInterestType.Yiddish);
                yield return new ComplementInterest(ComplementInterestType.Yoruba);
                yield return new ComplementInterest(ComplementInterestType.Zhuang);
            }
        }
    }
}