using System;
using System.Threading;
using System.Threading.Tasks;
using WebApi.Domain.Repositories;
using WebApi.Shared.Email;
using MediatR;
using WebApi.Domain.Services;

namespace WebApi.Application.CommandSide.Commands
{
    public class RequestNewPasswordCommand: IRequest
    {
        public string Email { get; set; }
    }

    public class RequestNewPasswordCommandHandler : IRequestHandler<RequestNewPasswordCommand>
    {
        private readonly IUserRepository _userRepository;
        private readonly IEmailService _sendEmailService;

        public RequestNewPasswordCommandHandler(IUserRepository userRepository, IEmailService sendEmailService)
        {
            _userRepository = userRepository;
            _sendEmailService = sendEmailService;
        }

        public async Task<Unit> Handle(RequestNewPasswordCommand request, CancellationToken cancellationToken)
        {
            var user = await _userRepository.FindByFilter(u => u.Authentication.Email == request.Email);
            if(user == null)
                throw new Exception();
                
            user.Authentication.RequestNewPassword();
            await _sendEmailService.SendNewTemporaryPassword(user.Authentication.Email, user.Authentication.Email, user.Authentication.TemporaryPassword);
            await _userRepository.Update(user);
            return Unit.Value;
        }
    }
}