using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using WebApi.Domain.Exceptions;
using Newtonsoft.Json;

namespace HostAndGo.Web.Middlewares
{
    public class ExceptionHttpCodes
    {
        private Dictionary<Type, HttpStatusCode> _exceptionCodes = new Dictionary<Type, HttpStatusCode>()
        {
            { typeof(DomainException), HttpStatusCode.BadRequest },
            { typeof(NotFoundException),     HttpStatusCode.NotFound },
        };

        public HttpStatusCode GetByException(Exception exception)
        {
            var query = _exceptionCodes
                .Where(e => e.Key.Equals(exception.GetType()))
                .Select(e => e.Value);

            return !query.Any() ? HttpStatusCode.InternalServerError : query.First();
        }
    }

    public abstract class ExceptionToHttpResultConverter
    {
        public HttpStatusCode Code { get; private set; }
        protected ExceptionHttpCodes ExceptionHttpCodes { get ; set; } = new ExceptionHttpCodes();

        protected abstract string GetResult(Exception exception);

        public string Convert(Exception exception)
        {
            Code = SetResultCode(exception);
            return GetResult(exception);
        }

        protected HttpStatusCode SetResultCode(Exception exception)
        {
            return ExceptionHttpCodes.GetByException(exception);
        }
    }

    public class ProductionResultConverter : ExceptionToHttpResultConverter
    {
        protected override string GetResult(Exception exception)
        {

            var result = JsonConvert.SerializeObject(new {
                error = Code == HttpStatusCode.InternalServerError ? "anErrorOccurred" : exception.Message
            });

            return result;
        }
    }

    public class DevelopmentResultConverter : ExceptionToHttpResultConverter
    {
        protected override string GetResult(Exception exception)
        {
            var result = JsonConvert.SerializeObject(new {
                error = Code == HttpStatusCode.InternalServerError ? "anErrorOccurred" : exception.Message,
                stackTrace = exception.StackTrace,
                exceptionMessage = exception.Message
            });

            return result;
        }
    }

    internal static class ResultConverterFactory
    {
        public static ExceptionToHttpResultConverter Create()
        {
            return new DevelopmentResultConverter();
            // switch(Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Production")
            // {
            //     case true: 
            //         return new ProductionResultConverter();
            //     default:
            // }
        }
    }
}