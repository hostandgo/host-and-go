using System;

namespace WebApi.Web.Utils
{
    public class RequestContext
    {
        public RequestContext(string token, Guid userId)
        {
            this.Token = token;
            this.UserId = userId;

        }
        public string Token { get; }
        public Guid UserId { get; }
    }
}