using System;
using System.Threading.Tasks;
using WebApi.Application.CommandSide.Commands;
using WebApi.Application.ViewModels;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApi.Domain.Services;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace HostAndGo.Web.Controllers
{
    [Route("api/authentication")]
    [AllowAnonymous]
    [ApiController]
    public class AuthenticationController: ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IFileService _fileService;

        public AuthenticationController(IMediator mediator, IFileService fileService)
        {
            _mediator = mediator;
            _fileService = fileService;
        }


        [HttpPost("login")]
        public async Task<ActionResult<UserAuthenticateViewModel>> Authenticate([FromBody]AuthenticateCommand command)
        {
            var result = await _mediator.Send(command);
            if(result == null)
                return BadRequest(new { Message = "User or password invalid"});

            return Ok(result);
        }
        
        [HttpGet("ping")]
        public ActionResult<string> Ping()
        {
            return Ok("pong");
        }

        [HttpPost("register")]
        public async Task<ActionResult<UserAuthenticateViewModel>> Register([FromBody]RegisterCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }

        [HttpPost("request-new-password")]
        public async Task<ActionResult> RequestNewPassword([FromBody]RequestNewPasswordCommand command)
        {
            await _mediator.Send(command);
            return Ok();
        }

        [HttpPost("update-password")]
        public async Task<ActionResult<UserAuthenticateViewModel>> UpdatePassword([FromBody] UpdatePasswordCommand command)
        {
            var result = await _mediator.Send(command);
            if(result == null)
                return BadRequest();

            return Ok(result);
        }
    }
}