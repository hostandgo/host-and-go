import React, { useState, useEffect } from 'react';
import { TextField, Fab, MenuItem, Chip } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import useWindowWidth from '../../hooks/useWindowWidth';
import dataInterests from './interests';
import useStyles from './useStyles';
import api from '../../api'

export default function InterestsCombos({
    interests = [],
    setInterests = [],
    disabled = false,
}) {
    const [baseInterests, setBaseInterests] = useState(dataInterests);
    const [currentInterest, setCurrentInterest] = useState([]);
    const [currentComplementInterest, setCurrentComplementInterest] = useState([]);
    const classes = useStyles();
    const windowWidth = useWindowWidth();
    const isMobile = windowWidth <= 900;

    useEffect(() => {
        async function getBaseInterests() {
            var result = await api.data.getInterests();
            if (!result.hasError)
                setBaseInterests(result.response);
        }
        getBaseInterests();
    }, [])

    function addInterest() {
        if (currentInterest && currentComplementInterest) {
            const interest = interests.find(item => item.interest.type === currentInterest.type);
            if (interest) {
                const hasComplement = interest.complementInterest.some(ci => ci.type === currentComplementInterest.type)
                if (!hasComplement) {
                    setInterests(
                        interests.map(item => item.interest.type === currentInterest.type ?
                            {
                                interest: item.interest,
                                complementInterest: item.complementInterest.concat([currentComplementInterest])
                            }
                            : item
                        )
                    )
                }
            } else {
                setInterests([...interests,
                {
                    interest: currentInterest,
                    complementInterest: [currentComplementInterest]
                }])
            }
        }
        setCurrentInterest('');
        setCurrentComplementInterest('');
    }


    function removeInterest(complementInterestToRemove) {
        setInterests(interests.map(item => ({
            interest: item.interest,
            complementInterest: item.complementInterest
                .filter((complementInterest) => complementInterest.type !== complementInterestToRemove.type)
        })));
    }
    console.log(interests)
    return (
        <div className={isMobile ? classes.mobileItem : classes.interestWrapper}>
            <div className={isMobile ? classes.mobileItem : classes.interestSection}>
                <TextField
                    id="outlined-interest"
                    select
                    label="Interest"
                    className={isMobile ? classes.textFieldMobile : classes.textFieldDesktop}
                    value={currentInterest}
                    onChange={e => setCurrentInterest(e.target.value)}
                    SelectProps={{
                        MenuProps: {
                            className: classes.menu,
                        },
                    }}
                    margin="normal"
                    variant="outlined"
                    disabled={disabled}
                >
                    {baseInterests.map(option => (
                        <MenuItem key={option.type} value={option}>
                            {option.description}
                        </MenuItem>
                    ))}
                </TextField>
                <TextField
                    id="outlined-select-complement-interest"
                    select
                    label="Complement interest"
                    className={isMobile ? classes.textFieldMobile : classes.textFieldDesktop}
                    value={currentComplementInterest}
                    onChange={e => setCurrentComplementInterest(e.target.value)}
                    SelectProps={{
                        MenuProps: {
                            className: classes.menu,
                        },
                    }}
                    margin="normal"
                    variant="outlined"
                    disabled={disabled}
                >
                    {
                        currentInterest.complements &&
                        currentInterest.complements.map(option => (
                            <MenuItem key={option.type} value={option}>
                                {option.description}
                            </MenuItem>
                        ))}
                </TextField>
                <Fab onClick={addInterest} color="primary" aria-label="add" className={classes.fabLanguage}>
                    <AddIcon />
                </Fab>
            </div>
            <div>
                {
                    interests.map((item) =>
                        item.complementInterest.map((complementInterest) =>
                            <Chip
                                label={`${item.interest.description} - ${complementInterest.description}`}
                                onDelete={() => removeInterest(complementInterest)}
                                className={classes.chip}
                                color="primary"
                            />
                        )

                    )
                }
            </div>
        </div>
    )
}