
import { format } from 'date-fns'

export function newDate() {
    return defaultDateFormat(new Date());
}

export function defaultDateFormat(date){
    return format(date, 'MM/dd/yyyy');
}