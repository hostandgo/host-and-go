using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WebApi.Domain.Models;
using WebApi.Domain.Repositories;
using WebApi.Shared.Utils;
using MongoDB.Driver;

namespace WebApi.Infrastructure.Repositories
{
    public class UserRepository: IUserRepository
    {
        private readonly MongoContext _context;

        public UserRepository(MongoContext context)
        {
            _context = context;
        }

        public async Task<User> FindByFilter(Expression<Func<User, bool>> filter)
        {
            var result = await _context.Users.FindAsync(filter);
            return await result.FirstOrDefaultAsync();
        }

        public async Task<User> FindByLogin(string email, string password)
        {
            var result = await _context.Users.FindAsync(u => u.Authentication.Email == email);
            var user = await result.FirstOrDefaultAsync();
            if(user == null)
                return null;
            
            if(user.Authentication.ForgotPassword && user.Authentication.TemporaryPassword == Cipher.OpenSSLDecrypt(password))
                return user;
                
            if(Cipher.OpenSSLDecrypt(user.Authentication.Password) == Cipher.OpenSSLDecrypt(password))
                return user;
                
            else return null;  
        }

        public async Task<User> FindById(Guid id)
        {
            var result = await _context.Users.FindAsync(i => i.Id == id);
            return await result.FirstOrDefaultAsync();
        }

        public async Task Insert(User user)
        {
            try
            {
                await _context.Users.InsertOneAsync(user);
            }
            catch (System.Exception ex)
            {
                throw new Exception(ex.InnerException.Message); 
            }

        }

        public async Task<IEnumerable<User>> ListByFilter(Expression<Func<User, bool>> filter)
        {
            var result = await _context.Users.FindAsync(filter);
            return result.Current;
        }

        public async Task Remove(User user)
        => await _context
            .Users
            .DeleteOneAsync(i => i.Id == user.Id);

        public async Task<User> Update(User user)
        {
            await _context.Users.ReplaceOneAsync(i => i.Id == user.Id, user);
            return user;
        }
    }
}