using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebApi.Application.ViewModels;
using WebApi.Domain.Autentication;
using WebApi.Domain.Repositories;
using MediatR;
using Microsoft.IdentityModel.Tokens;

namespace WebApi.Application.CommandSide.Commands
{
    public class AuthenticateCommand: IRequest<UserAuthenticateViewModel>
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
    public class AuthenticateCommandHandler: IRequestHandler<AuthenticateCommand, UserAuthenticateViewModel>
    {
        private readonly IUserRepository _userRepository;

        public AuthenticateCommandHandler(IUserRepository userRepository)
        {
            this._userRepository = userRepository;
        }
        //https://balta.io/blog/aspnet-core-autenticacao-autorizacao 
        public async Task<UserAuthenticateViewModel> Handle(AuthenticateCommand request, CancellationToken cancellationToken)
        {
            var user = await _userRepository.FindByLogin(request.Email, request.Password);
            if (user == null)
                return null;

            var token = JWT.GenerateToken(user.Id, user.Authentication.Email, user.Authentication.Role);
            user.Authentication.SaveToken(token);
            await _userRepository.Update(user);
            return new UserAuthenticateViewModel(user.Id, token, user.Authentication.ForgotPassword);
        }
    }
}