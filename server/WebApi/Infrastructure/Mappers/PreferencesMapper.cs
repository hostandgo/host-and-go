using WebApi.Domain.Models;
using MongoDB.Bson.Serialization;

namespace WebApi.Infrastructure.Mappers
{
    public static class PreferencesMapper
    {
        public static void Configure()
        {
            BsonClassMap.RegisterClassMap<Preferences>(map => 
            {
                map.AutoMap();
                map.MapField(p => p.UserId);
                map.MapCreator(p => new Preferences(p.UserId, p.Description));
                map.MapField("_interests").SetElementName("Interests");
                map.MapField("_restrictions").SetElementName("Restrictions");
                // map.MapField(p => p.Interests);
                // map.MapField(p => p.Restrictions);
            });
        }
    }
}