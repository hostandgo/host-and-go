import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(theme => ({
    inputs: {
        display: 'flex',
        width: 850,
        alignItems: 'center'
    },
    textFieldMobile: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: '90%'
    },
    textFieldDesktop: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: '25%'
    },
    button: {
        margin: theme.spacing(1),
    },
    extendedIcon: {
        marginRight: theme.spacing(1),
    },
    margin: {
        margin: theme.spacing(1),
    },
    listWrapper: {
        width: '65%'
    }
}));