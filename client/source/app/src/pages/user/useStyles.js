import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(theme => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        margin: 5,
        width: '100%'
    },
    fab: {
        margin: theme.spacing(1),
    },
    paperMobile: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        paddingBottom: 20,
        marginTop: 20
    },
    paperDesktop: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        paddingBottom: 20,
        marginTop: 20
    },
    paperDesktopTwo: {
        alignItems: 'flex-start',
        padding: 10,
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        paddingBottom: 20,
        marginTop: 20
    },
    textFieldDescribe: {
        // marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: '95%'
    },
    textFieldCustom: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: '55%'
    },
    comboCustom: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: '18%'
    },
    textFieldMobile: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: '90%'
    },
    textFieldDesktop: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: '45%'
    },
    textFieldHours: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: '10%'
    },
    firstRow: {
        flexDirection: 'row',
        alignItems: 'center',
        display: 'flex',
        width: '100%',
        marginLeft: 20,
        marginRight: 20
    },
    secondRow: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        marginTop: 20,
        justifyContent: 'space-between',
    },
    addresDescription: {
        width: '100%'
    },
    restrictions: {
        width: '30%',
        alignItems: 'flex-start',
        display: 'flex',
        justifyContent: 'center',
    },
    mobileItem: {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
        width: '100%'
    },
    datesSection: {
        width: '55%',
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'center'
    },
    combosSection: {
        width: '45%'
    },
    dataPickers: {
        display: 'flex',
        alignItems: 'center'
    },
    fabProgress: {
        color: theme.palette.primary,
        position: 'absolute',
        top: -6,
        left: -6,
        zIndex: 1,
        margin: 8
    },
    hourInputLabel: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'baseline'
    },
    wrapperSaveButton: {
        margin: theme.spacing(1),
        position: 'relative',
    }
}));