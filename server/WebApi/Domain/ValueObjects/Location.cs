namespace WebApi.Domain.ValueObjects
{
    public class Location
    {
        public Location(
            LocationItem continent,
            LocationItem country,
            LocationItem state,
            LocationItem region
        )
        {
            Continent = continent;
            Country = country;
            State = state;
            Region = region;
        }

        public LocationItem Continent { get; }
        public LocationItem Country { get; }
        public LocationItem State { get; }
        public LocationItem Region { get; }
    }

    public class LocationItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}