namespace WebApi.Domain.Autentication
{
    public static class Roles
    {
        public const string Key = "HostAndGo"; 
        public const string Travel = "travel";
        public const string Host = "host";
    }
}