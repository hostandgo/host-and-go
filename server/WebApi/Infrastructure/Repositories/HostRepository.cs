using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WebApi.Domain.Models;
using WebApi.Domain.Repositories;
using MongoDB.Driver;

namespace WebApi.Infrastructure.Repositories
{
    public class HostRepository : IHostRepository
    {
        private readonly MongoContext _context;

        public HostRepository(MongoContext context)
        {
            _context = context;
        }

        public async Task<Host> FindByFilter(Expression<Func<Host, bool>> filter)
        {
            var result = await _context.Hosts.FindAsync(filter);
            return await result.FirstOrDefaultAsync();
        }

        public async Task<Host> FindByUserId(Guid userId)
        {
            var result = await _context.Hosts.FindAsync(p => p.UserId == userId);
            return await result.FirstOrDefaultAsync();
        }

        public async Task Insert(Host host)
        => await _context.Hosts.InsertOneAsync(host);

        public async Task<IEnumerable<Host>> ListByFilter(Expression<Func<Host, bool>> filter)
        {
            var result = await _context.Hosts.FindAsync(filter);
            return result.Current;
        }

        public async Task Remove(Host host)
        => await _context
            .Hosts
            .DeleteOneAsync(i => i.Id == host.Id);

        public async Task<Host> Update(Host host)
        {
            await _context.Hosts.ReplaceOneAsync(i => i.Id == host.Id, host);
            return host;
        }
    }
}