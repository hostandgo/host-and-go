import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import FolderIcon from '@material-ui/icons/Folder';
import IconButton from '@material-ui/core/IconButton';
import useStyles from './useStyles';
import InfoIcon from '@material-ui/icons/Info';


export default function HostItem({user, infoOnClick}) {
    
    const classes = useStyles();

    return(
        <ListItem>
            {/* <IconButton edge="end" aria-label="delete">
                <ClearIcon onClick={() => handleRemove(user.id)} />
            </IconButton> */}
            <ListItemAvatar>
                <Avatar 
                    className={classes.mediumAvatar}
                    src={user.profile.picture}
                >
                <FolderIcon />
                </Avatar>
            </ListItemAvatar>
            <ListItemText
                primary={user.profile.name}
                secondary={user.host.description}
            />
            <ListItemSecondaryAction>
                <IconButton onClick={infoOnClick} edge="end" aria-label="delete">
                    <InfoIcon color="primary" fontSize="large"/>
                </IconButton>
            </ListItemSecondaryAction>
        </ListItem>
    )
}