using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using WebApi.Shared.Utils;

public static class HashSetExtension
{

    public static void AddRange<T>(this HashSet<T> source, IEnumerable<T> items)
    {
        foreach (var item in items)
            source.Add(item);
    }   

    public static void AddRange<T>(this HashSet<T> source, HashSet<T> items)
    {
        foreach (var item in items)
            source.Add(item);
    }      
}
