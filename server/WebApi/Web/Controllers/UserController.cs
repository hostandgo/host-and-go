using System;
using System.Threading.Tasks;
using WebApi.Application.CommandSide.Commands;
using WebApi.Application.QuerySide.Queries;
using WebApi.Application.ViewModels;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApi.Web.Utils;

namespace HostAndGo.Web.Controllers
{
    [Route("api/user")]
    [ApiController]
    public class UserController: ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly RequestContext _requestContext;

        public UserController(IMediator mediator, RequestContext requestContext)
        {
            _mediator = mediator;
            _requestContext = requestContext;
        }

        [HttpGet("get-weeks")]
        public async Task<ActionResult<int>> GetWeeks()
        {
            var result = await _mediator.Send(new GetWeeksQuery(_requestContext.UserId));
            return Ok(result);
        }
    }
}