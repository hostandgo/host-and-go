namespace WebApi.Domain.Enums
{
    public enum BathroomAccess
    {
        Private = 1,
        Shared = 2
    }
}