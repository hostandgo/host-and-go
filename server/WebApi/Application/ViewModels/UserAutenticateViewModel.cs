using System;

namespace WebApi.Application.ViewModels
{
    public class UserAuthenticateViewModel
    {
        public UserAuthenticateViewModel(Guid id, string token, bool forgotPassword)
        {
            this.Id = id;
            this.Token = token;
            this.ForgotPassword = forgotPassword;
        }
        public Guid Id { get; set; }
        public string Token { get; set; }
        public bool ForgotPassword { get; set; }
    }
}