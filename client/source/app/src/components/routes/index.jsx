import React from 'react';
import { Router, Route, withRouter } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import history from '../../services/history';
import PrivateRoute from '../private-route';
import ThemeProvider from '../theme-provider';
import 'react-toastify/dist/ReactToastify.css';
import Onboarding from '../onboarding';

const LazyPreferences = React.lazy(() => import('../../pages/preferences'))
const LazyProfile = React.lazy(() => import('../../pages/profile'))
const LazyHost = React.lazy(() => import('../../pages/host'))
const LazyTravel = React.lazy(() => import('../../pages/travel'))
const LazyRegister = React.lazy(() => import('../../pages/register'))

const ThemeWithRouter = withRouter(props => <ThemeProvider showTopBar={props.location.pathname === '/login' || props.location.pathname === '/'} {...props} />)

export default function Routes(){
    return (
        <Router history={history}>
            <ThemeWithRouter>
                <React.Suspense fallback={<div>Loading...</div>}>
                    <Route exact path="/" component={LazyRegister}/>
                    <Route exact path="/login" component={LazyRegister}/>
                    <PrivateRoute exact path='/preferences' component={LazyPreferences} />
                    <PrivateRoute exact path='/profile' component={LazyProfile} />
                    <PrivateRoute exact path='/host' component={LazyHost} />
                    <PrivateRoute exact path='/travel' component={LazyTravel} />
                </React.Suspense>
                <ToastContainer autoClose={8000} />
            </ThemeWithRouter> 
        </Router>
    )
}