import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(theme => ({
    wrapperSaveButton: {
        margin: theme.spacing(1),
        position: 'relative',
    },
    fab: {
        margin: theme.spacing(1),
    },
    fabProgress: {
        color: theme.palette.primary,
        position: 'absolute',
        top: -6,
        left: -6,
        zIndex: 1,
        margin: 8
    },
    chip: {
        margin: theme.spacing(1),
    },
}));