import { makeStyles } from '@material-ui/core/styles';
const drawerWidth = '80%';

export default makeStyles(theme => ({
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        width: drawerWidth,
    },
    drawerHeader: {
        background: theme.palette.primary.main,
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
        justifyContent: 'flex-start',
    },
    wrapAvatar: {
        display: 'flex', 
        alignItems: 'center', 
        flexDirection: 'column'
    },
    bigAvatar: {
        margin: 10,
        width: 150,
        height: 150,
    },
    firstSection: {
        display: 'flex',
        alignItems: 'center'
    },
    secondSection: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    threeSection: {
        margin: theme.spacing(2)
    }

}))