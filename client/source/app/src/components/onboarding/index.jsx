import React, { useState, useEffect } from 'react';
import Joyride from 'react-joyride';
import api from '../../api';

export default function Onboarding({customSteps, run, id}) {
    const [steps, setSteps] = useState([]); 
    const [open, setOpen] = useState(false); 
    
    function handleReset(props) {
        if(props.action === 'reset') {
            api.onboarding.usage.useFlow({
                flowId: id
            });
            setSteps([]);
            setOpen(false);
        }
    }

    useEffect(() => {
        async function getPassedThroughTheFlow(){
            if(id === 0) return;

            const result = await api.onboarding.usage.passedThroughTheFlow({
                flowId: id
            });

            if(!result.hasError && !result.response){
                setSteps(customSteps);
                setOpen(run)
            }
        }

        getPassedThroughTheFlow();

        return () => {
            setSteps([])
            setOpen(false);
        }
    }, [customSteps, run, setOpen, id])

    return(
        <Joyride
            run={open}
            steps={steps}
            continuous
            styles={{
                options: {
                    primaryColor: "#4caf50"
                }
            }}
            callback={handleReset}
        />
    )
}