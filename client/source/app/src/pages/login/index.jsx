import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import { Button } from '@material-ui/core';
import CryptoJS from 'crypto-js';
import api from '../../api';
import {secret, login } from '../../services/auth';

export default function Login() {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    async function submit() {
        var encryptPassword = CryptoJS.AES.encrypt(password, secret).toString();
        const result = await api.authentication.login(email, encryptPassword);
        if(!result.hasError){
            const { token, userId } = result.response 
            login(token, userId);
        }
    }
    return (
        <form>
            <TextField
                id="email"
                label="Email"
                value={email}
                onChange={ e => setEmail(e.target.value)}
                margin="normal"
            />
            <TextField
                id="password"
                label="Password"
                value={password}
                onChange={ e => setPassword(e.target.value)}
                margin="normal"
            />
            <Button
                onClick={submit}
            >
                Login
            </Button>
        </form>
    )
}