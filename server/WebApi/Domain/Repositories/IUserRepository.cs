using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WebApi.Domain.Models;

namespace WebApi.Domain.Repositories
{
    public interface IUserRepository
    {
        Task<User> FindById(Guid id);
        Task<User> FindByFilter(Expression<Func<User, bool>> filter);
        Task<User> FindByLogin(string email, string password);
        Task<IEnumerable<User>> ListByFilter(Expression<Func<User, bool>> filter);
        Task Insert(User user);
        Task Remove(User user);
        Task<User> Update(User user);
    }
}