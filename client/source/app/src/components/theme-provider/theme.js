import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
    palette: {
      background: {
        default: '#eceff1'
      },
      primary: {
        main: '#4caf50',
      },
      secondary: {
        main: '#84ffff',
      },
    },  
    typography: { useNextVariants: true },
});

export default theme;