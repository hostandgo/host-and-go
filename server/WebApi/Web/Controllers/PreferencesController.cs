using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApi.Application.CommandSide.Commands;
using WebApi.Application.QuerySide.Queries;
using WebApi.Application.ViewModels;
using WebApi.Domain.Models;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApi.Web.Utils;

namespace HostAndGo.Web.Controllers
{
    [Route("api/preference")]
    [ApiController]
    public class PreferencesController: ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly RequestContext _requestContext;

        public PreferencesController(IMediator mediator, RequestContext requestContext)
        {
            _mediator = mediator;
            _requestContext = requestContext;
        }

        [HttpPut("update")]
        public async Task<ActionResult> UpdatePreferences([FromBody]UpdatePreferencesCommand command)
        {
            command.UserId = _requestContext.UserId;
            await _mediator.Send(command);
            return Ok();
        }

        [HttpGet]
        public async Task<ActionResult> Get()
        {
            var result = await _mediator.Send(new GetPreferencesQuery(_requestContext.UserId));
            return Ok(result);
        }
    }
}