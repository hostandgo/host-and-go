using System.Collections.Generic;
using System.Linq;
using WebApi.Domain.Enums;
using WebApi.Domain.Models;

namespace WebApi.Application.ViewModels
{
    public class GetHostDetailsViewModel
    {
        public GetHostDetailsViewModel(
            string addres,
            string description,
            TypeOfRoom? roomType,
            BathroomAccess? bathroomAccess,
            TimeAvailableToReceiveViewModel[] timesAvailableToReceive,
            int hoursInteractTravel,
            string describeWhatCanDoTogether,
            RestrictionViewModel[] restrictions,
            string describeWhatYourCity,
            IEnumerable<PreferredAgeRange> preferredAgeRange,
            IEnumerable<CityType> city,
            IEnumerable<AttractionType> attractions,
            IEnumerable<Interest> interests)
        {
            Addres = addres;
            Description = description;
            RoomType = roomType;
            BathroomAccess = bathroomAccess;
            PreferredAgeRange = preferredAgeRange.Select(p => new ItemWithDescription((int)p, p.GetDescription()));
            TimesAvailableToReceive = timesAvailableToReceive;
            HoursInteractTravel = hoursInteractTravel;
            DescribeWhatCanDoTogether = describeWhatCanDoTogether;
            Restrictions = restrictions;
            CityType = city.Select(c => new ItemWithDescription((int)c, c.GetDescription()));
            DescribeWhatYourCity = describeWhatYourCity;
            Attractions = attractions.Select(a => new AttractionViewModel(a));
            Interests = interests;
        }

        public string Addres { get; set; }
        public string Description { get; set; }
        public TypeOfRoom? RoomType { get; set; }
        public BathroomAccess? BathroomAccess { get; set; }
        public IEnumerable<ItemWithDescription> PreferredAgeRange { get; set; }
        public TimeAvailableToReceiveViewModel[] TimesAvailableToReceive { get; set; }
        public int HoursInteractTravel { get; set; }
        public string DescribeWhatCanDoTogether { get; set; }
        public RestrictionViewModel[] Restrictions { get; set; }
        public IEnumerable<ItemWithDescription> CityType { get; set; }
        public string DescribeWhatYourCity { get; set; }
        public IEnumerable<AttractionViewModel> Attractions { get; set; }
        public IEnumerable<Interest> Interests { get; set; }
    }

    public class AttractionViewModel
    {
        public AttractionViewModel(AttractionType value)
        {
            this.Value = value;
            this.Description = value.GetDescription();

        }
        public AttractionType Value { get; set; }
        public string Description { get; set; }
    }

    public class ItemWithDescription
    {
        public ItemWithDescription(int value, string description)
        {
            Value = value;
            Description = description;
        }

        public int Value { get; set; }
        public string Description { get; set; }
    }
}