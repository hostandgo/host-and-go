import React, { useState } from 'react';
import SmokingRoomsIcon from '@material-ui/icons/SmokingRooms';
import SmokingRoomsOutlinedIcon from '@material-ui/icons/SmokingRoomsOutlined';
import PetsIcon from '@material-ui/icons/Pets';
import PetsOutlinedIcon from '@material-ui/icons/PetsOutlined';
import FavoriteIcon from '@material-ui/icons/Favorite';
import FavoriteOutLinedIcon from '@material-ui/icons/FavoriteBorderOutlined';
import VolumeUpIcon from '@material-ui/icons/VolumeUp';
import CircularProgress from '@material-ui/core/CircularProgress';
import VolumeUpOutLinedIcon from '@material-ui/icons/VolumeUpOutlined';
import Fab from '@material-ui/core/Fab';
import EditIcon from '@material-ui/icons/Edit';
import SaveIcon from '@material-ui/icons/Save';
import CheckIcon from '@material-ui/icons/Check';
import ChildCareIcon from '@material-ui/icons/ChildCare';
import ChildCareOutLinedIcon from '@material-ui/icons/ChildCareOutlined';
import DefaultWrapper from '../../components/default-wrapper';
import FullWidthTabs from './components/full-width-tabs';
import ProfileTab from './components/profile-tab';
import PreferencesTab from './components/preferences-tab';
import useStyles from './useStyles';
import api from '../../api';
import Loader from '../../components/loader';

const restrictionTypes = [
    { label: 'Smoke', value: 1, checked: false, icon: <SmokingRoomsOutlinedIcon fontSize="large" />, checkedIcon: <SmokingRoomsIcon fontSize="large" /> },
    { label: 'Pets', value: 2, checked: false, icon: <PetsOutlinedIcon fontSize="large" />, checkedIcon: <PetsIcon fontSize="large" /> },
    { label: 'Religion', value: 3, checked: false, icon: <FavoriteOutLinedIcon fontSize="large" />, checkedIcon: <FavoriteIcon fontSize="large" /> },
    { label: 'Sound', value: 4, checked: false, icon: <VolumeUpOutLinedIcon fontSize="large" />, checkedIcon: <VolumeUpIcon fontSize="large" /> },
    { label: 'Children', value: 5, checked: false, icon: <ChildCareOutLinedIcon fontSize="large" />, checkedIcon: <ChildCareIcon fontSize="large" /> }
]

export default function User() {
    const classes = useStyles();

    const [tab, setTab] = useState(0);

    const [disabled, setDisabled] = useState(false);

    const [firstName, setFirstName] = useState('');
    const [secondName, setSecondName] = useState('');
    const [nationality, setNationality] = useState('');
    const [dateOfBirth, setDateOfBirth] = useState(null);
    const [email, setEmail] = useState('');
    const [genre, setGenre] = useState('');
    const [level, setLevel] = useState('');
    const [profission, setProfission] = useState('');
    const [nativeLanguage, setNativeLanguage] = useState('');
    const [languages, setLanguages] = useState([]);
    const [city, setCity] = useState('');
    const [country, setCountry] = useState('');
    const [document, setDocument] = useState([]);
    const [instagram, setInstagram] = useState('');
    const [twitter, setTwitter] = useState('');
    const [facebook, setFacebook] = useState('');
    const [linkedin, setLinkedin] = useState('');
    const [others, setOthers] = useState('');
    const [currentLanguage, setCurrentLanguage] = useState('');
    const [success, setSuccess] = useState(false);
    const [loading, setLoading] = useState(false);
    const [showSaveButton, setShowSaveButton] = useState(false);
    const [screenLoading, setScreenLoading] = useState(false);
    const [familyPhotos, setFamilyPhotos] = useState([]);


    const [currentInterest, setCurrentInterest] = useState('');
    const [currentComplementInterest, setCurrentComplementInterest] = useState('');
    const [restrictions, setRestrictions] = useState(restrictionTypes);
    const [interests, setInterests] = useState([]);
    const [description, setDescription] = useState('');

    const profileStates = {
        firstName, setFirstName,
        secondName, setSecondName,
        nationality, setNationality,
        dateOfBirth, setDateOfBirth,
        email, setEmail,
        genre, setGenre,
        level, setLevel,
        profission, setProfission,
        nativeLanguage, setNativeLanguage,
        languages, setLanguages,
        city, setCity,
        country, setCountry,
        document, setDocument,
        instagram, setInstagram,
        twitter, setTwitter,
        facebook, setFacebook,
        linkedin, setLinkedin,
        others, setOthers,
        currentLanguage, setCurrentLanguage,
        success, setSuccess,
        loading, setLoading,
        showSaveButton, setShowSaveButton,
        screenLoading, setScreenLoading,
        familyPhotos, setFamilyPhotos,
        disabled
    }

    const preferencesState = {
        currentInterest, setCurrentInterest,
        currentComplementInterest, setCurrentComplementInterest,
        restrictions, setRestrictions,
        interests, setInterests,
        description, setDescription,
        loading, setLoading,
        screenLoading, setScreenLoading,
        disabled
    }

    function edit() {
        setShowSaveButton(!showSaveButton);
        setDisabled(!disabled)
    }

    async function savePreferences() {
        setDisabled(true);
        if (!loading) {
            setSuccess(false);
            await api.preferences.update(
                {
                    description: description,
                    interests: interests.map(i => ({
                        interest: i.interest.type,
                        complementInterest: i.complementInterest.type
                    })),
                    restrictions: restrictions.filter(r => r.checked).map(r => r.value)
                },
                setLoading
            )
            setSuccess(true);
            setTimeout(() => {
                setSuccess(false);
                setShowSaveButton(false)
            }, 2000);
        }
    }

    async function saveProfile() {
        setDisabled(true);
        if (!loading) {
            setSuccess(false);
            setLoading(true);
            let formData = new FormData();
            document.forEach((file) => {
                formData.append('files', file);
            }
            )

            await api.profile.updateDocument(
                formData,
                () => { }
            )

            formData = new FormData();
            familyPhotos.forEach((file) => {
                formData.append('files', file);
            }
            )
            await api.profile.updateFamilyPhotos(
                formData, () => { }
            )
            await api.profile.update(
                {
                    firstName, secondName, nationality, dateOfBirth, email, genre, level, profission
                    , nativeLanguage, languages, city, country, instagram, twitter
                    , facebook, linkedin, others
                },
                setLoading
            )
            setSuccess(true);
            setLoading(false);
            setTimeout(() => {
                setSuccess(false);
                setShowSaveButton(false)
            }, 2000);
        }
    }

    async function handleSaveApi() {
        if (tab === 0) {
            await saveProfile();
        } else {
            await savePreferences();
        }
    }

    return (
        <DefaultWrapper>
            <FullWidthTabs
                tabsTitleOne={'Profile'}
                tabsTitleTwo={'Preferences'}
                tabOne={<ProfileTab {...profileStates} />}
                tabTwo={<PreferencesTab {...preferencesState} />}
                setTab={setTab}
                setDisabled={() => { }}
            />
            <div
                style={{
                    position: 'fixed',
                    top: 150,
                    right: 10,
                }}
            >
                <div className="save-button">
                    <Fab onClick={async () => await handleSaveApi()} color="primary" aria-label="add" className={classes.fab}>
                        {success ? <CheckIcon /> : <SaveIcon />}
                    </Fab>
                    {loading && <CircularProgress size={68} className={classes.fabProgress} />}
                </div>
                {/* <Fab id="edit-button" onClick={edit} color="secondary" aria-label="add" className={classes.fab}>
                    <EditIcon />
                </Fab> */}
            </div>
            {
                screenLoading &&
                <Loader
                    style={{
                        top: '45%',
                        left: '50%',
                        position: 'absolute'
                    }}
                    size={80}
                />
            }
        </DefaultWrapper>
    )
}