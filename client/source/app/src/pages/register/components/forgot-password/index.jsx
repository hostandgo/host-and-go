import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import ButtonWithLoading from '../../../../components/button-with-loading';
import api from '../../../../api';

export default function ForgotPassword(props) {
    const { classes } = props; 
    const [emailForgotPassWord, setEmailForgotPassWord] = useState('');
    const [loading, setLoading] = useState(false);
    
    async function requestNewPassword(){
        await api.authentication.requestNewPassword({ email: emailForgotPassWord }, setLoading);
        props.backToInitialPage();
    }
    return (
        <div className={classes.forgotPassword}>
            <div className={classes.forgotPasswordText}>
                <Typography component="h3">
                    Do not worry, we will solve it :).
                </Typography>
                <Typography component="h3">
                    Just click the Send button and we will send you an email with your new password.
                </Typography>
            </div>
            <div style={{ width: '100%', marginTop: 40 }}>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <TextField
                            variant="outlined"
                            required
                            fullWidth
                            value={emailForgotPassWord}
                            onChange={e => setEmailForgotPassWord(e.target.value)}
                            id="email"
                            label="Email Address"
                            name="email"
                            autoComplete="email"
                        />
                    </Grid>
                </Grid>
                <ButtonWithLoading
                    styles={{marginTop: 10, width: '100%'}}
                    className={classes.submit}
                    loading={loading}
                    handleButtonClick={async () => await requestNewPassword()}
                    label={'SEND'}        
                />
                <Button
                    style={{ marginTop: 10, width: '100%' }}
                    variant="contained"
                    onClick={props.backToInitialPage}
                >
                    BACK
                </Button>
            </div>
        </div>
    )
}