using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebApi.Application.ViewModels;
using WebApi.Domain.Exceptions;
using WebApi.Domain.ValueObjects;
using WebApi.Infrastructure;
using MediatR;
using MongoDB.Driver;

namespace WebApi.Application.QuerySide.Queries
{
    public class GetProfileQuery : IRequest<ProfileViewModel>
    {
        public GetProfileQuery(Guid userId)
        {
            UserId = userId;
        }

        public Guid UserId { get; set; }
    }

    public class GetProfileQueryHandler : IRequestHandler<GetProfileQuery, ProfileViewModel>
    {
        private readonly MongoContext _context;

        public GetProfileQueryHandler(MongoContext context)
        {
            _context = context;
        }

        public async Task<ProfileViewModel> Handle(GetProfileQuery request, CancellationToken cancellationToken)
        {   
            var userResult = await _context.Users.FindAsync(u => u.Id == request.UserId);
            var user = await userResult.FirstOrDefaultAsync();

            if(user == null) throw new NotFoundException("User not found");
            
            var profileResult = await _context.Profiles.FindAsync(p => p.UserId == request.UserId);
            var profile = await profileResult.FirstOrDefaultAsync();

            return new ProfileViewModel(
                user.Stars, 
                profile != null ? profile.Name.FirstName : "", 
                profile != null ? profile.Name.SecondName : "", 
                user.Authentication.Email, 
                user.Status.GetDescription(), 
                profile?.Genre, 
                profile != null ? profile.NativeLanguage : "", 
                profile != null ? profile.Document : "", 
                profile != null ? profile.Nationality : "", 
                profile?.DateOfBirth,
                profile?.Location,
                profile != null ? profile.SocialMedia.Instagram : "",
                profile != null ? profile.SocialMedia.Facebook : "",
                profile != null ? profile.SocialMedia.Twitter : "",
                profile != null ? profile.SocialMedia.Linkedin : "",
                profile != null ? profile.Religion: null,
                profile?.Deficiency,
                profile != null ? profile.Languages.ToArray() : new Language[]{},
                profile != null ? profile.FamilyPhotos.ToArray() : new string[]{},
                profile != null ? profile.Picture : ""
            );
        }
    }
}