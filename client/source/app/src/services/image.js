import { bucketBaseUrl } from '../config/url';

export const formatImageToView = (imageName) => Object.assign(new File(["duvido tanto em homenagem ao JUJU"], imageName, { type: "image/jpeg"}), {
    preview : `${bucketBaseUrl}/${imageName}`,
});