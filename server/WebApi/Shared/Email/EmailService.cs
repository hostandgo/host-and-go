using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;
using WebApi.Domain.Services;

namespace WebApi.Shared.Email
{
    public class EmailService: IEmailService
    {
        private readonly IConfiguration _configuration;

        public EmailService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task SendNewTemporaryPassword(string email, string name, string password)
        {
            var apiKey = _configuration.GetSection("SendGrid").GetSection("ApiKey").Value;
            var emailSender = _configuration.GetSection("SendGrid").GetSection("EmailSender").Value;
            var nameSender = _configuration.GetSection("SendGrid").GetSection("NameSender").Value;
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress(emailSender, nameSender);
            var subject = "New password";
            var to = new EmailAddress(email, name);
            var plainTextContent = $"New password: {password}";
            var htmlContent = $"<strong>New password: {password}</strong>";
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
            var response = await client.SendEmailAsync(msg);
        } 
    }
}