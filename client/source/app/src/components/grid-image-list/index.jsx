import React, { useState } from 'react';
import useStyles from './useStyles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import ClearOutlinedIcon from '@material-ui/icons/ClearOutlined';
import SearchOutlinedIcon from '@material-ui/icons/SearchOutlined';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import { Carousel } from 'react-responsive-carousel';
import { Modal } from '@material-ui/core';

const Thumbs = ({ files, onClick, onRemove }) => {
    const classes = useStyles();

    return files.map(file => (
        <GridListTile className={classes.item} key={file.name}>
            <img src={file.preview} alt={file.name} className={classes.img} />
            <GridListTileBar
                classes={{
                    root: classes.titleBar,
                    title: classes.title,
                }}
                actionIcon={
                    onRemove &&
                    <>
                    <IconButton aria-label={`star ${file.name}`} onClick={onClick} >
                        <SearchOutlinedIcon className={classes.title} />
                    </IconButton>
                    <IconButton aria-label={`star ${file.name}`} onClick={() => onRemove({ file })}>
                        <ClearOutlinedIcon className={classes.title} />
                    </IconButton>
                    </>
                }
            />
        </GridListTile>
    ))
};

export default function GridImageList({ files, remove }) {
    const classes = useStyles();
    const [openModal, setOpenModal] = useState(false);
    return (
        <>
            <div className={classes.root}>
                <GridList className={classes.gridList} cols={2.5}>
                    <Thumbs files={files} onRemove={remove} onClick={() => setOpenModal(true)} />
                </GridList>
            </div>
            <Modal
                open={openModal}
                onClose={() => setOpenModal(false)}
            >
                <div className={classes.modal}>
                    <Carousel>
                        {files.map(f => (
                            <div>
                                <img src={f.preview} alt={f.name} />
                            </div>
                        ))}
                    </Carousel>
                </div>
            </Modal>
        </>
    )
}