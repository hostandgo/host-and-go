using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using WebApi.Domain.Exceptions;
using WebApi.Domain.ValueObjects;

namespace WebApi.Domain.Models
{
    public class Profile : BaseEntity
    {
        private const int PhotosAmountMaximum = 10;

        public Profile(
            Guid userId,
            Name name,
            int? genre,
            string nativeLanguage,
            string nationality,
            Location location,
            SocialMedia socialMedia,
            string deficiency,
            string religion,
            DateTime? dateOfBirth,
            IEnumerable<Language> languages
        )
        {
            this.UserId = userId;
            this.Name = name;
            this.Genre = genre;
            this.NativeLanguage = nativeLanguage;
            this.Nationality = nationality;
            this.Location = location;
            this.SocialMedia = socialMedia;
            this.Deficiency = deficiency;
            this.Religion = religion;
            this.DateOfBirth = dateOfBirth;
            this._languages.AddRange(languages);
        }

        public Guid UserId { get; private set; }
        public string Picture { get; private set; }
        public Name Name { get; private set; }
        public int? Genre { get; private set; }
        public string NativeLanguage { get; private set; }
        public string Document { get; private set; }
        public string Nationality { get; private set; }
        public DateTime? DateOfBirth { get; private set; }
        private List<Language> _languages = new List<Language>();
        public IReadOnlyCollection<Language> Languages => _languages;
        public Location Location { get; private set; }
        public SocialMedia SocialMedia { get; private set; }
        public string Religion { get; private set; }
        public string Deficiency { get; private set; }
        private List<string> _familyPhotos = new List<string>();
        public IReadOnlyCollection<string> FamilyPhotos => _familyPhotos;

        public Profile Update(
            Name name, 
            int? genre, 
            string nativeLanguage,
            string nationality, 
            Location location,
            SocialMedia socialMedia,
            string deficiency, 
            string religion, 
            DateTime? dateOfBirth,
            IEnumerable<Language> languages)
        {
            this.Name = name;
            this.Genre = genre;
            this.NativeLanguage = nativeLanguage;
            this.Nationality = nationality;
            this.Location = location;
            this.SocialMedia = socialMedia;
            this.Deficiency = deficiency;
            this.Religion = religion;
            this.DateOfBirth = dateOfBirth;
            this._languages.Clear();
            this._languages.AddRange(languages);
            return this;
        }

        public Profile AddPhotos(List<string> fotosToAdded)
        {
            if (_familyPhotos.Count > PhotosAmountMaximum ||
                _familyPhotos.Count + fotosToAdded.Count() > PhotosAmountMaximum)
                throw new DomainException("Photos is less than 10");

            _familyPhotos.AddRange(fotosToAdded);
            return this;
        }


        public Profile RemovePhotos(List<string> filesToRemove)
        {
            _familyPhotos = _familyPhotos.Except(filesToRemove).ToList();
            return this;
        }

        public Profile UpdateDocument(string fileName)
        {
            Document = fileName;
            return this;
        }

        public Profile UpdatePicture(string picture)
        {
            Picture = picture;
            return this;
        }
    }
}