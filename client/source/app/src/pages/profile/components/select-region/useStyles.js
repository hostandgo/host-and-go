import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(theme => ({
    container: {
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
    },
    textFieldRegion: {
        paddingLeft: theme.spacing(1),
        paddingRight: theme.spacing(1),
        width: '100%'
    }
}));