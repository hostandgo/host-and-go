using System;
using WebApi.Domain.Enums;
using WebApi.Domain.Models;

namespace WebApi.Application.ViewModels
{
    public class RestrictionViewModel
    {

        public RestrictionViewModel(RestrictionsTypes type, string complement)
        {
            this.Type = type;
            this.Complement = complement;

        }
        public RestrictionsTypes Type { get; set; }
        public string Complement { get; set; }
        public static Func<Restriction, RestrictionViewModel> FromModel = r => new RestrictionViewModel(r.Type, r.Complement);
    }
}