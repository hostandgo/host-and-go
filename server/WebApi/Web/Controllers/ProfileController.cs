using System.Threading.Tasks;
using WebApi.Application.CommandSide.Commands;
using WebApi.Application.QuerySide.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using WebApi.Web.Utils;

namespace HostAndGo.Web.Controllers
{
    [Route("api/profile")]
    [ApiController]
    public class ProfileController: ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly RequestContext _requestContext;

        public ProfileController(IMediator mediator, RequestContext requestContext)
        {
            _mediator = mediator;
            _requestContext = requestContext;
        }

        [HttpPut("update")]
        public async Task<ActionResult> UpdateProfile([FromBody]UpdateProfileCommand command)
        {
            command.UserId = _requestContext.UserId;
            await _mediator.Send(command);
            return Ok();
        }

        [HttpGet]   
        public async Task<ActionResult> Get()
        {
            var result = await _mediator.Send(new GetProfileQuery(_requestContext.UserId));
            return Ok(result);
        }

        [HttpPost("update-family-photos")]
        public async Task<ActionResult> UpdateFamilyPhotos(List<IFormFile> files)
        {
            await _mediator.Send(new UpdateFamilyPhotosCommand(_requestContext.UserId, files));
            return Ok();
        }

        [HttpPost("update-document")]
        public async Task<ActionResult> UpdateDocument(List<IFormFile> files)
        {
            await _mediator.Send(new UpdateDocumentCommand(_requestContext.UserId, files));
            return Ok();
        }

        [HttpPost("update-picture")]
        public async Task<ActionResult> UpdatePicture(List<IFormFile> files)
        {
            await _mediator.Send(new UpdatePictureCommand(_requestContext.UserId, files));
            return Ok();
        }
    }
}