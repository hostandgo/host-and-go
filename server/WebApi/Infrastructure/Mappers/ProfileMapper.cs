using WebApi.Domain.Models;
using MongoDB.Bson.Serialization;

namespace WebApi.Infrastructure.Mappers
{
    public static class ProfileMapper
    {
        public static void Configure()
        {
            BsonClassMap.RegisterClassMap<Profile>(map =>
            {
                map.AutoMap();
                map.MapField(p => p.UserId);
                map.MapField(p => p.Name);
                map.MapField(p => p.Document);
                map.MapCreator(p =>
                    new Profile(
                        p.UserId,
                        p.Name,
                        p.Genre,
                        p.NativeLanguage,
                        p.Nationality,
                        p.Location,
                        p.SocialMedia,
                        p.Deficiency,
                        p.Religion,
                        p.DateOfBirth,
                        p.Languages
                ));
                map.MapField(p => p.Languages);
                map.MapField("_familyPhotos").SetElementName("FamilyPhotos");
            });
        }
    }
}