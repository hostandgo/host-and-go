import React, { useEffect } from 'react';
import useStyles from './useStyles';
import useWindowWidth from '../../../../hooks/useWindowWidth';
import { Paper, TextField, Typography } from '@material-ui/core';
import api from '../../../../api';
import InterestsCombos from '../../../../components/interests-combos';

export default function PreferencesTab(props) {
    const classes = useStyles();
    const windowWidth = useWindowWidth();
    const isMobile = windowWidth <= 900;

    const {
        setRestrictions,
        setInterests,
        setDescription,
        restrictions,
        setScreenLoading,
        description,
        disabled,
        interests
    } = props;

    useEffect(() => {
        async function getProfile() {
            const result = await api.preferences.get(setScreenLoading)
            if (!result.hasError) {
                setRestrictions(restrictions.map(r => result.response.restrictions.includes(r.value) ? { ...r, checked: true } : r))
                const mapInterestToViewModel = result.response.interests
                    .map(i => i.complements.map(c => ({ interest: { type: i.type, description: i.description }, complementInterest: { type: c.type, description: c.description } })));
                setInterests(mapInterestToViewModel.length > 0 ? mapInterestToViewModel.reduce((p, c) => p) : []);
                setDescription(result.response.description)
            }
        };

        getProfile()
    }, [
        setRestrictions,
        setInterests,
        setDescription,
        restrictions,
        setScreenLoading
    ])

    return (
        <div className={isMobile ? classes.containerMobile : classes.containerDesktop}>
            <Paper
                style={{
                    padding: 8,
                    height: 100,
                    width: '100%',
                    marginBottom: 8
                }}
            >
                <Typography>Preferences</Typography>
                <Typography>Adicione suas preferencias pessoais, estas serão utilizadas para encontrar viajantes e também podem ser utilizadas para você encontrar hosts com perfil próximo ao seu</Typography>
            </Paper>
            <Paper className={isMobile ? classes.paperMobile : classes.paperDesktop}>
                <Typography className={classes.title}>Describe your profile in this field, talk about what you like to do, listen to, see and any other information you find relevant</Typography>
                <div className={isMobile ? classes.formContainerMobile : classes.formContainerDesktop}>
                    <TextField
                        disabled={disabled}
                        id="outlined-description"
                        label="Description"
                        className={isMobile ? classes.textFieldMobile : classes.textFieldDescribe}
                        value={description}
                        onChange={e => setDescription(e.target.value)}
                        multiline
                        rows="4"
                        rowsMax="4"
                        margin="normal"
                        variant="outlined"
                    />
                    <InterestsCombos
                        interests={interests}
                        setInterests={setInterests}
                        disabled={disabled}
                    />
                </div>
            </Paper>
        </div>
    )
}