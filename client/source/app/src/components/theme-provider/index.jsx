import React from 'react';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import CssBaseline from "@material-ui/core/CssBaseline";
import theme from './theme';
import TopBar from '../top-bar';

export default function ThemeProvider(props){
    return(
        <MuiThemeProvider theme={theme}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <CssBaseline />
                {!props.showTopBar && <TopBar />}
                {props.children}
            </MuiPickersUtilsProvider>
        </MuiThemeProvider>
    )
}