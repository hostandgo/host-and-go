import React from 'react';
import useStyles from './styles';
import useIsMobile from '../../hooks/useIsMobile';

export default function DefaultWrapper(props){
    const classes = useStyles();
    const isMobile = useIsMobile(); 
    return (
        <div className={ isMobile ? classes.mobileWrapper : classes.desktopWrapper }>
            {props.children}
        </div>
    )
}