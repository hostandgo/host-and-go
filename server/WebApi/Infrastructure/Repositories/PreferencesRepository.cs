using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WebApi.Domain.Models;
using WebApi.Domain.Repositories;
using MongoDB.Driver;

namespace WebApi.Infrastructure.Repositories
{
    public class PreferencesRepository : IPreferencesRepository
    {
        private readonly MongoContext _context;

        public PreferencesRepository(MongoContext context)
        {
            _context = context;
        }

        public async Task<Preferences> FindByFilter(Expression<Func<Preferences, bool>> filter)
        {
            var result = await _context.Preferences.FindAsync(filter);
            return await result.FirstOrDefaultAsync();
        }

        public async Task<Preferences> FindByUserId(Guid userId)
        {
            var result = await _context.Preferences.FindAsync(p => p.UserId == userId);
            return await result.FirstOrDefaultAsync();
        }

        public async Task Insert(Preferences preferences)
        => await _context.Preferences.InsertOneAsync(preferences);

        public async Task<IEnumerable<Preferences>> ListByFilter(Expression<Func<Preferences, bool>> filter)
        {
            var result = await _context.Preferences.FindAsync(filter);
            return result.Current;
        }

        public async Task Remove(Preferences preferences)
        => await _context
            .Preferences
            .DeleteOneAsync(i => i.Id == preferences.Id);

        public async Task<Preferences> Update(Preferences preferences)
        {
            await _context.Preferences.ReplaceOneAsync(i => i.Id == preferences.Id, preferences);
            return preferences;
        }
    }
}