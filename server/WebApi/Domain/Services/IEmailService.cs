using System.Threading.Tasks;

namespace WebApi.Domain.Services
{
    public interface IEmailService
    {
        Task SendNewTemporaryPassword(string email, string name, string password);
    }
}