using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WebApi.Domain.Models;

namespace WebApi.Domain.Repositories
{
    public interface IProfileRepository
    {
        Task<Profile> FindByUserId(Guid userId);
        Task<Profile> FindByFilter(Expression<Func<Profile, bool>> filter);
        Task<IEnumerable<Profile>> ListByFilter(Expression<Func<Profile, bool>> filter);
        Task Insert(Profile profile);
        Task Remove(Profile profile);
        Task<Profile> Update(Profile profile);
    }
}