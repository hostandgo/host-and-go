import { makeStyles } from '@material-ui/core/styles';


export default makeStyles(theme => ({
    desktopWrapper: {
        display: 'flex',
        marginTop: 63,
        marginLeft: 73,
    },
    mobileWrapper: {
        display: 'flex',
        marginTop: 55,
    }
}));