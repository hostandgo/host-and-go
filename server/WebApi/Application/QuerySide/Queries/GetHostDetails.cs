using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebApi.Application.ViewModels;
using WebApi.Domain.Exceptions;
using WebApi.Infrastructure;
using MediatR;
using MongoDB.Driver;

namespace WebApi.Application.QuerySide.Queries
{
    public class GetHostDetailsQuery : IRequest<GetHostDetailsViewModel>
    {
        public GetHostDetailsQuery(Guid userId)
        {
            this.UserId = userId;

        }
        public Guid UserId { get; set; }
    }

    public class GetHostDetailsQueryHandler : IRequestHandler<GetHostDetailsQuery, GetHostDetailsViewModel>
    {
        private readonly MongoContext _context;

        public GetHostDetailsQueryHandler(MongoContext context)
        {
            _context = context;
        }

        public async Task<GetHostDetailsViewModel> Handle(GetHostDetailsQuery request, CancellationToken cancellationToken)
        {
            var hostResult = await _context.Hosts.FindAsync(p => p.UserId == request.UserId);
            var host = await hostResult.FirstOrDefaultAsync();

            if (host == null)
                throw new NotFoundException("Host not found");

            return new GetHostDetailsViewModel(
                host.Addres,
                host.Description,
                host.TypeOfRoom,
                host.BathroomAccess,
                host.TimesAvailableToReceive.Select(t => new TimeAvailableToReceiveViewModel(t.Start, t.End)).ToArray(),
                host.HoursInteractTravel,
                host.DescribeWhatCanDoTogether,
                host.Restrictions.Select(RestrictionViewModel.FromModel).ToArray(),
                host.DescribeWhatYourCity,
                host.PreferredAgeRange,
                host.City,
                host.Attractions,
                host.Interests
            );
        }
    }
}