using System;
using System.Linq;
using WebApi.Domain.Autentication;
using WebApi.Shared.Utils;

namespace WebApi.Domain.ValueObjects
{
    public class Authentication
    {
        public Authentication(string email, string password)
        {
            Email = email;
            Password = password;
            Role = Roles.Host;
            ForgotPassword = false;
        }

        public string Email { get; private set; }
        public string Password { get; private set; }
        public string Token { get; private set; }
        public string Role { get; private set; }
        public bool ForgotPassword { get; private set; }   
        public string TemporaryPassword { get; private set; }   

        public Authentication SaveToken(string token)
        {
            Token = token;
            return this;
        }

        public Authentication UpdatePassword(string password)
        {
            Password = password;
            ForgotPassword = false;
            TemporaryPassword = null;
            return this;
        }

        public Authentication RequestNewPassword()
        {
            ForgotPassword = true;
            Password = null;
            TemporaryPassword = GenerateTemporaryPassword();
            return this;
        }

        private string GenerateTemporaryPassword()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            return new string(
                Enumerable.Repeat(chars, 8)
                        .Select(s => s[random.Next(s.Length)])
                        .ToArray()
            );
        }

    }
}