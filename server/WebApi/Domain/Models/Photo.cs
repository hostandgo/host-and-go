namespace WebApi.Domain.Models
{
    public class Photo : BaseEntity
    {
        public Photo(string name, long length)
        {
            this.Name = name;
            this.Length = length;

        }
        public string Name { get; set; }
        public long Length { get; set; }
    }
}