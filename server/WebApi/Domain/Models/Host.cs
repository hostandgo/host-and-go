using System;
using System.Collections.Generic;
using System.Linq;
using WebApi.Domain.Enums;
using WebApi.Domain.Exceptions;
using WebApi.Domain.ValueObjects;

namespace WebApi.Domain.Models
{
    public class Host : BaseEntity
    {
        private const int PhotosAmountMaximum = 10;
        public Host(
            Guid userId,
            string addres,
            string description,
            TypeOfRoom? typeOfRoom,
            BathroomAccess? bathroomAccess,
            int hoursInteractTravel,
            string describeWhatCanDoTogether,
            string describeWhatYourCity,
            IEnumerable<PreferredAgeRange> preferredAgeRange,
            IEnumerable<CityType> city,
            IEnumerable<AttractionType> attractions,
            IEnumerable<Interest> interests,
            IEnumerable<TimeAvailableToReceive> timesAvailableToReceive,
            IEnumerable<Restriction> restrictions
        )
        {
            this.UserId = userId;
            this.Addres = addres;
            this.Description = description;
            this.TypeOfRoom = typeOfRoom;
            this.BathroomAccess = bathroomAccess;
            this.HoursInteractTravel = hoursInteractTravel;
            this.DescribeWhatCanDoTogether = describeWhatCanDoTogether;
            this.DescribeWhatYourCity = describeWhatYourCity;
            this._preferredAgeRange.AddRange(preferredAgeRange);
            
            this._city.AddRange(city);
            this._interests.AddRange(interests);
            this._attractions.AddRange(attractions);
            this._timesAvailableToReceive.AddRange(timesAvailableToReceive);
            this._restrictions.AddRange(restrictions);
        }
        public Guid UserId { get; private set; }
        public string Addres { get; private set; }
        public string Description { get; private set; }
        public TypeOfRoom? TypeOfRoom { get; private set; }
        public BathroomAccess? BathroomAccess { get; private set; }
        public int HoursInteractTravel { get; private set; }
        public string DescribeWhatCanDoTogether { get; private set; }
        public string DescribeWhatYourCity { get; private set; }


        public IEnumerable<CityType> City => _city;
        private HashSet<CityType> _city = new HashSet<CityType>();
        public IEnumerable<PreferredAgeRange> PreferredAgeRange => _preferredAgeRange;
        private HashSet<PreferredAgeRange> _preferredAgeRange = new HashSet<PreferredAgeRange>();
        private HashSet<AttractionType> _attractions = new HashSet<AttractionType>();
        public IReadOnlyCollection<AttractionType> Attractions => _attractions;
        private HashSet<TimeAvailableToReceive> _timesAvailableToReceive = new HashSet<TimeAvailableToReceive>();
        public IReadOnlyCollection<TimeAvailableToReceive> TimesAvailableToReceive => _timesAvailableToReceive;
        private HashSet<Restriction> _restrictions = new HashSet<Restriction>();
        public IReadOnlyCollection<Restriction> Restrictions => _restrictions;
        private HashSet<Interest> _interests = new HashSet<Interest>();
        public IReadOnlyCollection<Interest> Interests => _interests;
        private List<string> _photos = new List<string>();
        public IReadOnlyCollection<string> Photos => _photos;
        public Host Update(
            string addres,
            string description,
            TypeOfRoom? typeOfRoom,
            BathroomAccess? bathroomAccess,
            int hoursInteractTravel,
            string describeWhatCanDoTogether,
            string describeWhatYourCity,
            IEnumerable<PreferredAgeRange>preferredAgeRange,
            IEnumerable<CityType> city,
            IEnumerable<AttractionType> attractions,
            IEnumerable<Interest> interests,
            IEnumerable<TimeAvailableToReceive> timesAvailableToReceive,
            IEnumerable<Restriction> restrictions
        )
        {
            this.Addres = addres;
            this.Description = description;
            this.TypeOfRoom = typeOfRoom;
            this.BathroomAccess = bathroomAccess;
            this.HoursInteractTravel = hoursInteractTravel;
            this.DescribeWhatCanDoTogether = describeWhatCanDoTogether;
            this.DescribeWhatYourCity = describeWhatYourCity;
            
            
            this._preferredAgeRange.Clear();
            this._city.Clear();
            this._attractions.Clear();
            this._interests.Clear();
            this._timesAvailableToReceive.Clear();
            this._restrictions.Clear();

            this._preferredAgeRange.AddRange(preferredAgeRange);
            this._city.AddRange(city);
            this._attractions.AddRange(attractions);
            this._interests.AddRange(interests);
            this._timesAvailableToReceive.AddRange(timesAvailableToReceive);
            this._restrictions.AddRange(restrictions);
            return this;
        }

        public Host AddPhotos(IEnumerable<string> newPhotos)
        {
            if (_photos.Count > PhotosAmountMaximum || _photos.Count + newPhotos.Count() > PhotosAmountMaximum)
                throw new DomainException("Photos is less than 10");

            _photos.AddRange(newPhotos);
            return this;
        }

        public Host RemovePhotos(IEnumerable<string> keys)
        {
            _photos = _photos.Except(keys).ToList();
            return this;
        }
    }
}