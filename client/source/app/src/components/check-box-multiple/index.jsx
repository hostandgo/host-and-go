import React from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import ListItemText from '@material-ui/core/ListItemText';
import Select from '@material-ui/core/Select';
import Checkbox from '@material-ui/core/Checkbox';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        }
    },
};

export default function CheckBoxMultiple({
    label,
    collection,
    key,
    menuValue,
    description,
    value,
    onChange,
    className,
    errors,
}) {
    const inputLabel = React.useRef(null);
    const [labelWidth, setLabelWidth] = React.useState(0);
    React.useEffect(() => {
        setLabelWidth(inputLabel.current.offsetWidth);
    }, []);
    const hasError = errors && value.length === 0;
    return (
        <FormControl
            variant="outlined"
            margin="normal"
            className={className}
            error={hasError}
            helperText={hasError && "This field is required"}
        >
            <InputLabel ref={inputLabel} id="demo-mutiple-checkbox-label">{label}</InputLabel>
            <Select
                labelId={`demo-mutiple-checkbox-label-${label.toLowerCase().replace(' ', '-')}`}
                id={`mutiple-checkbox-${label.toLowerCase().replace(' ', '-')}`}
                fullWidth
                multiple
                labelWidth={labelWidth}
                value={value}
                onChange={onChange}
                renderValue={selected => selected.map(s => s[description]).join(', ')}
                MenuProps={MenuProps}
            >
                {collection.map(c => (
                    <MenuItem key={c[key]} value={c}>
                        <Checkbox checked={value.map(v => v.value).indexOf(c.value) > -1} />
                        <ListItemText primary={c[description]} />
                    </MenuItem>
                ))}
            </Select>
        </FormControl>
    )
}