using System;

namespace WebApi.Domain.ValueObjects
{
    public class TimeAvailableToReceive
    {
        public TimeAvailableToReceive() {}
        public TimeAvailableToReceive(DateTime start, DateTime end)
        {
            this.Start = start;
            this.End = end;

        }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
    }
}