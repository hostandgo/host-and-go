import React, { useState, useEffect } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Avatar from '@material-ui/core/Avatar';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import CryptoJS from 'crypto-js';
import api from '../../api';
import { secret, login, isAuthenticated } from '../../services/auth';
import history from '../../services/history';
import useStyles from './styles';
import './transition.css';
import ForgotPassword from './components/forgot-password';
import FooterBox from './components/footer-box';
import UpdatePassword from './components/update-password';
import ButtonWithLoading from '../../components/button-with-loading';


export default function SignUp() {
    const classes = useStyles();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [hasError, setHasError] = useState(false);
    const [emailError, setEmailError] = useState(false);
    const [passwordError, setPasswordError] = useState(false);
    const [isRegister, setIsRegister] = useState(false);
    const [forgotPassword, setForgotPassword] = useState(false);
    const [updatePassword, setUpdatePassword] = useState(false);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        if(isAuthenticated())
            history.push('/profile');
    })

    async function registerSubmit() {
        var erros = validateForm();
        if(erros.length > 0){
            erros.map(e => e());
            return;
        }

        const encryptPassword = CryptoJS.AES.encrypt(password, secret).toString();
        const result = await api.authentication.register({email, password: encryptPassword}, setLoading);
        if(!result.hasError){
            const { token, id } = result.response;
            loginAndRedirect(token, id, true);
        }
    }

    async function loginSubmit() {
        if(!loading){
            var erros = validateLogin();
            if(erros.length > 0){
                erros.map(e => e());
                return;
            }
            const encryptPassword = CryptoJS.AES.encrypt(password, secret).toString();
            const result = await api.authentication.login({email, password: encryptPassword}, setLoading);
            if(!result.hasError)
            {
                if(result.response.forgotPassword)
                    setUpdatePassword(true);
                else
                    loginAndRedirect(result.response.token, result.response.id, false);
            }
        }
    }

    function validateForm(){
        setEmailError(false);
        setPasswordError(false);
        var errorsHandlers = [];
        if (password !== confirmPassword)
            errorsHandlers.push(() => setHasError(true));

        if(!email)
            errorsHandlers.push(() => setEmailError(true));
            
        if(!password)
            errorsHandlers.push(() => setPasswordError(true));

        return errorsHandlers;
    }

    function validateLogin(){
        setEmailError(false);
        setPasswordError(false);
        var errorsHandlers = [];
        if(!email)
            errorsHandlers.push(() => setEmailError(true));
            
        if(!password)
            errorsHandlers.push(() => setPasswordError(true));

        return errorsHandlers;
    }

    function loginAndRedirect(token, userId, fromRegister) {
        login(token, userId);
        let tokenExist = false;
        while (!tokenExist) {
            if(isAuthenticated()){
                tokenExist = true;
                    history.push(`/profile?fromRegister=${fromRegister.toString()}`);
            }
        }

    }
    return (
        <Grid container component="main" className={classes.root}>
            <Grid item xs={false} sm={4} md={7} className={classes.image} />
            <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Sign in
                    </Typography>
                    
                    {
                        !updatePassword &&
                        (
                            forgotPassword ?
                            <ForgotPassword 
                                classes={classes}
                                backToInitialPage={() => setForgotPassword(false)}
                            />
                            :
                            <form className={classes.form}>
                            <Grid container spacing={2}>
                                <Grid item xs={12}>
                                    <TextField
                                        error={emailError}
                                        helperText={emailError && "Email is required"}
                                        variant="outlined"
                                        required
                                        fullWidth
                                        type="email"
                                        value={email}
                                        onChange={e => setEmail(e.target.value)}
                                        id="email"
                                        label="Email Address"
                                        name="email"
                                        autoComplete="email"
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        error={passwordError}
                                        variant="outlined"
                                        required
                                        fullWidth
                                        helperText={passwordError && "Password is required"}
                                        value={password}
                                        onChange={e => setPassword(e.target.value)}
                                        name="password"
                                        label="Password"
                                        type="password"
                                        id="password"
                                        autoComplete="current-password"
                                    />
                                </Grid>
                                    <Grid item xs={12}>
                                        <ReactCSSTransitionGroup
                                            transitionName="example"
                                        >
                                            {
                                                isRegister &&
                                                <TextField
                                                    error={hasError}
                                                    variant="outlined"
                                                    required
                                                    fullWidth
                                                    value={confirmPassword}
                                                    onChange={e => setConfirmPassword(e.target.value)}
                                                    helperText={hasError && "Incorrect confirmation"}
                                                    name="confirmPassword"
                                                    label="Confirm password"
                                                    type="password"
                                                    id="confirmPassword"
                                                    autoComplete="current-password"
                                                />
                                            }
                                    </ReactCSSTransitionGroup>
                                    </Grid>
                            </Grid>
                            <ReactCSSTransitionGroup
                                transitionName="example"
                            >
                                <ButtonWithLoading
                                    styles={{marginTop: 30, marginBottom: 5}}
                                    loading={loading}
                                    handleButtonClick={isRegister ? async () => await registerSubmit() : async () => await loginSubmit()}
                                    label={isRegister ? 'Sign Up' : 'Login'}        
                                />
                                <span 
                                    style={{color:'blue',cursor:'pointer'}} 
                                    onClick={() => setForgotPassword(true)}
                                >
                                    Forgot password?
                                </span>
                            </ReactCSSTransitionGroup>
                            <FooterBox 
                                classes={classes}
                                onClick={() => setIsRegister(!isRegister)}
                                label={isRegister ? 'Already have an account? Sign in' : `Don't have an account? Register`}
                            />
                        </form>
                        )    
                    }
                    { 
                        updatePassword &&
                        <UpdatePassword 
                            email={email}
                            classes={classes}
                        />
                    }
                </div>
            </Grid>
        </Grid>
    );
}